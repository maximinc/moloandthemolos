
\version "2.18.2"
\language "english"

\header {
  title = "Douce ambiance"
  composer = "Django Reinhardt"
}

global = {
  \time 4/4
  \key g \minor
  \tempo 4=160
}



melody = \relative g' {
  \global
  d8 d d d~d4 r | d8 d d d~d4 r | d8 d d d~d4 r | d8 d d d~d4 r | \break
  d8 d d d~d4 r | d8 d d d~d4 r | d8 d d d~d4 r | d2. r4 | \break
  
  \bar "||"
  
  d'8 d d d~d4 r | d8 d d d~d4 r | \tuplet 3/2 {d8 ef d} cs8 d f ef bf c | d1   | \break
  c8 c c c~ c cs d bf~ | bf4. g8~ g2 | \tuplet 3/2 {af4 af af} ef'4 ef8 d~ | d2 r | \break

  d8 d d d~d4 r | d8 d d d~d4 r | \tuplet 3/2 {d8 ef d} cs8 d f ef bf c | d1   | \break
  \tuplet 3/2 {c4 c c} \tuplet 3/2 {c4 cs d} | bf4. g8~ g2 | \tuplet 3/2 {a4 a a} a8 bf~bf g8~ | g2 r \break

  af4. bf8 cf df ef f | gf4. f8~ f4 r | ef8 cf af gf f4 \tuplet 3/2 {gf8 af gf} | f2  r | \break
  
  a4. b8 c d e fs  | g4. fs8~ fs4 r | f4. e4. ef4 | r  cs d  r | \break

  d8 d d d~d4 r | d8 d d d~d4 r | \tuplet 3/2 {d8 ef d} cs8 d f ef bf c | d1   | \break
  \tuplet 3/2 {c4 c c} \tuplet 3/2 {c4 cs d} | bf4. g8~ g2 | \tuplet 3/2 {a4 a a} a8 bf~bf g8~ | g2 r \break

  \bar "||"
  
  % Solo A1
  d8 d d d~ d d d d | ef d cs d~d4 r | r d4 d d  | ef8 d8~d8 d ef c b d~ | \break
  d8 c4.~c8 cs8 d a c bf~   bf4~ bf4. bf'8  | af af, c b g'4 f8 ef | d4. fs,8 a d fs a |  \break  
  
  % Solo A2
  g' g, bf d g4 bf8 bf | bf  a c  \tuplet 3/2 {bf16 c bf } a8 g fs ef d c \tuplet 3/2 {bf bf bf} g a bf d g4 d8 f8~f4 r | \break
  r8 c d ef d4\mordent c8 g'~ | g8 g, bf c \tuplet 3/2 {cs d cs } c bf | d4 d d  fs,8 g~ |  g8 g4. r2 | \break
  
  % Solo B
  af8 cf~ cf4~ cf2~ | cf2 \tuplet 3/2 {af,8 bf af } g8 af~ | 
  af4  \tuplet 3/2 {af8 bf af } g8 af cf ef | af cf  e a r4 r | \break
  
  a \glissando e8 c8 r a \tuplet 3/2 {gs a gs} | a4 a a r | f4 r e ef | r cs d r | \break
  r8 d' c bf g d bf a | g fs a c d ef fs a c ef \tuplet 3/2 {d ef d } bf g d bf g4  bf8 f' ef4 \mordent r | \break
  
  % Solo A 3
  r8 c d ef \tuplet 3/2 {d ef d } c g' | r g, a bf \tuplet 3/2 { d ef d } c bf | \tuplet 3/2 { cs d cs } c bf d4 fs,8 g~ | g g4. r2 | \break
  bf'2 a4 g8 d'8~| d4 r r2 | bf2 a4 g | gf8 f~f f~f4 r | \break
  r8  ef f g a c ef cs | d  bf g d bf4 bf | c r8 c ef af c ef | d4 \tuplet 3/2 {d8 ef d} \tuplet 3/2 {c a fs} \tuplet 3/2 {d df c} | \break
  
  % Solo A1 (grille 2)
  \tuplet 3/2 {a af fs} g4 r2 | ef''8 d f ef d bf c a | c bf g4 d4 r  | g8 d d f~\mordent f4 r | \break 
  r8 ef f g a c ef cs | d bf g4 r r | \tuplet 3/2 {cs,8 d cs} c bf d d fs, g~ | g g4.  r2 | \break
  
  % Solo B (grille 2)
  af'4. bf8 cf df ef f | gf4. f8~ f4 r | ef8 cf af gf f4 \tuplet 3/2 {gf8 af gf} | f2  r | \break
  a4. b8 c d e fs  | g4. fs8~ fs4 r | f4 r  e ef | r  cs d  r | \break
  
  % Solo A2
  r1 | r1 | r8 bf' a g fs4 fs | \tuplet 3/2 {fs8 g fs} f d f4 \mordent r | \break
  
  r8 ef r ef c g ef c | d4 d d g,8 cs~ | cs4 c8 bf c4 bf8 g~ | g g4.  r2 | \break

  \bar "||"
  d'8 d d d~d4 r | d8 d d d~d4 r | d8 d d d~d4 r | d8 d d d~d4 r | \break
  d8 d d d~d4 r | d8 d d d~d4 r | r8 d' cs4 c b | bf8 a af g r  d' g,4

  \bar "|."
  
  
}

chordintro = \chordmode{ 
    d1:7 ef:maj7 e:7 f:6
    bf:maj7 ef:maj7 a:m11 d:7
}

chordb = \chordmode{ 
    g1:m6 d:7 g2:m6 f:7/a bf b:dim7
    c1:m6 g:m6  af:7  d:7

    g1:m6 d:7 g2:m6 f:7/a bf b:dim7
    c1:m6 g:m6  af2:7  d:7 g1:m6
    
    af1:m6 af1:m6 af1:m6 af1:m6
    a1:m6 a1:m6 f2:7 e4:7 ef:7 s4 cs:7 d2:7

    g1:m6 d:7 g2:m6 f:7/a bf b:dim7
    c1:m6 g:m6  af2:7  d:7 g1:m6

}




chordNames = {
  \chordintro
  \chordb
  
  \chordb
  
  \chordb
  \chordmode{ 
    d1:7 ef:maj7 e:7 f:6
    bf:maj7 ef:maj7}
  
}

\book{
  \bookOutputName "douce_ambiance_C"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
      \new Staff \with {instrumentName = #"(C)"}{ 
        \melody
      }
    >>
    \layout { }      
  }
}

\book{
  \bookOutputName "douce_ambiance_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames}
      }
      \new Staff \with {instrumentName = #"(Bb)"}{ 
        \transpose c d {\melody}
      }
    >>
    \layout { }      
  }
}

\book{
  \bookOutputName "douce_ambiance_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c a { \chordNames}
      }
      \new Staff \with {instrumentName = #"(Eb)"}{ 
        \transpose c a {\melody}
      }
    >>
    \layout { }      
  }
}
