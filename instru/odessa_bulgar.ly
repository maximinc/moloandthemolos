
\version "2.18.2"
\language "english"

\header {
  title = "Odessa Bulgar"
}


chordNames = \chordmode{
  s1
  d1:m d:m d:m a:7
  d:m d:m a:7 d:m
  a:7 d:m d:m
  
  d:m e2:7 a:m e:7 d:m a1:7 d:m
  a:7 d:m
  d:m a:7
  d:m a:7
  a2:7 d:m

  d1:m d:m d:m a:7
  d:m d:m a:7 d:m
  a:7 d:m d:m

}

themeA={
  \repeat volta 2{
  a4 gs a b c b a8 gs f4 gs16 b a8 a a a gs f gs a b a gs f e f gs \break
  a4 gs a b c b a8 gs f4 e8 f 
  a16 gs f8
  gs4  a d, r8 a'8 {a16 gs f8} f4
  \break 
  e8 f  {a16 gs f8 } gs4  a 
  
  }
   \alternative{
    {d,  r r8 a d f }
    {d2  r2}
   }
}
clarinet = {
  \key a \minor
  \relative c'{
    r4 r r8 a d f 
    \themeA
  }
  \break
  \relative c''{
    
    \repeat volta 2 {
    r8 d d d d d d d
    
    d c b d c b a c b a gs b
    a gs f4 
    e8 f  {a16 gs f8 } gs4  a \break
    d, r8 a'8 {a16 gs f8 } f4
    e8 f {a16 gs f8  } gs4  a <d, d'> r r2
    }
  }
  \break	 
  \relative c''{
    \repeat volta 2{
    d8 d r a8 {a16 gs f8} f4
    e8 d e f gs a f16 e d8
    d'8 d r a8  {a16 gs f8 } f4
    }
    \alternative{
      {e8 d e f e d d'4}
      {e,8 d e f d a d f}
    }
  }
  \break
  \relative c''{
    \themeA
  }
  \bar "|."
}


\book{
  \bookOutputName "odessa_bulgar_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (C)" }{ 
        \transpose c c { \clarinet  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "odessa_bulgar_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (Bb)" }{ 
        \transpose c d { \clarinet  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "odessa_bulgar_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c a { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (Eb)" }{ 
        \transpose c a { \clarinet  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "odessa_bulgar_C_clef_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (C)" }{ 
        \clef C
        \transpose c c { \clarinet  }
      }
    >>
    \layout { }
  }
}
