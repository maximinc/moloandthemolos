\language "english"

\version "2.18.2"

\header {
  title = "Love Like You"
  composer = "Rebecca Sugar"
}

global = {
  \time 3/4
  \key ef \major
  \tempo 4=160
}



A = \relative g' {
  g8 bf g4. g8 | af4 ef'8 ef8~ ef4 | bf8 d bf4 bf | b f'8 f8~f8 ef16 f |
}

B = \relative g'{
    g'8 f ef d~d ef | c4 c8 df c g f4 a c | 
}


melody = \relative g' {
  \global
  
  bf'4 af <bf, gf'> | <af f'>2 <gf ef'>4 <b ef g>2. f'\fermata \break

  \mark \markup { \musicglyph #"scripts.segno" }
  \repeat volta 2
  {\A \break
  \B
  \mark \markup { \musicglyph #"scripts.coda" 2nd}
  }
  \alternative{
    {g8 bf8 ef, f bf,4 |}
    {
        << { \voiceOne g2 ef4   | \break d2. }
        \new Voice {\voiceTwo    \ottava #-1 r4 bf, b | c \ottava #0 } >>
    }
  }
  
  r2. | r  | g'''2 ef4 | \break
  d2.~ | d | r | g2 ef4 | \break
  d2.~ | d | r | r  | \break
  r2. | r | r | r4 g8 f ef4 | \break
  c g'8 f~ f ef | b4 f' ef | g bf8 c8 ~c8 f,~ | f4 ef r8 g,8 | \break
  af4 c8 ef~ ef4  | d r d8 c | bf8 d~d f~f4  | ef\staccato g8 f ef4 | \break
  c g'8 f~ f ef | b4 f' ef | g bf8 c8 ~c8 d~  | d4 r c | \break
  bf r af | g af c, | g'2. | f2.     \bar "||"
  
  %\once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \cadenzaOn
  \stopStaff
  \mark \markup { \center-column { \musicglyph #"scripts.segno" }} |
       \once \override TextScript.extra-offset = #'( 0 . -3.0 )
       \once \override TextScript.word-space = #1.5
       <>^\markup { \center-column { "D.S. al Coda" \line { \musicglyph #"scripts.coda"} } }

       % Increasing the unfold counter will expand the staff-free space
       \repeat unfold 3 {
          s1
          \bar ""
        }
  
  %\mark \markup { \center-column { \musicglyph #"scripts.segno" \line{D.S. al coda} }} |
  \break
  \cadenzaOff
  \startStaff
  
  \once \override Staff.KeySignature.break-visibility = #end-of-line-invisible
  \once \override Staff.Clef.break-visibility = #end-of-line-invisible
  \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 0 )
  
  \once \override Score.RehearsalMark.font-size = #5
  \mark \markup { \musicglyph #"scripts.coda" }
  
  g2  f4 | 
  << { \voiceOne
       ef2.   |  r | r | g2 ef4 | \break
       d2.     | r | r | g4 bf d, | \break
       df2. | r2. | r | r | r \fermata |
     }
     \new Voice {
      \voiceTwo   
      g,4 f ef | d ef c  | c2.   | r | 
      f4 ef d  | c d  bf | bf2. | r |
      f'4 ef df  | c df  bf | bf2. | |
     }
  >>
  
  \bar "|."
}

words = \lyricmode {
  {\skip 1} {\skip 1} {\skip 1} {\skip 1} {\skip 1} {\skip 1} {\skip 1} 
  <<
  { If I could beg -- in   to be half of what you think of me I could do a -- bout a -- ny -- thing
  I could e -- ven learn how to love
  {\skip 1} {\skip 1} {\skip 1} {\skip 1} {\skip 1} like you
  }
  \new Lyrics = "secondVerse"
  { When I see the way you act won -- dering when I'm co -- ming back}
  
  >>
 {\skip 1} {\skip 1} {\skip 1} {\skip 1} {\skip 1} {\skip 1}
  I al -- ways thought I might be bad now I'm sure that it's true cause
  I think you're soo good and I'm no -- thing  like you	
  Look at you go I just a -- dore you I wish that I knew what makes you think I'm so spe -- cial
}

chordNames = {
  \chords{ 
        af2.:m9 s b:aug s
        
        ef2.:maj f:min7 g:min7 af:min6 g:min7 c:7 f:7 af:maj9/bf af:maj9/bf
         c:m9 c:m9 c:m9 c:m9
         af:maj7.11+ af:maj7.11+ af:maj7.11+ af:maj7.11+
         c:m9 c:m9 c:m9 c:m9
         f:m9 f:m9 g:aug g:aug
         af:maj7 af:dim7 g:m7 c:m7
         f:m7 fs:dim7 g:m7 ef:7
         af:maj7 af:dim7 g:m7 c:7
         f:m7 f:m7 b:aug/d bf:13
         
         s s s s
         af:maj7/bf af:maj7 af:maj7 af:maj7 af:maj7
         g:m7 g:m7 g:m7 g:m7
         
         gf:maj7 gf:maj7 gf:maj7 gf:maj7
         
         b:maj7 
  }
}

\book {
  \bookOutputName "love_like_you_C"
  \score {
    <<
      \new ChordNames { 
         \chordNames
      }
      \new Staff \with {instrumentName=#"(C)"} { 
         \melody
      }
      \addlyrics { \words }
    >>
    \layout { }
  }
}
\book {
  \bookOutputName "love_like_you_Bb"
  \score {
    <<
      \new ChordNames { 
         \transpose c d { \chordNames }
      }
      \new Staff \with {instrumentName=#"(Bb)"} { 
         \transpose c d { \melody }
      }
      \addlyrics { \words }
    >>
    \layout { }
  }
}