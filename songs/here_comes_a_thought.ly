
\version "2.18.2"
\language "english"

\header {
  title = "Here comes a thought"
  composer = "Rebecca Sugar"
}



melody = \relative c' {
  \time 4/4
  \key c \major

  r8 e16 e e e g16 e~ e d d8~  d4 | r8 e16 e e e g16 e~ e d d8~  d4 |
  r8 e16 e e e g16 e~ e d d8~  d4 | r8 e16 e e e g16 e~ e d d8~  d4 | \break
  
  d8. c16~ c16 b8 a16~ a4 r | r8 d8~ d16 c8 b16 a8. b16~ b4 |
  e8. d16~ d16 c8 b16~ b4 r  | r8 e8~ e16 d8 c16 b8. c16~ c4 \break

  d8. c16~ c16 b8 a16~ a4 r | r8 g8 \tuplet 3/2 {d'8 c b }  a8. b16~ b4 |
  e8. d16~ d16 c8 b16~ b4 r8 g | r8. e'16 ~ e16 d8 c16 b8. c16~ c8 g8 \break

  g'2 g8 a b g~ | g2 g8 a b b~ | b2 d8 c b16 a8 c16~ |  c4 a8 b c b a4  | \break
  f c' a f | e4. d8~ d4 r | r c' a f e4. d8~ d4 r \break
  
  r8 e16	 e e e g16 e~ e d d8~  d8 g, | r8 e'16 e e e g16 e~ e d d8~  d4 |
  r8 e16	 e e e g16 e~ e d d8~  d16 g,8. | 
  \tuplet 3/2 {e'8 d e d e c} b4 r8 c'16 c16 | \break
  %e'16.  d  e d e16 c b4 r8 c'16 c16 | \break
  c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4  \tuplet 3/2 {r8 c'8 c}
  %c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c16 |
  \tuplet 3/2 { c b b b a a a g g g e d} c2 r4  \tuplet 3/2 {r8 c'8 c} |
  %c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c16 |
  \tuplet 3/2 { c b b b a a a g g g e d} | c2 r4. g'8~ |
  g8 c,4 g'8~g8 c,4 g'8~ | g8 c,4. r2 | \break  
  
  \bar "||"
 
 
  d8. c16~ c16 b8 a16~ a4 r | r8. d16~ d16 c8 b16 a8. b16~ b4 |
  e8. d16~ d16 c8 b16~ b4 r  | r8. e16~ e16 d8 c16 b8. c16~ c4 \break

  d8. c16~ c16 b8 a16~ a4 r | r8 g16 d'~ d c8 b16  a8. b16~ b4 |
  e8. d16~ d16 c8 b16~ b4 r8 g | r8. e'16 ~ e16 d8 c16 b8. c16~ c8 g8 \break

  g'2 g8 a b g~ | g2 g8 a b b~ | b2 d8 c b16 a8 c16~ |  c4 a8 b c b a8 f~  | \break
  f4 c' a f | e4. d8~ d4 r | r <c' e> <a c> <f a> <e g>4. <g, b>8~ <g b>4 r \break
 
 %{
  d4 c8 b a4 r | r16 d8. c8 b a8. b16~ b4 |
  e4 d8 c~ c b4. | r8 e8 d8 c b8. c16~ c4 \break

  d4 c8 b a4 r | r8 g8 \tuplet 3/2 {d'8 c b }  a8. b16~ b4 |
  e4 d8 c~ c b8	 r8. g16 | r4 \tuplet 3/2 {e'8 d8 c} b8. c16~ c8 g8 \break

  g'2 g8 a b g~ | g2 g8 a b b~ | b2 d8 c b a |  c4 a8 b c b a f~  | \break
  f4 c' a f | e4. d8~ d4 r | r c' a f e4. d8~ d4 r \break
 %}

  \bar "||"
  
  r8 <e' a>16 <e a> <e a> <e a> <g a>16 <e a>~ <e a> <d a'> <d a'>8~  <d a'>8 <a a'> | 
  r8 <e' af>16 <e af> <e af> <e af> <g af>16 <e af>~ <e af> <d af'> <d af'>8~  <d af'>4 |
  r8 <e a>16 <e a> <e a> <e a> <g a>16 <e a>~ <e a> <d a'> <d a'>8~  <d a'>8 <a a'> |

  \tuplet 3/2 {<e' gs>8 <d f> <e a> <d gs>  <e a> <c f>} <b d>4 r8 <c' e>16 <c e>16 | \break
  %e'16.  d  e d e16 c b4 r8 c'16 c16 | \break
  <<
  {c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c | \break
  %c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c16 |
  c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4  \tuplet 3/2 {r8 c'8 c} |
  %c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c16 |
  \tuplet 3/2 { c b b b a a a g g g e d} | c2 r4. g'8~ | 
  g8 c,4 g'8~g8 c,4 g'8~ | g8 c,4 r16 c16 c16 d8 e16~ e g8 c16~  | 
  
  c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4  \tuplet 3/2 {r8 c'8 c} |
  \tuplet 3/2 { c b b b a a a g g g e d} | c2 r4.  c'16 c | 
  c8 b16 b b8  a16 a a8 g16 g g8 e16 d | c2 r4.   g'8~ |
  g8 c,4 g'8~g8 c,4 g'8~ | g c,4. r2
  }
  
  {e'8 d16 d d8  c16 c c8 b16 b b8 g16 f e2 r4.  e'16 e |
  %c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c16 |
  e8 d16 d d8  c16 c c8 b16 b b8 g16 f f2 r4  \tuplet 3/2 {r8 e'8 e} |
  %c8 b16 b b8  a16 a a8 g16 g g8 e16 d c2 r4. c'16 c16 |
  \tuplet 3/2 { e d d d c c c b b b g f} | f2 r4. c'8~ |
  c8 e,4 c'8~ c8 e,4 c'8~ | c8 e,4 r16 e16 e16 f8 g16~ g c8 e16~  | 
 
  e8 d16 d d8  c16 c c8 b16 b b8 g16 f e2 r4  \tuplet 3/2 {r8 e'8 e} |
  \tuplet 3/2 { e d d d c c c b b b g f} | f2 r4. e'16 e |
  e8 d16 d d8  c16 c c8 b16 b b8 g16 f e2 r4. c'8~ | 
  
   c e,4 c'8~ c e,4 c'8~ c8 e,4. r2
  }
  >>

  r8 e16 e e e g16 e~ e d d8~  d4 | r8 e16 e e e g16 e~ e d d8~  d4 |
  r8 e16 e e e g16 e~ e d d8~  d4 | r8 e16 e e e g16 e~ e d d8~  d4 | \break

  d'8. c16~ c16 b8 a16~ a4 r 
  \bar "|."
}

words = \lyricmode {
  
  take a mo -- ment to think o' just
  fle -- xi -- bi -- li -- ty  love and trust
   
  take a mo -- ment to think o' just
  fle -- xi -- bi -- li -- ty  love and trust
  
  here comes a thought
  that might a -- larm you
  what some -- one said
  and how it harmed you
  
  some -- thing you did
  that failed to be char -- ming
  things that you said
  are sud -- den -- ly swar -- ming
  
  and oh 
  you're lo -- sing sight
  you're lo -- sing touch
  
  all these lit -- tle things 
  seem to mat -- ter  so much
  that they con -- fuse you
  that I might lose you
  
  take a mo -- ment re -- mind your -- self to
  take a mo -- ment and find your -- self
  take a mo -- ment to ask your -- self if
  this is how we fall a -- part 
  
  but it's not but it's not
  but it's not but it's not 
  but it's not
  it's o -- kay it's o -- kay
  it's o -- kay it's o -- kay 
  it's o -- kay
  
  you've got no -- thing got no -- thing got no -- thing got no -- thing to fear
  
  I'm here  I'm here  I'm here 

  here comes a thought
  that might a -- larm me
  what some -- one said
  and how it harmed me
  
  some -- thing I did
  that failed to be char -- ming
  things that I said
  are sud -- den -- ly swar -- ming

  and oh 
  I'm lo -- sing sight
  I'm lo -- sing touch
  
  all these lit -- tle things 
  seem to mat -- ter  so much
  that they con -- fuse me
  that I might lose me

  take a mo -- ment re -- mind your -- self to
  take a mo -- ment and find your -- self
  take a mo -- ment and ask your -- self if
  this is how we fall a -- part

  but it's 
  not but it's not but it's not but it's not but it's not
  it's o -- kay it's o -- kay it's o -- kay it's o -- kay it's o -- kay 
  
  I've got no -- thing got no -- thing got  no -- thing got no -- thing to fear
  
  I'm here I'm here I'm here
  
  and it was just a thought just a thought just a thought just a thought just a thought
  it's o -- kay it's o -- kay it's o -- kay it's o -- kay it's o -- kay
  we can watch we can watch we can watch we can watch  them go by

  from here from here from here
  
  take a mo -- ment to think of just
  fle -- xi -- bi -- li -- ty love and trust
  
  take a mo -- ment to think of just
  fle -- xi -- bi -- li -- ty love and trust

  
}

chorda = \chordmode{
    f1:maj7 e2 e:7
    f1:maj7 f:dim
}

chordaa = \chordmode{
    c:maj7 c:maj7
    f:maj7 e2 e:7
    c1:maj7 c:maj7
    f:maj7 e1
}

chordb = \chordmode{
    d1:m7 g:7/d c:maj7 fs:dim
    d:m7 d:m7 f:m7+ f:m7+
}

chordc = \chordmode{
  c:maj7 f:maj7
  c:maj7 f:maj7
  c:maj7 f2:maj7 s4.
  d2:m7 f:m c:maj s8 s2
  
}


chordNames = {
  \chorda
  \chordaa
  \chordb
  
  \chorda
  \chordc
  \chordaa
  \chordb
  \chorda
  \chordc
  \chordc
  \chordmode {
    f1:maj7 e
    f:maj7 e
  }
}

\book{
  \bookOutputName "here_comes_a_thought_C"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
      \new Staff \with {instrumentName = #"(C)"}{ 
        \melody
      }
      \addlyrics { \words }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "here_comes_a_thought_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d {\chordNames}
      }
      \new Staff \with {instrumentName = #"(Bb)"}{ 
        \transpose c d {\melody }
      }
      \addlyrics { \words }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "here_comes_a_thought_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c a {\chordNames}
      }
      \new Staff \with {instrumentName = #"(Eb)"}{ 
        \transpose c a {\melody }
      }
      \addlyrics { \words }
    >>
    \layout { }
  }
}
