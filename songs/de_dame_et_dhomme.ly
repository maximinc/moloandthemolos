
\version "2.18.2"
\language "english"

\header {
  title = "De dame et d'homme"
  composer = "Marc Perrone, André Minvielle"
}

global = {
  \time 3/4
  \key c \minor
  \tempo 4=160
}

% Structure:
% A instrumental

% B chant
% A chant

% B instrumental
% C instrumental

% B chant
% A chant
% B chant




Aa = \relative f'{
  f8 e f g af bf | c b c d ef f | d8 ef16 d c8 b c d | c bf af g f ef | \break
  d cs d ef f g | af g fs g a b | c b c d c b | c4 d, e | \break 
}

Ab = \relative f'{
  <<
  { f8 e f g af bf | c b c d ef f | d8 ef16 d c8 b c d | c bf af g f ef | \break
    <<{d cs d ef f g }
      {\tuplet 3/2 {f'8 g ef} d c b af }
    >>
    | af g fs g a b | c b c d c b | c4 r r | \break 
  }
  \\
  {
    f,2 ef4~ ef4 d2 | d c4~ | c b2 | bf a4 | af g fs g a b | c c,
  }
  >>
}



Ba = \relative f'{
  g4.  af8 g fs | g4. af8 g fs | g2 r8 g       | ef' d c bf af g | \break
  f4.  g8  f e  | f4. g8  f e  | f4 f'8 ef d c | bf af g f ef d  | \break
  
}

Bfina = \relative f'{
  ef4. f8 ef d  | ef4. f8 ef d | ef2 r8     c' | ef d c bf af g | \break
  d4. ef8 d cs  | d cs d4 ef   | c4. d8 c b    | c b c d ef f  | \break
}

Bfinb = \relative f'{
  <<
  {
    \voiceOne
  ef4. f8 ef d  | ef4. f8 ef d | ef2 r8     c' | ef d c bf af g | \break
  d4. ef8 d cs  | d cs d4 ef   | c4. d8 c b    | c4   d ef      | \break
  }
  \new Voice{
    \voiceTwo
    c2. | b | bf | a | af | a2 b4 | c4. d8 c b    | c4   d ef     
  }
  >>
}


Bc = \relative f'{
  <<
  { 
    \voiceOne
  g4.  af8 g fs | g4. af8 g fs | g c, g' c, c' c,  | ef' d c bf af g | \break
  f4.  g8  f e  | f4. g8  f e  | f g f' ef d c | bf af g f ef d  | \break
  ef4. f8 ef d  | ef4. f8 ef d | ef2    c'8 d  | ef d c bf af g | \break
  d4. ef8 d cs  | d cs d4 ef   | c4. d8 c b    | c4. r8 r8 g'16 g \break
}
\new Voice{
  \voiceTwo
   s2. | s | s | s |
   s   | s | s | s |
   c,2. | b | bf | a | 
   af | a2 b4 | c4. d8 c b | c4   
}
  >>
  }

C = \relative f' {
  g8 c, e g a g f g16 f e8 c e f | g8 c, e g a g  | f g16 f e8 c d e  | \break 
  f b, d f g f | e f16 e d8 f b, d | f b, d f g f | e f16 e d8 f b, d | \break
  a' gs a b c d | c d16 c b8 c a fs | g f e f g a | g2. \startTrillSpan  |\break
  f8\stopTrillSpan  e d e f g | a e' c d \tuplet 3/2 {e d c }
  | \tuplet 3/2 { d c b } \tuplet 3/2 { a b c } \tuplet 3/2 { d c b } <g ef b>2.  | \break
}

melody = \relative g' {
  \global
  r2 e4 
  \bar "||"
  \Aa
  \Ab
  \bar "||"  
  \Ba
  \Bfina
  \Ba
  \Bfinb
  \bar "||"
  \Aa
  \Ab
  \bar "||"
  \Bc
  \bar "||"
  \C
  \bar "||"
  
  \bar "|."
  
  
}

words = \lyricmode {
}

chorda = \chordmode{ 
    f2.:m f:m c:m c:m 
    d:m7.5- g:7 c:m c:7
    f:m f:m c:m c:m
    d:m7.5- g:7 c:m c:m
}

chordb = \chordmode{ 
    c2.:m7 c:m7 c:m7 c:m7
    g:7 g:7 g:7 g:7
    c2.:m7 c:m7 c:m7 c:m7
    d:7.5- g:7 c:m7
}

chordc = \chordmode {
  c:6 c:6 c:6 c:6
  g:7 g:7 g:7 g:7 
  f fs:dim7 c/g a:7
  d:m7 d:m7/c d:m7 g:aug7
}


chordNames = {
  \chordmode{ s2. }
  \chorda
  \chordb 
  \chordmode{ c:m7 }
  \chordb 
  \chordmode{ c:7 }
  \chorda
  
  \chordb 
  \chordmode{ c:m7 }
  \chordc
}

\book{
  \bookOutputName "de_dame_et_d_homme_C"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
      \new Staff \with {instrumentName = #"(C)"}{ 
        \transpose c c {\melody }
      }
    >>
    \layout { }    
  }
}

\book{
  \bookOutputName "de_dame_et_d_homme_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d{ \chordNames }
      }
      \new Staff \with {instrumentName = #"(Bb)"}{ 
        \transpose c d {\melody }
      }
    >>
    \layout { }    
  }
}

\book{
  \bookOutputName "de_dame_et_d_homme_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c a{ \chordNames }
      }
      \new Staff \with {instrumentName = #"(Eb)"}{ 
        \transpose c a {\melody }
      }
    >>
    \layout { }    
  }
}