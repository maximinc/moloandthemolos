
\version "2.18.2"
\language "english"
\header {
  title = "Mon amoureuse"
  composer = "Mansfield Tya"
}


melody = \relative c' {
  \time 3/4
  \key bf \minor
  
  r2. |  r |  r | r |
  r2. |  r |  r | r | \break
  r4. bf8 bf df | bf'4 bf8 af bf af f4. f8 f df ef4 f ef8 df | \break
  bf4. bf8 bf df | bf'4 bf8 af bf af f4. f8 f df ef4 f ef8 df | \break
  bf4 bf8 bf bf df | bf'4 bf  af  f4. f8 f df ef4 f ef8 df | \break
  bf4. bf8 bf df | bf'4 bf8 af bf af f4 f \tuplet 3/2 {df4 ef8~} | ef4 ef8 df ef df | \break
  
  bf4 r2 | r2. | r2. | r2.
  r2.     | r2. | r2. | r2. \break
  
 
  r4.  bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break

  bf4 r2 | r2. | r2. | r2.
  r2.     | r2. | r2. | r2. \break
  
  r2 bf8 df | bf'4 af bf8 af | f4. f8 f df | ef4 f ef8 df | \break
  bf4. bf8 bf df | bf'4 bf8 af bf af f4. f8 f df ef4 f ef8 df | \break
  
  bf4 r2 | r2. | r4. f'8 f df | ef4 f ef8 df \break
  bf4 r2 | r2. | r2. | r2. \break

  r4.  bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break

  bf4 r2 | r2. | r2. | r2.
  r2.     | r2. | r2. | r2. \break
 
  r2 bf8 df | bf'4 af bf8 af | f2   f8 df | ef4 f ef8 df | \break
  bf4 r bf8 df | bf'4 af4  bf8 af f4. f8 f df ef4 f ef8 df | \break

  bf4 r2 | r2. | r2. | r2.
  r2.     | r2. | r2. | r2. \break

  r2 bf8 df | bf'4 bf af | f2   f8 df | ef4 ef df | \break
  bf4 r bf8 df | bf'4 bf  af f4. f8 f df ef4 f ef8 df | \break

  bf4 r2 | r2. | r2. | r2.
  r2.     | r2. | r2. | r2.

  r2. 
  r2.
  r2.
  r2.
  \repeat volta 1{r2. r2. }
  \bar "|."
}


partA = \relative c'{
  a4  bf4 c df c df a bf c ef df c
}

partB = \relative c'{
  bf4  bf8 df c df bf4 bf8 ef df ef
  bf4 bf8 df c df bf4 df8 df c c  
}
partC = \relative c'{
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break
}

alto = \relative c' {
  \time 3/4
  \key bf \minor
  r2. |  r |  r | r |
  r2. |  r |  r | r | \break
  r4 bf4 c df c df a bf c ef df c
  \partA \break
  \partA
  \partA \break
  %a  bf4 c df c df a bf c ef df c
  %a  bf4 c df c df a bf c ef df c
  %a  bf4 c df c df a bf c ef df c

  \partB
  \partB \break
%  bf4  bf8 df c df bf4 bf8 ef df ef
%  bf4  bf8 df c df bf4 df8 df c c
%  bf4  bf8 df c df bf4 bf8 ef df ef
%  bf4  bf8 df c df bf4 df8 df c c
 
  \partC
  \partC \break
%  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
%  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break
%  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 f8 ef~ ef df \break
%  bf4. bf8 df f | bf2  af4 | f r4 f8 df8 ef4 ef df \break

 \partB
 \partB \break
 
 \partA
 \partA \break

 \partB
 \partB \break
 
 \partC
 \partC \break
 
 \partB
 \partB \break
 
 \partA
 \partA \break
 
 \partB
 \partB \break
 
 \partC
 \partC \break
 
  bf4  bf8 df c df
  bf4  bf8 df c df
  bf4  bf8 df c df
  bf4  bf8 df c df 
  
  \repeat volta 2{
    bf4 bf8 bf~ bf bf
    bf4 bf8 bf~ bf bf
  }
  \break
}

words = \lyricmode {
  Quelle é -- tait belle ma môme  o -- pal -- ine
  Vue sur une prai -- rie so -- li -- taire
  Ma bel -- le tendre sculp -- ture an -- dro -- gyne
  Tu n'in -- si -- stas pas pour me plaire
  Je n'suis pas de celles qui bon -- dissent
  Sur le pre -- mier bi -- jou sou -- riant
  Du temps il me faut pour que j'a -- gisse
  Coeur de mousse mo -- dère ses batte -- ments
  
  Sous un -- e pluie mo -- queuse
  Je -- e te vois t'é -- loi -- gner
  De moi mon a -- mou -- reuse
  Telle un -- e mer pres -- sée

  Sous un -- e pluie mo -- queuse
  Je -- e te vois t'é -- loi -- gner
  De moi mon a -- mou -- reuse
  Telle un -- e mer pres -- sée

  Où cours -tu ma fu -- gi -- tive?
  Mon in -- dé -- ci -- sion t'a gla -- cée
  Mais elle te ré -- clame mon âm -- e vive
  Tu m'as trop vite a -- ban -- don -- née
  Tu m'as trop vite a -- ban -- don -- née

  Sous un -- e pluie mo -- queuse
  Je -- e te vois t'é -- loi -- gner
  De moi mon a -- mou -- reuse
  Telle un -- e mer pres -- sée

  Sous un -- e pluie mo -- queuse
  Je -- e te vois t'é -- loi -- gner
  De moi mon a -- mou -- reuse
  Telle un -- e mer pres -- sée

  An -- cré dans ma peau pen -- sive
  J'ai le sent -bon de l'in -- grate
  Une a -- bsurde ran -- coeur a -- vive
  L'o -- deur d'une i -- vresse é -- car -- late
  
  Elle em -- porte a -- vec elle
  Ses s pen -- sées se -- crètes
  Ses dra -- pés, ses den -- telles
  Ses cen -- dres de ci -- ga -- a -- rettes 
  
}


cycle = \chordmode {
  bf2.:m ef:m df ef:m 
  bf2.:m ef:m df ef:m 
  bf2.:m ef:m df ef:m 
  bf2.:m ef:m df ef:m 
}

grid = {
    \cycle
    \cycle
    \cycle
    \cycle
    \cycle
    \cycle
    \cycle
    \cycle
    \chordmode{ bf:m }
}


\book {
  \bookOutputName "mon_amoureuse_C"
  \score {
    <<  
    \new ChordNames { 
      \grid
    }
    \new Staff  \with {instrumentName = #"Voice (C)"}{ 
      \melody
      \addlyrics { \words }
    }
    \new Staff \with {instrumentName = #"Alto (C)"}{ 
      \alto
    }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "mon_amoureuse_Bb"
  \score {
    <<  
    \new ChordNames { 
      \transpose c d {\grid}
    }
    \new Staff  \with {instrumentName = #"Voice (Bb)"}{ 
      \transpose c d {\melody}
      \addlyrics { \words }
    }
    \new Staff \with {instrumentName = #"Alto (Bb)"}{ 
      \transpose c d {\alto}
    }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "mon_amoureuse_Eb"
  \score {
    <<  
    \new ChordNames { 
      \transpose c a {\grid}
    }
    \new Staff  \with {instrumentName = #"Voice (Eb)"}{ 
      \transpose c a {\melody}
      \addlyrics { \words }
    }
    \new Staff \with {instrumentName = #"Alto (Eb)"}{ 
      \transpose c a {\alto}
    }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "mon_amoureuse_clef_C"
  \score {
    <<  
    \new ChordNames { 
      \transpose c c {\grid}
    }
    \new Staff  \with {instrumentName = #"Voice (C)"}{ 
      \clef C
      \transpose c c {\melody}
      \addlyrics { \words }
    }
    \new Staff \with {instrumentName = #"Alto (C)"}{ 
      \clef C
      \transpose c c {\alto}
    }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}
