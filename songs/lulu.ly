\version "2.20"
\language "english"


\header {
  title = "Lulu le ver luisant"
  composer = "Anne Sylvestre"
}


theme={
  \time 2/4
  \tempo 4=160
  \clef G
  \key b \minor
  r2 r2 r2 r2
  r2 r2
  r2 
  \relative c'{
    r4 fs8 fs 
    \break
    \repeat volta 1{
      fs e d cs b4 
       fs'8 fs fs e d cs b4 
       fs'8 fs fs e d cs b4 
       b8 b e8. d16 e8 g fs4
       fs8 fs fs8. fs16 e8 d e2 r2
       r8. e16 e8 d e4 fs d8. d16 d8 b cs4 d b2 r
    r4 b8 b b d cs b a4 a8 a a cs b a b4
       b8 b b d cs b cs4 cs8 cs cs e d cs d4
       d8 d d fs e d cs2~ cs8. b16 cs8 d cs2~ cs2 fs2~ fs4 r8
       
       g8
       fs4 r8 d8 e8 d8 e8 g
       fs4 r8 d8 e8 d8 e8 g
       fs8. fs16 e8 d cs2~ cs
       fs2 r4. 
       g8
       fs4 r8 d8 e8 d8 e8 g
       fs4 r8 d8 e8 d8 e8 g
       fs8. fs16 e8 d cs2~ cs
       
       b4 \coda r r r r r r fs'8 fs
    }
    \break
  }
  
  \relative c'{
    b2 \coda r4. 
    g'8
    fs4 r8 d8 e8 d8 e8 g
    fs4 r8 d8 e8 d8 e8 g
    fs8. fs16 e8 d cs2~ cs
    fs2 r4. 
    g8
    fs4 r8 d8 e8 d8 e8 g
    fs4 r8 d8 e8 d8 e8 g
    fs8. fs16 e8 d cs2~ cs

    b2 r r
  }
  \bar "|."
}

cla={
  \key b \minor
  \time 2/4
  \relative c'{
    b4\fermata \tuplet 3/2 {cs8 d e} f4 <b f'>
    b4\fermata \tuplet 3/2 {a8 gs b} f4 <b d>
    f fs4 
    
    <fs'  d b>2
  }
  r2
  r2 
  
  \relative c'{
    cs2 d cs4 a d r r8 cs d e d4. b8 a4 g'8 cs, 
    d8 a'8-. fs-. d-. d'4 r r8 cs,8 b fs' a e b' fs a4
    r8 b, cs fs cs' a fs4 r r
    as, b8 d fs b d\> b fs' a b4  \! r
  }
  
  \relative c'{
    r2 r4 a e' a r b~b~b8 a8 fs4 r r8 cs b cs d4 d' a fs
    r4  <a cs>4~<a cs>2 r8 a8 fs a fs a cs, fs a,4 r
    e''8 cs as as' b4
    d,8 b a4 r r fs'8 d cs4 r r8 fs d fs f fs, b cs b e fs e
    cs4 b8 cs
    fs8 f fs as b4
    r
    r2 r4 b,8 fs' e4 g8 e d8 fs d fs, b b b b as2
    r4 \coda d8 b fs fs' b, b' r8 g a a b b r4
  }
  
  \relative c'''{
    r4\coda
    d8 b a4 e  fs8 a fs d e2
    d2 cs r r r r r r r r r r r r 
  }
  \relative c'{
    r8 fs16 g e8 e16 fs d8 d16 e cs b cs d b4 r
  }
  \bar "|."
}

bass={
  \key b \minor
  \time 2/4
  \clef F

  r2 r2
  r2 r2
  r2 r4
  \relative c{
    fs4 b, fs'
    b4 d8 b 
    \repeat volta 1{
    a4 fs 
    b, d'8 b a4 fs
    b, d'8 b a4 fs
    b, fs'8 b cs4 a, 
    d a' d d,8 e 
    fs4 cs' fs, cs 
    fs, cs' fs a 
    b a g fs 
    b, a' af g8 d
    b4 fs' b, d fs fs, cs'8 a' fs4
    b, b' a g8 b e,4 e a, a'8 g 
    fs4 g fs a8 e a,4 e' a g
    fs fs f f e e cs fs
    b, b' cs cs, d d'
    a, cs' d a af af g g fs e cs fs
    b, b' cs cs, d d' cs a, d d8 e
    fs4 cs fs, fs' 
    b, \coda b' a af g a b8 b r4
    }
  }
  \relative c{
    b4 \coda d e fs b, d' cs cs, d d' d d d, a' af af g g 
    fs e d cs8 fs, b4 b' cs cs, d d' cs a, d8 d e4 fs fs, fs' fs,
    b b' b, b' b, r4
  }
  \bar "|."
}

chordNames={
  \chordmode{
    s2 s s s s s s s
    fs:m b:m fs:m b:m
    fs:m b:m  a:7  d d
    fs:7 fs:7 fs:7 fs:7 b:m fs:7 b:m
    b:m b:m b:m
    fs:m fs:m b:m b:m
    a:7 a:7  d d
    a a
    fs:7 fs:7 fs:7 fs:7
    
    b:m a d a b:m
    cs:7 cs:7 fs:7 fs:7
    b:m a d a d fs:7 fs:7 b:m
    s b:m s
    b:m b:m
    b:m a d a d cs:7 cs:7 fs:7 fs:7
    b:m a d a d fs:7 fs:7 b:m
    
  }
}

\book {
  \bookOutputName "lulu_all"
  \score{
    <<
       \new ChordNames { 
         \transpose c c { \chordNames }
       }
  
      \new Staff \with { instrumentName = #"Chat (C)" } {
        \set Staff.midiInstrument = "lead 6 (voice)"
        \transpose c c {
          \theme
        }
      }            
  
      \new Staff \with { instrumentName = #"Charinette (C)" } {
        \set Staff.midiInstrument = "clarinet"
        \transpose c c{
          \cla
        }
      }
  
      \new Staff \with { instrumentName = #"Chat-basse (C)" } {
        \set Staff.midiInstrument = "fretless bass"
        \bass
      }            
    >>
    \layout { }
  }
}


\book {
  \bookOutputName "lulu_Bb"
  \score{
    <<
       \new ChordNames { 
         \transpose c c { \chordNames }
       }
  
      \new Staff \with { instrumentName = #"Chat (Bb)" } {
        \set Staff.midiInstrument = "lead 6 (voice)"
        \transpose c d{
          \theme
        }
      }            
  
      \new Staff \with { instrumentName = #"Charinette (Bb)" } {
        \set Staff.midiInstrument = "clarinet"
        \transpose c d{
          \cla
        }
      }
  
%       \new Staff \with { instrumentName = #"Chat-basse (C)" } {
%         \set Staff.midiInstrument = "fretless bass"
%         \bass
%       }            
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "lulu_C"
  \score{
    <<
       \new ChordNames { 
         \transpose c c { \chordNames }
       }
  
      \new Staff \with { instrumentName = #"Chat (C)" } {
        \set Staff.midiInstrument = "lead 6 (voice)"
        \transpose c c{
          \theme
        }
      }            
  
      \new Staff \with { instrumentName = #"Charinette (C)" } {
        \set Staff.midiInstrument = "clarinet"
        \transpose c c{
          \cla
        }
      }
  
%       \new Staff \with { instrumentName = #"Chat-basse (C)" } {
%         \set Staff.midiInstrument = "fretless bass"
%         \bass
%       }            
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "lulu_bass"
  \score{
    <<
       \new ChordNames { 
         \transpose c c { \chordNames }
       }
  
      \new Staff \with { instrumentName = #"Chat (C)" } {
        \set Staff.midiInstrument = "lead 6 (voice)"
        \transpose c c{
          \theme
        }
      }            
  
%       \new Staff \with { instrumentName = #"Charinette (Bb)" } {
%         \set Staff.midiInstrument = "clarinet"
%         \transpose c d{
%           \cla
%         }
%       }
  
      \new Staff \with { instrumentName = #"Chat-basse (C)" } {
        \set Staff.midiInstrument = "fretless bass"
        \bass
      }            
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}