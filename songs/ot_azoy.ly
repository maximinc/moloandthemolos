\version "2.18.2"
\language "english"
\header {
  title = "Ot Azoy"
}

introchords=\chordmode{
  b4:m fs:7 b:m e:m b:m fs:7 b2:m
  b4:m fs:7 b:m e:m b:m fs:7
}
refrainchords=\chordmode{
  b1:m b2:m fs4:7 b4:m
}
coupletchords=\chordmode{
  d1 fs2:7 b:m
}
pontchords=\chordmode{
  b1:m b4:m fs:7 b:m fs:7
  b1:m b4:m fs:7 b:m fs:7
  d1 d b:m b:m d d b4:m fs:7 b:m e:m b4:m/fs fs4:7 b2:m
  
  d1 d
  a  a2 d4 a8 fs:7
  b1:m b:m b:m 
  a2:m b4:m fs:7
}

introFannyEle=\relative c'''{
  b,16^"Fanny" fs b d cs fs, cs' e d b d fs e b e g fs g fs e d cs d e fs8 b b4
  b,,,16^"Élé" fs b d cs fs, cs' e d b d fs e b e g \time 2/4 fs g fs e d e d cs  \break
  \time 4/4
}

introFanny=\relative c''{
  b16^"Fanny" fs b d cs fs, cs' e d b d fs e b e g fs g fs e d cs d e fs8 b b4
  b,16 fs b d cs fs, cs' e d b d fs e b e g \time 2/4 fs g fs e d e d cs \break
  \time 4/4
}

introLeo=\relative c''{
  b16^"Léo" fs b d cs fs, cs' e d b d fs e b e g fs g fs e d cs d e fs8 b b4
  b,16 fs b d cs fs, cs' e d b d fs e b e g \time 2/4 fs g fs e d e d cs \break
  \time 4/4
}
introLeoFanny=\relative c''{
  b16^"Léo" fs b d cs fs, cs' e d b d fs e b e g fs g fs e d cs d e fs8 b b4
  b,16^"Fanny" fs b d cs fs, cs' e d b d fs e b e g \time 2/4 fs g fs e d e d cs \break
  \time 4/4
}

refrain=\relative c'{
  \repeat volta 2 {
    r8 <b d fs> r8 <b d fs> r8 <b d fs> r8 <b d fs>
    r8 <b d fs> r8 <b d fs> r8 <as cs fs> r8 <b d fs>
    \break
  }
}
couplet=\relative c'{
  \repeat volta 2{
    r8 <a d fs> r8 <a d fs> r8 <a d fs> r8 <a d fs>
    r8 <as cs fs> r8 <as cs fs> r8 <b d fs> r8 <b d fs>
    \break
  }
}
pont=\relative c{
  \repeat volta 2{
    fs16 b b d d fs fs16 d fs e e d fs4 a8 g16 fs g8 fs16 e d8 b' a4
    fs,16 b b d d fs fs16 e fs16 e e d b'4 a16 g fs e d8 cs b4 r
  }
  \break
  \repeat volta 2{
    fs'16 g a4 a8 a16 b a b a4 d,16 e fs4 fs8 fs16 g fs g fs4 \break
    b,16 cs d4 e8 e fs16 e d8 e fs4 (fs8) a16 g fs2 \break
    fs16 g a4 a8 a16 b a b a4 d,16 e fs4 fs8 fs16 g fs g fs4 \break
    b,16 cs d b cs d e cs d e fs d e fs g e fs g e fs d e cs d b2 \break
  }
  \repeat volta 2{
    d4.^"Fanny puis Léo" e16 fs d4. e16 fs d8 e16 fs d8 e16 fs d8. fs16 a g fs e \break
    e4. fs16 g a,4. a'8 a16 g fs e fs g e fs d4 fs16 e^"ou" d cs \break
  }
  \repeat volta 2{
    b4 e16 d cs e d4 (d8) cs b4 g'16 fs e g fs4.d8 \break
    b4 e16 d cs e d4. d8 d16 a a d d c c b b fs' e g fs16 e d cs \break
  }
}




all_chords=\new ChordNames{
  \introchords
  \refrainchords
  \coupletchords
  
  \introchords
  \refrainchords
  \coupletchords

  \introchords
  \refrainchords
  \coupletchords

  \introchords
  \refrainchords
  \coupletchords
  
  \pontchords

  \introchords
  \refrainchords
  \coupletchords

  %\introchords
  %\refrainchords
  %\coupletchords

  \introchords
}

all_theme={
  \key b \minor
  
  \introFannyEle
  \refrain    
  \couplet
  
  \introFanny
  \refrain
  \couplet
  
  \introLeo
  \refrain
  \couplet

  \introLeoFanny
  \refrain
  \couplet
   
  \pont
   
  \transpose c c,{\introFanny}
  \refrain
  \couplet
  
  %\introLeo
  %\refrain
  %\couplet
  \introLeoFanny
  b'8^"Ot Azoy" b' b'4 r r
  \bar "|."
}



bassrefrain={
  \time 4/4
  \repeat volta 2{
    \relative c'{
      b4 fs' b,4 fs'
      b b fs8 fs, b4
    }
  }
  \repeat volta 2{
    \relative c'{
      d4 a d d8 e fs4 fs, b b8 cs 
    }
  }
}
bassintro={
  \relative c'{
    b4 fs b e b fs' b8 b fs d
    b4 fs b e 
    \time 2/4
    fs fs,
  }
}

basspont={
  \relative c'{
    \repeat volta 2{
      b4 fs b fs b fs8 as b4 fs'
      b, fs b fs' b, fs' b8 b fs d 
    }
    \repeat volta 2{
      d4 a8 cs d4 a d a8 cs d4 cs
      b b b b b b b b
      d4 a8 cs d4 a d a8 cs d4 d8 cs    
      b4 fs b e fs fs, b b8 cs 
    }

    \repeat volta 2{
      d4 a8 cs d4 a8 cs d4 a8 cs d8 d cs b
      a4 cs8 e a4 e8 cs a a b cs d a 
      
      <<
        {fs'16 e d cs}
        \\
        {b8 cs}
      >>
    }
    \repeat volta 2{
      b4 fs b fs' b, fs b fs'8 fs, b4 fs b b a c b fs 
    }
  }
}

bass={
  \clef F
  \key b  \minor

  \bassintro
  \bassrefrain
  \bassintro
  \bassrefrain
  \bassintro
  \bassrefrain
  \bassintro
  \bassrefrain
  \basspont
  \bassintro
  \bassrefrain
  \bassintro
}


\book {
  \bookOutputName "ot_azoy_all"
  \score{
    <<
      \transpose a c{
        \all_chords
      }
      \transpose a c'{
        \all_theme
      }
      \new Staff \with {instrumentName = #"bass"}{
        \tempo 4=140
        \transpose a c {\bass}
      }
    >>
  \layout {}
  \midi {}
  }
}


\book {
  \bookOutputName "ot_azoy_C"
  \score{
    <<
      \transpose a c{
        \all_chords
      }
      \transpose a c'{
        \all_theme
      }
    >>
  }
}

\book {
  \bookOutputName "ot_azoy_Bb"
  \score{
    <<
      \transpose a d{
        \all_chords
      }
      \transpose a d'{
        \all_theme
      }
    >>
  }
}

\book {
  \bookOutputName "ot_azoy_Eb"
  \score{
    <<
      \transpose a a{
        \all_chords
      }
      \transpose a a'{
        \all_theme
      }
    >>
  }
}

\book {
  \bookOutputName "ot_azoy_C_clef_C"
  \score{
    <<
      \transpose a c{
        \all_chords
      }
      \transpose a c{
        \clef "C"
        \all_theme
      }
    >>
  }
}

