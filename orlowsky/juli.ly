
\version "2.18.2"
\language "english"

\header {
  title = "Juli"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 4/4
}

\paper {
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 6)
       (padding . 0.4)
       (stretchability . 60))
}

chordNames={
  \chordmode{
    f1:m  f1:m
    f:m c:m f:m ef
    f:m c:m f:m ef
    g:m d:m g:m f
    a1:m g2 d g1 e:7
    a1:m g2 d b1:7 e:m
    
    f:m c:m f:m ef
    f:m c:m f:m ef
    
    g:m d:m g:m f
    a1:m g2 d g1 e:7
    a1:m g2 d b1:7 e:m

    c:m f:m c:m g:m
    d:m g:m d:m a:m
    
    fs:m fs:m gs:m gs:m
    fs:m fs:m gs:m gs:m
    fs:m ds2:m7.5- cs:m gs:7 cs1:m cs1:m
    
    fs:m cs:m fs:m e
    fs:m cs:m fs:m e
    
    af:m gf2 df gf1 ef:7
    af:m gf2 df bf1:7 ef:m

    c:m f:m c:m g:m
    d:m g:m d:m a:m

    fs:m fs:m gs:m gs:m
    fs:m fs:m gs:m gs:m
    fs:m ds2:m7.5- cs:m gs:7 cs1:m

    e1 gs:m e gs:m
    d cs:m d cs:m

    e1 gs:m e gs:m
    d cs:m gs:7 cs:m
    
    cs:m
    fs:m cs:m fs:m e
    fs:m cs:m fs:m e
    
    af:m gf2 df gf1 ef:7
    af:m gf2 df bf1:7 ef:m
   
    c:m f:m c:m g:m
    d:m g:m d:m a:m

    c:m f:m c:m g:m
    d:m g:m d:m a:m

    c:m f:m c:m g:m
    d:m g:m d:m a:m
    c:m f:m c:m c:m
  }
}

themeA={
  \relative c''{
    \key c \minor
    af4 f4 r8 af8 g f ef d c b c af'16 af16 af8 af
    af4 f4 r8 af16 af16 g8 f g f g4 r8 af16 af16 af8 af8 \break
    af4 f4 r8 af8 g f ef d c b c af'16 af16 af8 af
    af4 f4 r8 af16 af g8 f g af bf4 r8 bf16 bf bf8 bf \break
    \key d \minor
    bf4 g4 r8 bf8 a g f e d cs d bf'16 bf bf8 bf 
    bf4 g d' bf f2 r8 c'16 c c8 c
    \break
    \key e \minor
    c4 a4 r8 d8 b a g fs g a fs4 g8 a b4 b d cs8 d b2 r8 c16 c c8 c\break
    c4 a4 r8 d8 b a g fs g a fs4 g8 a 
  }
}

themeB={
  \relative c''{
    c4 af4 r8 c8 bf af g f ef d ef c'16 c16 c8 c

    c4 af4 r8 c8 bf8 af g af bf4 r8 c8 c c \break
    c4 af4 r8 c8 bf af g f ef d ef c'8 c8 c
    c4 af4 r8 c  bf8 af bf af bf4 r8 d d d \break
    %\key d \minor
    bf4 d4 r8 g8 f ef d c bf a bf d d d 
    d4 bf2 d4 a2 r8 f'16 f f8 f
    %\key e \minor
    e4 c4 r8 f8 d c b a b c a4 g8 a b1  b2 r8 e e e\break
    e4 c4 r8 f8 d c b a b c a4 r4 ds b as b g4 r
  }
}

themeD={
  \relative c''{
    c8 d 
    \key c \minor
    \break
    ef d c4 g' f8 g af g f4 c'
    c,8 d ef d c d g4 fs d2
    r4 d8 e
    \key d \minor
    \break
    f e d4 a' g8 a bf a g4 d'
    d,8 e f e d e a4 gs e2 r2
  }
  \break
}

themeE={
  \relative c''{
    \key cs \minor
    a8^\pp gs fs gs a4 a a8 gs a cs a4 r
    b8 as gs as b4 b b8 as b ds b4 r \break
    a8 gs fs gs a4 a~ a8 gs a cs a4 r
    b8 as gs as b4 b b8 as b as b as b as\break
    a8 gs fs gs a4 fs8 e ds e fs4 e8 ds cs e
    \time 2/4
    ds cs bs ds 
  }
}

clarinet={
  \key c \minor
    \new CueVoice {
      \relative c'{
        f8 f <af c> <af c>
        f8 f <af c> <af c>
        f8 f <af c> <af c>
        f8
      }
    }
    \relative c''{
      af8 af af 
      \break
      \themeA
      \relative c''{
        b4. g8 fs4 g  e  r 
      }
      r8 <c af'>8 <c af'> <c af'>\break
      <<
      \transpose c c' {
        \themeA
        \relative c''{
          b4 a g fs e  r 
        }
      }
      \\
      \themeB
      >>
      r4
    }
    
    \themeD
    %{\relative c''{
      c8 d 
      \key c \minor
      \break
      ef d c4 g' f8 g af g f4 c'
      c,8 d ef d c d g4 fs d2
      r4 d8 e
      \key d \minor
      \break
      f e d4 a' g8 a bf a g4 d'
      d,8 e f e d e a4 gs e2 r2
    }
    \break
    %}
    

    \themeE
    %{
    \relative c''{
      \key cs \minor
      a8^\pp gs fs gs a4 a a8 gs a cs a4 r
      b8 as gs as b4 b b8 as b ds b4 r \break
      a8 gs fs gs a4 a~ a8 gs a cs a4 r
      b8 as gs as b4 b b8 as b as b as b as\break
      a8 gs fs gs a4 fs8 e ds e fs4 e8 ds cs e 
      \time 2/4
      ds cs bs ds 
    }
    %}
    \time 4/4
      \relative c' {cs4}
      \new CueVoice {
        \relative c'{
                 <e gs>8 <e gs>
          cs8 cs <e gs> <e gs>
          cs8 cs <e gs> <e gs>
          cs8
        }
      }
    \relative c''{
      a16 a a8 a \break
      a4 fs r8 a16 a gs8 fs e16 e ds8 cs bs cs a'16 a a8 a
      a4 fs r8 a gs fs gs fs gs4 r8 a16 a a8 a \break
      a4 fs r8 a16 a gs8 fs e16 e ds8 cs bs cs a'16 a a8 a
      a4 fs r8 a gs fs gs a b4 r8 cf16 cf cf8 cf \break
      \key ef \minor 
      cf4 af r8 df bf af gf f gf af f4 gf8 af bf4 bf df c8 df bf2 r8 cf16 cf cf8 cf \break
      cf4 af r8 df bf af gf f gf af f4 gf8 af bf4 r8 af bf af gf f ef2 r4 
    }
    
    \themeD
    
    \themeE
    \time 4/4
    \relative c'{
      cs2 \fermata
    }
    
    \relative c'{
      r4 e'^"Luc"~
      \break
      
      e4 e8 ds e4 b' b gs ds2
      e4 e8 ds e4 b' ds,1 \break
      d4 d8 fs a4 gs8 fs gs4 gs8 fs gs2
      d4 d8 fs a4 gs8 fs gs2 r \break

    }

    <<
    \relative c''{
      e4^"+ Fanny en bas" e8 ds e4 b' b gs ds2
      e4 e8 ds e4 b' ds,1 \break
      d4 d8 fs a4 gs8 fs e ds cs4 cs2
      gs'4 fs e ds cs1 \break
    }
    \\
    \relative c'{
      e4 e8 ds e4 b' b gs ds2
      e4 e8 ds e ds e b' ds,1 \break
      d4 d8 fs a4 gs8 fs gs8 fs e ds cs2
      gs'4 fs e ds 
      <<
        {cs1}
        \\
        \new CueVoice {
        \stemDown
        \relative c'{
          cs8 cs <e gs> <e gs>
          cs8 cs <e gs> <e gs>
        }
      }
      >>
    }
    >>
    \new CueVoice {
      \stemDown
      \relative c'{
        cs8 cs <e gs> <e gs>
        cs8
      }
    }
  \relative c'' {
    
    
     a16 a a8 a
     a4 fs r8 a gs fs e ds cs bs cs a'16 a a8 a 
     a4 fs r8 a gs fs gs fs gs4 r8  a16 a a8 a \break
     a4 fs r8 a gs fs e ds cs bs cs a'16 a a8 a 
     a4 fs r8 a gs fs gs a b4 r8 cf16 cf cf8 cf
     
     \key ef \minor
     cf4 af4 r8 df bf af gf f gf af f4 gf8 af bf4 bf df c8 df bf4 r r8 cf16 cf cf8 cf \break
     cf4 af4 r8 df bf af gf f gf af f4 gf8 af bf4. af8 bf af gf8 f ef4 r r
  }

  \relative c''{
    c8 d 
    \key c \minor
    \break
    ef d c4 g' f8 g af g f4 c'
    c,8 d ef d c d g4 fs d2
    r4 d8 e
    \key d \minor
    \break
    f e d4 a' g8 a bf a g4 d'
    d,8 e f e d e a4 gs a a a c,8 d 
    \break
    \key c \minor 
    ef d c4 g' f8 g af g f f f f f  f
    ef d c d ef4 c d2 g,
    \break
    \key d \minor
    f'8 e d4 a' g8 a bf a g a d4
    d,8 e f e d e a4 gs e2 r \break
    
    \key c \minor
    g1 af c d
    \break
    d g f e2. 
    c,8 d 
    
    \break
    ef d c4 g' f8 g af g f4 c'
    c,8 d ef d c ef d c b d c4 r c' r \bar "|."

  }

}

bass={
  \clef F
  \key c \minor
  r1 r r r r r r r r
  
  \new CueVoice{
    r2 r8 bf16^"Fanny" bf bf8 bf 
  }

  \key d \minor
  \break
  \relative c'{
    g1 f2. e4 d1 c
    \break
    \key e \minor
    a b4 c d2 g1 e'4 d c b
    \break
    a1 b,4 c d2 ds1 e4 b e r
    
    \break
    \key c \minor 
    f4. f8~ f4 f
    c4. c8~ c4 c
    f4. f8~ f4 g
    ef4. ef8~ ef4 ef

    \break
    f4. f8~ f4 f
    c4. c8~ c4 c
    f4. f8~ f4 f
    ef4. ef8~ ef4 ef
    
    \break
    \key d \minor

    g4. g8~ g4 bf
    d,4. d8~ d4 f4
    g4. g8~ g4 bf 
    f4. f8~ f4  e
    
    \key e \minor
    \break
    a,4. a8~ a4 e'
    g4. d8~ d4 d
    g4. g8 g g g g 
    r8 e' d4 c b
    
    \break
    a4. a8~ a4 b
    g4. g8 d4. d8
    b4. fs'8  b4. ds,8
    
    e4 e d c 
    
    \break
    \key c \minor
    c2 g f' c
    c g r8 g' g g d4 d
    \break
    d2 a g' d d a'
    r8 a a a e4 e4
    
    \break
    \key cs \minor 
    fs4 fs cs' cs 
    fs, fs cs' cs
    gs ds' gs, ds' gs, ds' gs, ds'
    
    \break
    fs, cs' fs, cs' fs, cs' fs, cs'
    gs ds' gs, ds' gs, ds' gs, ds'
    
    \break
    fs, cs' fs, cs'
    ds, ds cs cs 
    \time 2/4
    gs'  bs 
    \time 4/4
    cs
    r4 r2 r1
    
    \break
    fs4. fs8~ fs4 fs 
    cs4. cs8~ cs4 cs 
    fs4. fs8~ fs4 fs 
    e4. e8~ e4 e 
    
    \break
    fs4. fs8~ fs4 fs 
    cs4. cs8~ cs4 cs 
    fs4. fs8~ fs4 fs 
    e4. e8~ e4 e 
    
    \break
    \key ef \minor
    af,4. af8~ af4 af
    gf4. df8~ df4 df
    gf4. gf8~ gf4 df'8 d 
    ef4 df cf bf
    
    \break
    af4. af8~ af4 ef8 f
    gf4. df8~ df4 df
    bf4. f'8 bf4. f8
    ef2 bf
    
    \break
    \key c \minor
    c2 g f' c 
    c g g'8 a bf4 a g
    
    \break
    \key d \minor
    d2 a g' d d a4. g'8 a g e d a4 e'
    
    \break
    \key cs \minor

    fs4 fs cs' cs 
    fs, cs' fs, cs'
    gs ds' gs, ds' gs, ds' gs, ds'
    \break
    fs, cs' fs, cs' fs, cs' fs, cs'
    gs ds' gs, ds' gs, ds' gs, ds'
    \break
    fs, cs' fs, cs'
    ds,2 cs 
    \time 2/4 
    gs' 
    \time 4/4
    cs r2
  }
  
  \break
  \relative c{
    e1 gs e gs
    d2 a'4 b cs1
    d1 cs 
    
    \break
    e,1 gs e gs
    d cs
    gs cs
  }
  r r r r r r r r r
  
  \break
  \key ef \minor
  \relative c'{
    af4. af8 r4 r
    gf4. df8~ df4 df
    gf4. gf8~ gf4 f8 e
    ef4. ef8~ ef4 bf'
    
    \break
    af4. af8~ af4 bf 
    gf4. df8~ df4 df4
    bf4. f'8 bf4. f8
    ef2 bf'
  }
  \break
  \key c \minor
  \relative c'{
    c4 g c g f c' f, c' 
    c g c g g d g d 
    
    \break
    \key d \minor
    d a' d, a' g d g d 
    d a' d, a' a8 g e4 d8 c a4
    
    \break
    \key c \minor
    c4 g c g f c' f, c' 
    c g c g g' d bf g 
    
    \break
    \key d \minor
    d' e f d g a bf g 
    d f d a
    a a c c
    
    \break
    \key c \minor
    c g c d8 ef f4 g af bf c g ef c
    g'8 bf a g f ef d cs
    
    \break
    d4 a d a g' d g d d a d a a a bf b
    
    \break
    c g c d8 ef f4 c f c	c g d' df c r c r
    \bar "|."
    
  }
}

\book{
  \bookOutputName "juli_all"
  \score {
    <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }
    
    \new Staff  \with {
      instrumentName = #"Clarinet (C)"
    }{ 
     \global
     \transpose c c { 
       \clarinet
     }
    }
    \new Staff  \with {
       instrumentName = #"Bass (C)"
    }{ 
       \global
       \bass
    }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "juli_C"
  \score {
    <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }
    
    \new Staff  \with {
      instrumentName = #"Guitar (C)"
    }{ 
     \global
     \transpose c c { 
       \clarinet
     }
    }
    >>
    \layout { 
      #(layout-set-staff-size 17)
    }
  }
}

\book{
  \paper{
    page-count = 3
  }
  \bookOutputName "juli_Bb"
  \score {
    <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }
    
    \new Staff  \with {
      instrumentName = #"Clarinet (Bb)"
    }{ 
     \global
     \transpose c d { 
       \clarinet
     }
    }
    >>
    \layout { 
      #(layout-set-staff-size 17)
    }
  }
}

\book{
  \bookOutputName "juli_bass"
  \score {
    <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }
    \new Staff  \with {
       instrumentName = #"Bass (C)"
    }{ 
       \global
       \bass
    }
    >>
    \layout { }
  }
}
