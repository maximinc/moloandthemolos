
\version "2.18.2"
\language "english"

\header {
  title = "Lyra"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 4/4
}

chordNames = {
  \chordmode {
    s1 s s s s
    s s s s s 
    
    s
    g1   b1:m e:m b:m
    g1   b2:m d e:m9 d a b:m 
    s2 
    g1   b1:m e:m b:m
    g1   b2:m d e:m9 d a b:m 

    af1 c:m g:m f:m
    af  c:m g:m f:m
    
    df  f:m   c:m c:m 
    df  f:m   f:m  c
    bf:m df  f:m f:m
    bf:m df  f:m f:m
    
    bf2 c d1:m g:m d:m
    bf2 c d:m f g:m f c d:m
    
    d:m bf bf  d:m d:m
    g1:m d:m
    bf  d2:m f g:m f c d:m
    
    s4.
    e2.:m b:m
    e2.:m b4.:m
    a2.:m e:m
    b:m a:m b:m
    e:m d4. a g a b2.:m	
    
    
    b4.:m e:m a b:m
    b4.:m e:m a d
    b:m e:m fs:m b:m
    b:m fs:m
    d e:m a d d fs:m/cs
    
    b:m b:m d  a
    b:m g a b:m  
    b:m b:m d  a
    b:m g a b:m
    
    
    af1 c:m g:m f:m
    af  c:m g:m f:m
    
    df  f:m   c:m c:m 
    df  f:m   f:m  c
    bf:m df  f:m f:m


    bf2 c d1:m g:m d:m
    bf2 c d:m f g:m f c d:m
    
    d:m bf bf  d:m d:m
    g1:m d:m
    bf  d2:m f g:m f c d:m

  e4.:m c a:m e:m e:m c d g
  e:m a:m b:m e:m  g b:m
  g a:m d g s b:m
 
  e:m c d c
  e:m c d e:m

  e:m c d c
  e:m c d e:m

  }
}


 
guitarintro = \relative c'{
  \key b \minor  
  b'4 b8 fs <b, fs' b b b>4 
  fs''4 | <fs b, b,>16 e fs8~ fs4 r 
  b,8 g' <fs b, e,,>4 \mordent e\mordent  \grace d16 e4  r|
  <d g, g,>4 cs8 d e4 e4  
  <d a 	  a,> cs cs r | \break
  
  b4 b8 fs <b, fs' b b b>4 
  fs''4 | <fs b, b,>16 e fs8~ fs2 r4 |
  e,,16 b'' fs' g fs4 \mordent e\mordent  \grace d16 e4 |
  <d g, g,>4 cs8 d e4 e4  
  
  <cs b fs b,> b b r4 \fermata \break
  
  \break
  \bar "||"
  
}

guitarmiddle = \relative c' {
  \key e  \minor
  \time 3/8
  b'8 e8 fs |
  
  <g b, g e b e, >4  e8 g fs e 
  <d b fs b,>4 r8 <b fs b,> e8 fs 
  <g b, g e b e, >4 e8 g fs \mordent e 
  d8. c16  b c a8 e a, c8 e'8 fs   \break
  
  <g b, g e b e, >4 e8 g fs e 
  fs8. d16 b8  b4 c16 b  a4 b8 c4 e8
  b8 b16 b b b b8 <e b>8 <fs b, >
  g8 b, e g fs e d8. fs16 e d cs8 a cs <b g d g,>8. g16 b d <cs a e a,>8 e d16 cs 
  b8 b16 b b b b8 b b 
  \bar "||"
  \break
}


melodyDm = \relative c'{
  \key d \minor
  d'4. d8 e4 d8 e f8 e d4   d,8 f g bf  g4. f8 g f ef g d4 r  d8 f g bf \break
  d4. d8 e4 d8 e f8 e d4   f8 g a bf a4 g f a8 g e g f e d4 r \break
  e8\mordent  d  e \mordent d d4 r e8\mordent  d  e f a f d4 
  \time 2/4 d,8 f g bf \time 4/4 g4. f8 g f ef g d4 r  d8 f g bf  \break 
  d4. d8 e8\mordent d8 e f e \mordent d d4 f8 g a bf a4 g f a8 g e g f e d2 \fermata
  \break
}
melodyA= \relative c'{
  \key b \minor  
  r2 b8 d e g | 
  b4. b8 cs4 b8 cs | d cs b4  b,8 d e g | e4. d8 e d c e | b2 b8 d e g | \break
  b4. b8 cs4 b8 cs | d cs b4  d8 e fs g | fs4 e d fs8 e | cs e d cs b2 | \time 2/4  b,8 d e g | \break
  \time 4/4
  b4. b8 cs4 b8 cs | d cs b4  b,8 d e g | e4. d8 e d c e | b2 b8 d e g | \break
  b4. b8 cs4 b8 cs | d cs b4  d8 e fs g | fs4 e d fs8 e | cs e d cs b2 \fermata
  \bar "||"
  \break
  \key af \major
  af,4 af af af8 bf c4 c c8 ef c bf g4 g g8 bf af g af g af4 af4 r | \break
  af4 af af af8 bf c4 c c8 ef c bf g4 g g8 bf af g af g f4 r r | \break
  df'4 df df df8 ef f4 f af8 g f ef c4 c c8 ef c bf c4 r r2 \break
  df4 df df df8 ef f4 f af8 g f ef c4 af'8 g af g f ef c4 e af, g \break
  bf bf bf bf8 c df4  df f8 g f ef c4 af f8 af f ef f4 r r r \break
  bf bf bf bf8 c df4  df f8 g f ef c4 af f8 af f ef f4 r  d'8 f g bf \break
  \bar "||"
  \melodyDm
  \bar "||"
  \break
}

melodyB = \relative c'{
  \time 6/8
  \key d \major
  fs,8 b4 g8 b4 | d8 cs b b fs4  | fs8 b4 g8 b4 | cs8 d e fs4. | \break
  fs4 d8 e fs d cs b a fs'4 cs8 d e fs a4. \break
  fs4 d8 e fs d cs b a fs'4 d8 fs e d cs4. \break
  fs8 b4 g8 b4 | d8 e d cs b  a | fs8 b4 g8 b4 | cs8 d e fs4. | \break
  fs,8 b4 g8 b4 | d8 e d cs b  a | fs8 b4 g8 b4 | e,8 d cs b4. | \break
  \bar "||"
}

melodyC = \relative c'{
  \time 4/4
  \key af \major
  af4 af af af8 bf c4 c c8 ef c bf g4 g g8 bf af g af g af4 af4 r | \break
  af4 af af af8 bf c4 c c8 ef c bf g4 g g8 bf af g af g f4 r r | \break
  df''4 df df df8 ef f4 f af8 g f ef c4 c c8 ef c bf c4 r r2 \break
  df4 df df df8 ef f4 f af8 g f ef c4 af'8 g af g f ef c4 r af g \break
  bf bf bf bf8 c df4  df f8 g f ef c4 af f8 af f ef f4 r  d8 f g bf \break

}

melodyMandoline = \relative c'{
  \key e \minor
  \time 6/8
  b8 e4 c8 e4 | c'8 b16 a g8 b4 e,8 |
  b8 e4 c8 e4 | fs8 g a b4. | \break
  b4 g8 a b g | fs e d  b'4 fs8 | g a b d4. 
  b4 g8 a b g | \break
  fs e d  b'4 g8 | b a g fs4. | \break
  b,8 e4 c8 e4 | d8 fs a g fs  e | 
  b e4  g8 e4 | fs8  e d e4. \break
  b8 e4 c8 e4 | d8 fs a g fs  e | 
  b e4  g8 e4 | fs8  e d e4. \bar "|."
}

bass = \relative c {
  \clef F
  \key b \minor
  r1 r1 r1 r1 r1
  r1 r1 r1 r1 r1 \break
 
  <<
  \new CueVoice {
    \stemUp r2 b'8^"Fanny" d e g
  }  
  >>

  g,,1 b e, b' g b2 d e d a b2 \break
  \time 2/4
  <<
  \new CueVoice {
    \stemUp b'8^"Fanny" d e g
  } 
  >>
  \time 4/4
  g,,1 b e, b' g b2 d e d a b2 \fermata
  \bar "||"
  \key af \major
  \break
  r2 af | r g | r g | r4 af f2 |
  r2 af | r g | r g | r4 af f2 | \break
  r4 df'~ df df~ | df f~ f f~ |f c~ c c~ | c c~ c c~ | \break
  c  df~ df df~ | df f~ f f~ | f f~ f f~ | f c~ c c~ | \break
  c bf~ bf bf~ | bf df~ df df~ | df f~ f f~ | f f g af | \break
  bf2 f4 bf, | df1  | f2 c | f, r \break
  
  \bar "||"
  \key d \minor
  bf2 f'4 bf | d,2 d4 bf | g'2 g, | d' d4 f | \break
  
  bf,2 bf4 c |
  
  d2 f | g f | c d | f4 a  bf2 | bf4 a  d,2
  \time 2/4 d4 f | 
  \time 4/4
  g2 g4 f | d2 c | 
  bf bf4 c | d2 f | g f | c d \fermata \break
  \bar "||"
  
  \time 3/8
  \key e \minor
  <<
  \new CueVoice {
    \stemUp b'8^"Maximin" e fs 
  }  
  >>
  e,,4. e' | b | 
  <<
  \new CueVoice {
    \stemUp b'8^"Maximin" e fs 
  }
  r4.
  >>
  |
  e,,4.       | e' | b a a' \break
  e,    e' |
  b4 fs'8 | b4. 
  a, | a'
  b,4 fs'8 | b4. \break
  e,,4 b'8 | e4.
  d
  a
  g
  a
  b
  r
  \bar "||"
  \break
  \key b \minor
  \time 6/8 
  
  b4. e a, b
  b4. e a, d
  b e fs b~ \break
  b8 a g 
  fs4.
  d e a, d~
  d8 fs d
  cs4.
  
  b b' \break 
  d, a'
  b4 a8
  g4. a b 
  b,4 b8 b4 b8  d4. a
  b4. g a b \fermata
  \bar "||"
  \break
  \time 4/4
  \key af \major
  r8 af4 af af af8~|af g4 g4 g4 g8~ |g8 g4 g4 g4 g8~ |g8 f4 f f f8~ | \break
  f8 af4 af af af8~|af g4 g4 g4 g8~ |g8 g4 g4 g4 g8~ |g8 f4 f r8 c'4 | \break
  df2. ef4 | f2.  f4 | c2. ef4 | c2. c4 | \break
  df2. ef4 | f2.  f4 | f2.  f4 | c2. c4 | \break 
  bf2. c4 | df1  | f2.  c4 | f1 \break
  \break
  \key d \minor
  bf,2. f'4 | d2 d4 f | g2 g4 f | d2 d4 f | 
  bf,2 bf4 c | d2 f | \break
  
  g f | c d
  
  d4 f bf,2 | bf4 c d2 | 
  \time 2/4 d4 f  \break
  \time 4/4
  g2 g4 f | d2 d4 c | bf2. c4 |
  d2 f g f c d \fermata \break
  
  \bar "||"
  
  \time 6/8
  
  e,4. c' a e'
  e, c' d g
  e a, b e,
  g b g a
  d g g, b
  e, c' d c
  e, c' d e
  e, c' d c
  e, c' d e
  \bar "|."
}



theme={
  \relative c'{ 
    \global
    \guitarintro
    \melodyA
    \guitarmiddle
    \melodyB
    \melodyC
    \melodyDm
    \melodyMandoline
  }
}

\paper {
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 6)
       (padding . 0.4)
       (stretchability . 60))
} 

\book{
  \bookOutputName "lyra_all"
  \score {
    <<
      \new ChordNames { 
           \chordNames
      }
      \new Staff  \with {instrumentName = #"Clarinet (C)"}{
        \theme
      }
      \new Staff  \with {instrumentName = #"Bass (C)"}{
        \bass
      }
    >>
    \layout {     }
  }
}


\book{
  \bookOutputName "lyra_bass"
  \score {
    <<
      \new ChordNames { 
          \chordNames
      }
      \new Staff  \with {instrumentName = #"Bass (C)"}{
          \bass
      }
    >>
    \layout {     }
  }
}
\book{
  \bookOutputName "lyra_C"
  \score {
    <<
      \new ChordNames { 
          \chordNames
      }
      \new Staff  \with {instrumentName = #"Guitar (C)"}{
          \theme
      }
    >>
    \layout {     }
  }
}

\book{
  \bookOutputName "lyra_Bb"
  \score {
    <<
      \new ChordNames { 
         \transpose c d { \chordNames }
      }
      \new Staff  \with {instrumentName = #"Clarinet (Bb)"}{
        \transpose c d {\theme}
      }
    >>
    \layout {     }
  }
}