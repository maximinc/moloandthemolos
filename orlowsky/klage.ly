
\version "2.18.2"
\language "english"

\header {
  title = "Klage"
  composer = "David Orlowsky Trio"
}

global = {
  \set Score.skipBars = ##t
  \time 4/4
}


chordNames={
  \chordmode{
    g2:m a:m g:m a:m g:m b:m d:m s
    g2:m a:m g:m a:m g:m b:m d:m s
    
    g1:m a:m ef2:m c:m g:m s
    ef1:maj7 a:m ef2:m c:m g1:m
    
    d:m g:m g:m9 d:m9
    a:m g:m9 g2.:m  d4:7 g:m
  }
}

clarinet={
  \tempo 4=75
  \key g \minor
  \relative c'''{
    g4\p bf a2 g8 (fs) g bf a2 g8 (fs) g ef d4 bf' a d,2. \break
    g4 bf a2 g8 (fs) g bf a2 g8 (fs) g ef d4 bf' a d,2. \break \bar "||"
    
    ef4.\mp (d8) c (g') fs bf a4 a8 (bf) a2
    ef4. (d8) c (g') fs bf g1 \break

    ef4. (d8) c (g') fs bf a4 a8 (bf) a2
    ef4. (d8) c (g') fs bf g1^"Fine" \break \bar "||"
    
    c4.( \p bf8) c4 d bf1 g8( a) bf a g4 ef d1 \break
    
    c4.( bf8) c4 d ef bf a2 g4. d'8 bf4 a4_"rit." g1 \fermata ^"D.C. al Fine"
    \bar "|."

  }
}

\book {
  \bookOutputName "klage_C"
  \score {
    <<
      \new ChordNames { 
        \transpose d c { \chordNames }
      }
      \new Staff \with {instrumentName= "(C)"}{ 
         \global
         \transpose d c { 
           \clarinet
         }
       }
  
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "klage_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff \with {instrumentName= "# Clarinet (Bb)"}{ 
         \global
         \transpose c c { 
          \clarinet
         }
       }
  
    >>
    \layout { }
  }
}