
\version "2.18.2"
\language "english"

\header {
  title = "Moldawanka"
  composer = "David Orlowsky Trio"
}

global = {
  \set Score.skipBars = ##t
  \set Score.markFormatter = #format-mark-circle-letters
}

clarinet={
  \tempo 4.=140
  \time 12/8
  \key d \minor
  
  r1. r1. r1. r1. \break
  
  \mark \default

  \relative c'{
    d4 f8 f4 d8 a'4 g8 e4.
    f4 e8 d8 e f a4 a4 r
    d,4 f8 f4 d8 a'4 g8 e4 e8
    f4 e8 d4 cs8 d4. r
    
    d4 f8 f4 d8 a'4 g8 e4.
    f8 e d e f g a4 d r
    \break
    d,4 f8 f4 d8 a'4 g8 e4 e8
    f4 e8 d4 cs8 d4. r
  }
  
  \relative c'{
    c4 e8 e4 c8 f4 e8 d4 c8 
    c4 e8 e4 c8 g'4. r
    c,4 e8 e4 c8 f4 e8 d4 c8 
    e4 d8 c4 b8 c4. r
  }
  \relative c'{
    d4 f8 f4 d8 a'4 g4 r8 e8 f e d e f g a4 d r
    d,4 f8 f4 d8 a'4 g8 e4.
    \break
 
    \tempo \markup {
      \concat {
      (
      \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
      )
      }
    }
    \time 3/2
    f4 e d cs d4 r4 

    \mark \default

    <<
      \new CueVoice{
        \relative c'''{
          \stemUp
          a8 e d s^"etc"
        }
      }
      \stemDown
      {r4 a'}
    >>
    
    
    
    a4 bf~ bf a~ a a 
    g a~ a4 r4 r a4
    bf d~d8 cs4. bf2 a8 g a4 r4
    
    r4 
    
    r b b c~c b~ b b a b4 b8 b \tuplet 3/2 {c ds e}
    fs4. e8 ds c b8 r4 c8 b a 
    \time 3/4
    b4. a8 g f
    
    <<
      \new CueVoice{
        \stemUp
        \relative c'''{
          b8 g e b' g e a f d a' f d g e b g' e b f' d a f' d a
          e g b e b g
        }
      }
      {f4 e2 r4. d8 e f f4 e2 r4. d8 e f e2 r4}
    >>
    \stemNeutral
    \key e \minor
    
    \mark \default

    r2. r2. r2. r2. r2. r2.
    \break
  }
  
  \time 12/8
  \relative c'{
     e4 g8 g4 e8 b'4 a8 fs4 r8
     g4 fs8 e fs g b4 b r
     
     e,4 g8 g4 e8 b'4 a8 fs4 r8
     g4 fs8 e4 ds8
     e4.      r
     \break
     r1. r1. r1.
  }
  \relative c'{
    e4 g8 g4 e8 b'4 a8 fs4 r8
    g fs e fs g a b4 b b 
    e,4 g8 g4 e8 b'4 a8 fs4 r8
    g4^"Ral" fs e ds e4->  r
  }
  
  \time 4/4
  \relative c'{
    \mark \default
    b2\fermata \glissando b''~\fermata
    
    \tempo 4=105
    b8 as as b b as as g 
    e4 r g r8 b8 b as as b b as as g
    
    e2~e8 ds16 e d e cs e
    \break
    c4 r e4 r8 g8 
    g fs fs g g fs fs e c4 r e4 r8 g
    g fs fs g g fs fs e 
    \break
    e4 r g r8 b b as as b b as as g
    e g g fs fs e e4 e8 g g fs fs e e\turn d e\turn d r8 g16 fs e4 r
  }
  \relative c'{
    e8 g g fs fs e e4 e8 fs fs e e\mordent d d\mordent c b4 r d r8 g g fs fs e e d d c
    \break
    b4 r d r8 g8 g fs fs e e d d e 
    e4 r g r8 b b as as b b as as g 
    e8 g g fs fs e e\turn d e g g fs fs e e d b4 r d r8 g g fs fs e e d d c 
    b4 r d r8 g g fs fs  e e d d e
  }
  \tempo 4=95
  \mark \default
  \time 2/4
  \relative c'{
    e16 ds e g r e ds e g fs g a b c b as b as g e r d c b
    \break
    a gs a c r a gs a c b c d e fs g a 
    ds e as, b g fs e ds
    e g fs e r g fs e ds e fs g e c e g 
    \break 
    e4
    
  }
  \relative c'{
    r16 e16 g fs 
    
    <<
      \new CueVoice{
        \stemUp
        \relative c'''{
          b8 as b g b as b g b as b g
          e ds e c e ds e c
        }
      }
      {
        \stemDown
      e16 ds e g r e ds e 
      g fs g a b c b as b as g e r 
      d c b 
      \break
      a gs a c r a gs a c b c d e fs g a 
      
      }
    >>
      \stemNeutral
    ds e as, b g fs e ds
    e g fs e r g fs e ds e fs g e c e g e4 r
  }

  \key f \minor
  \relative c'{
    f16 e f af r f e f af bf c df c af g f g ef d c r
    g' ef d ef d ef f g f ef d 
    \break
    c bf c d ef f g \parenthesize af af g bf af
    r g bf af c bf af g r c bf af 
    g8. f16 af g bf af
    
    \time 3/4
    g8. f16 af g bf af g8 f16 ef
  }
  \time 4/4
  \relative c'{
     f4 r16 c f af g fs d c b c e g 
     af4 r16 f af c b g fs e c b c e 
     f4 r16 c f af g fs d c b c e g 
     af4 r16 f af c b af g f g af b c b\mordent af g c
     r b af g b c af g ~ g b c af g f af g b af c b af g b af c b ef d 
     \break
     c4
     b8 ef c4 b8 ef c4
     bf8 af g f~f g16 af 
     
     \time 2/4
     g4 f8 ef
     \break
  }
  \time 4/4
  \mark \default
  \relative c'{
    f4 r af r8 c8 c b b c c b b af
    f4 r af r8 c'8 c b b c c b b af
    f2~ f8 f ef f 
    \break
    df4 r f r8 af8 af g g af af g g f 
    df4 r f r8 af8 af g g af af g g f
    \break
    f4 r af r8 c8 c b b c c b b af
    f af af g g f f4
    f8 g g f f ef ef df 
    \break
    c4 r ef r8 af8 af g g f f ef ef df 
    c4 r ef r8 af8 af g g f f ef ef f
    \break
    f4 r af r8 c c b b c c b b af 
    f4 r af r8 c c b b d d c c e f1~ f1~ f ~ f ~ f4 
    
    r\fermata 
  }
  \relative c''{
    b8 c f,4
  }
  \bar "|."
}

bass={
  \time 12/8
  \key d \minor
  
  \relative c'{
    d4. f,8 a d c4. f,
    bf4. d,8 f bf a4 bf c
    d4. f,8 a d c4. f, g4. r8 f e
    d2.
  }
  \mark \default
  \relative c'{
    d2. c bf a4 bf c 
    d2. c g d 4 f a
    d2. c bf a4 bf c 
    \break
    d2. c g d4 f g
    a2. f a g4 a b 
    a2. d,4. e f g 
    a4 bf c 
  }
  
  \relative c'{
    d2. c bf a4 bf c 
    d2. c 

    \tempo \markup {
      \concat {
      (
      \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
      )
      }
    }
    \time 3/2
    g2 d' bf
    \mark \default
    a2 a a a a a a a a a a as
    \break
    
    b b b b b b b b b 
    \time 3/4
    b b4 
  }
  %\time 3/4
  \relative c {
    e2. d e d e2 e4
    \break
    \mark \default
    \key e \minor
    d g fs e2 e4 d g fs 
    e2 d4 c2. b4 g' fs 
    
    \time 12/8
    \break
    e2. d'2. c b4 c d 
    e2. d 

    a
    e4. g8 b e d4. g, c e,8 g c b4 c d
    e4. g,8 b e d4. g, a4.~ a8 g8 fs
    
    e2. d c b4 c d e2. d4. d a2~^"Ral" a2 e4-> r
    \break
  }
  \time 4/4
  \mark \default
  \relative c{
    b1\fermata ~
    \tempo 4=105
    b4 r r2
    %# cue voice
  }
  \relative c'{
    e4 b e b e b e b e b e b
    
    a e' a, e' a, e' a, e' 
    a, ds8 e a,4 e'
    a, a c d 
    
    e as,8 b e,4 b' e b e d
    c4. c8 c4 c
    a4. a8 a4 a
    d4 d8 ds e4 d
    
    c4. c8 c4 c8 g
    a4. a8 a g a as 
    b4 f8 fs d4 b
    c f8 fs e d c4 
    b f'8 fs d4 b
    c b8 c d4 c8 d
    
    e4 b'8 e e,4 b'
    e, b' e,8 e g b
    c4. c8 c4 c8 b
    a4. a8 a4 r
    b4 f8 fs d4 b
    c f8 fs e d c4
    b f'8 fs d4 b
    c4 b8 c d4 c8 d 
  }
  \tempo 4=95
  \mark \default
  \time 2/4
  \relative c{
    e4 b e b e b
    \break
    a e' a e e'8 ef d df 
    c4. c8 c4 f,
    e2 r4 r
    
    r2 r
    \break
    a8 e' a, e' a, e' a, e'
    e ef d df 
    c g c g c c f, f
    e b d e
    \break
  }
  \key f \minor
  \relative c{
    f c' f, c' f, c' f, c'
    c g c g c g c bf
    \break
    af ef af g f f c' c 
    
    df af df df 
    
    g,4 bf8 df 
    
    \time 3/4
    c4 df8 df ef4
  }
  \break
  \time 4/4
  \relative c'{
    f8. f f8 e8. e e8
    f8. f f8 e8 b g e
    f8. f f8 e8. e e8

    f8. f f8 g8. g g8 
    af8. af af8 g8. g g8 
    b,8 d f gs d f gs b
    c4 af c af c r f,2 
    \time 2/4 
    g
  }
  \time 4/4
  \mark \default
  \relative c{
    f4 c' f, c' f, c' f, c'
    f, c' f, c' f, c' f, c'
    f, 
    \break
    c' f, c' 
    bf f bf f bf f bf f
    bf f bf f bf f df ef 
    \break
    f c' f, c' f, c' f, c'
    df4. df8 df4 df
    bf4. bf8 bf4 bf
    \break
    c4 fs,8 g ef ef d c
    df4 g8 af f4 df
    c4 fs8 g ef ef d c
    df4 c8 df ef4 ef8 e
    f4 b8 c af af g c,
    f4 b8 c af af g c,
    f4 b8 c af af g c,
    af'8 af g g gf gf gf4
    f4 b8 c af af g c,
    f4 b8 c af af g c,
    f4 b8 c af af g c,
    f4 b8 c af af g c, ~ c4
    
    r \fermata
    c f
  }
  \bar "|."
}

chordNames=\chordmode{
    d2.:m f/c bf a:m d:m f bf/g  d:m
    d:m   f/c bf a:m d:m f/c bf/g  d:m
    d:m   f/c bf a:m d:m f/c g:m  d:m
    
    a:m f c/a g  a:m d:m f4. g:sus a2.:m
    
    d:m   f/c bf a:m d:m f/c g:m s
    
    a1.:sus  a bf:dim7 s
    b1.:sus  b c:dim7  s2.
    e2.:m d:m e:m d:m
    e:m d e:m d e:m c b:m
    
    e:m g:maj7/d c b:m
    e:m g:maj7/d a:m 
    e:m g/d c b:m
    e:m g/d a:m
    e:m g/d c b:m
    e:m g/d a:m e:m 
    b1:7 s
    
    e:m e:m e:m a:m a:m a:m a2:m c4 d4
    
    e1:m e:m c a:m d2 e:m c1 a:m b:m c b:m c2 d 
    e1:m e:m c a:m b:m c b:m c2 d
    
    e1:m e2:m a:m a:m e8 ef d df 
    c2 c4 f4 e1:m e:m a2:m a:m e8 ef d df 
    c2 c4 f e2:m
    
    f:m f:m c:m c:m af f:m df:maj7
    g:m7.5- c2.:7
    
    f2:m e:m f:m e:m f:m e:m f:m e:m/g f:m/af e:m/g
    d2:dim7 s16 b:dim7 s gs:dim7 s f:dim7 s8
    c4:m af:m c:m af:m c2:m f:m g:m
    
    
    f1:m s f:m s f:m
    bf:m s bf:m bf2:m df4 ef
    f1:m f:m  df bf:m c:m df c:m df
    f:m f:m f:m 
    
    af4 g gf2
    f:m  af g gf
    f:m  af g gf
    f:m
    
    
}



\book {
  \bookOutputName "moldawanka_all"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Clarinet (C)"
      }{ 
         \global
         \transpose c c { 
           \clarinet
         }
       }
  
       \new Staff  \with {
         instrumentName = #"Bass (C)"
       }{ 
           \global       
           \clef "F"
           \transpose c c {
             \bass
           }
        }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "moldawanka_bass_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with {
         instrumentName = #"Bass (C)"
      }{ 
         \global 
         \clef "F"
         \bass
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "moldawanka_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Clarinet (Bb)"
      }{ 
         \global
         \transpose c d { 
           \clarinet
         }
       }
    >>
    \layout { }
  }
} 


\book {
  \bookOutputName "moldawanka_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Guitar (C)"
      }{ 
         \global
         \transpose c c { 
           \clarinet
         }
       }
    >>
    \layout { }
  }
}

% 
% \book {
%   \bookOutputName "moldawanka_bass_Bb"
%   \score {
%     <<
%       \new ChordNames { 
%         \transpose c d { \chordNames }
%       }
%       \new Staff  \with {
%          instrumentName = #"Bass (Bb)"
%       }{ 
%          \global        
%          \transpose c d' { \bass }
%       }
%     >>
%     \layout { }
%   }
% }
