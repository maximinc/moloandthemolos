
\version "2.18.2"
\language "english"

\header {
  title = "Vodka Afinata"
  composer = "David Orlowsky Trio"
}


maintheme=\relative c''{
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  \repeat volta 2{
  \tuplet 3/2 {d16 f a}    d8 d d    \tuplet 3/2 {c,16 e a} c8 c 
  \tuplet 3/2 {d,16 f a}   d8 d d    \tuplet 3/2 {c,16 e a} c8 c  \break
  \tuplet 3/2 {bf,16 d f}  bf8 bf bf \tuplet 3/2 {a,16 c e} a8 a
  \tuplet 3/2 {bf,16 d f}  bf8 bf bf \tuplet 3/2 {a,16 c e} a8 a  \break
  \time 3/4
  \tuplet 3/2 {g,16 bf d}  g8 g g    \tuplet 3/2 {a16 e c}  a8 
  
  }
  \alternative{
    { \tuplet 3/2 {g16 bf d} g8 g g a  r }
    { \tuplet 3/2 {g,16 bf d} g8 g g \tuplet 3/2 {a16 e c} a8}
  } 
  \break
}

themeB=\relative c''{
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  \tuplet 3/2 {d16 f a} d8 d d \tuplet 3/2 {d16 c d e d c} c8
  \tuplet 3/2 {d,16 f a} d8 d d \tuplet 3/2 {a16 bf a g f g} a8 \break
  \tuplet 3/2 {bf,16 d f} bf8 bf bf \tuplet 3/2 {a,16 c e} a8 a 
  \tuplet 3/2 {bf,16 d f} bf8 bf bf \tuplet 3/2 {a,16 c e} a8 a \break
}

themeBfinA=\relative c''{
  \time 3/4
  \tuplet 3/2 {g16 bf d}  g8 g g    \tuplet 3/2 {a16 e c}  a8 
  \tuplet 3/2 {g16 bf d}  g8 g g  \tuplet 3/2 {a16 e c}  a8 
}

themeBfinB=\relative c''{
  \time 5/8 
  \tuplet 3/2 {g16 bf d}  g8 g \tuplet 3/2 {g16 a g} f8  
  \tuplet 3/2 {g,16 bf d}  g8 g \tuplet 3/2 {g16 a g} f8
}


clarinet={
  \time 7/8
  \tempo 4=160
  \set Timing.beatStructure = #'(2 2 3)
  \key d \minor
  r4 r r4.
  r4 r r4.
  r4 r r4.
  r4 r r4.  
  \break
  \maintheme
  \time 4/4
  \relative c''{
    g4. a8 e4 r
    g8 f g a e4 r8 f16 e d4 r r r 
  }
  
  \break
  
  \themeB
  \themeBfinA
 
  \break
  \time 4/4
  \relative c''{
    g4. a8 e4 r
    g4. a8 e4. f16 e 
    d4.  e8 c a~ a4   
    d8 c d e c4 r \break
    
    d4. e8 c a16 d bf a g8
    c4. bf8 a \mordent g a4
    
    r8 d d e c a4.
    d8 \mordent c d e c4 r \break
    
    d4. e8 c a16 d bf a g8
    c4. bf8 a \mordent g a4
  }
  \break
  \relative c''{
    r8 bf8 r a bf4 d4
    r8 a r g a f' e\mordent d
    r8 bf8 r a bf a g f r4 a8 g a g a f \break
    r8 bf8 r a bf cs d bf r8 a r d f d f4 e8 \mordent c a  d f d bf f'8 r4 r r r \break
  }

  \maintheme

  \relative c''{
    \time 4/4
    g4. a8 e4 r
    g8 f g a e4 r8 f'16 e
  }


  \themeB
  \themeBfinB
  \time 4/4
  \relative c''{
    d4. f8 d4. f8 d bf d f d4 r
  }
  \themeBfinB
  \time 4/4
  \relative c''{
    d4. f8 d4. f8 
    \time 2/4
    e c a f' 
  }

  \time 7/8
  \relative c''{
    d4 r4 r4.
    r4 r4 r4.
    r4 r4 r4.
    r4 r4 r4.
  }
  \break
  \relative c'{
    d16 e f g a4~ a4.~a4~a4~a4.
  }
  \relative c'{
    d16 e f g a4~ a4.~a4~a4~a4.
  }
  \relative c'{
    d16 e f g a4~ a4.~a4~a4~a4.
  }
  \relative c'{
    d16 e f g a4~ a4.~
    \time 4/4
    a1
  }
  
  \break
  \maintheme
  \relative c''{
    \time 4/4
    g4. a8 e4 r
    g8 f g a e4 r8 f'16 e
  }
  \themeB
  \themeBfinB
  \time 4/4
  \relative c''{
    d4. f8 d4. f8 d bf d f d4 r
  }
  \themeBfinB
  \time 4/4
  \relative c''{
    d4. f8 d4. f8 
    \time 2/4
    e\mordent c a f' 
  }

  \bar "||"
  \time 4/4
  \relative c''{d4} r4 r2 r1
  \break
  \relative c'' {
    bf8 a bf c d cs d e f e\mordent d cs d f a f cs e a e cs e a e d f a f a4 r4
  }
  \relative c'' {
    bf8 a bf c d cs d e f e\mordent d cs d f a d, bf' a g f e d cs e d2 r
  }
  \break
  \relative c''' {
    g8 d bf d g bf a\mordent g a e c e a e a e
    g  d bf d g bf a\mordent g a e c e a4 r
  }
  \relative c''' {
    g8 d bf d g bf a\mordent g a e c e a e a e
    g  d bf d g bf a\mordent g a g f e cs4 r
  }
  \break
  \relative c' {
    \tuplet 3/2 {bf8 g bf cs d e f a f e d cs}
    \tuplet 3/2 { d e f bf f e d e f bf f e}
    \tuplet 3/2 { d e f a f e d e f a f e}
    \tuplet 3/2 { bf g bf cs e g bf a g f e f }
    \tuplet 3/2 { a g f e d e}  cs4 r
  }
  
  \break
  \relative c' {
    \tuplet 3/2 {bf8 g bf cs d e f a f e d cs}
    \tuplet 3/2 { d e f bf f e d e f bf f e}
    \tuplet 3/2 { d e f a f e d e f} a4
  }
  \relative c' {
    \tuplet 3/2 { bf8 bf bf bf d g a, a a a d f e d cs d e f} d4 r4
  }

  \break
  \repeat volta 2{
    \relative c' {
      bf16 a g a bf d c bf a g f g a f g a
      bf   a g a bf d c bf a g f g a4
      
      bf16 a g a bf d c bf a g f g a f g a
      bf   a g a bf d c bf f' e d e cs4
    }
    \break
    \relative c'{
      \tuplet 3/2 {bf8 bf bf g' g g bf, bf bf g' g g a, a a f' f f a, a a} f'4
    }
    \relative c''{
      \tuplet 3/2 {e8 ef d cs d ds e8 ef d cs d ds }
    }
  
    \relative c'{
      \tuplet 3/2 { bf8 bf bf bf d g a, a a a d f e d cs d e f} d4 r4
    }
  }
  \break
  \relative c'' {
    bf16 a bf c d cs d e f e d cs d f a f cs e a e cs e a e d f a f d f a f
  }
  \relative c'' {
    bf16 a bf c d cs d e f e d cs d f a f cs e a e cs e a e d f a f d f a f
  }

  \break 
  \relative c'' {
    bf16 a bf c d cs d e f e d cs d f a8
  }
  \relative c'''{
      bf8. a16 g f e d cs e d8 a' d
  }
  \bar "|."
}

bass={
  \transposition c,
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  \clef "F"
  \key d \minor
  \relative c{
    d4 d a4 a8
    d4 d a4  a8
    d4 d a4 a8
    d4 d a4  a8
  }
  
  \break
  \relative c{
    \repeat volta 2{
      d4 d a4 a8 d4 d a4  a8
      bf4 bf a4 a8  bf4 bf a4 a8 
      \time 3/4
      g4 g a 	
    }
    \alternative{
      { bf4 bf c4}
      { bf4 bf a4}
    }
  }
  \break
  \time 4/4
  \relative c{
    g2 a bf c4 cs d4 a8 a' bf, bf' c, c' 
  }
  \time 7/8
  \relative c{
    d2 c4 c8 d4 e f f8
    bf2 a4 a8
    bf4 bf a a8
    \time 3/4
    g2 a4 bf2 a4
    \time 4/4
    \break
    g4. g8 a4. a8 bf4. bf8 c4 cs 
    d4. d8 a8 a c4
    d4. d8 a8 a c4
    d4. d8 a a bf4
    g4. g8 a g f e
    d4. d8 a8 a c4
    d4. d8 a8 a c4
    d4. d8 f4 a
    g4 g8 g8 a g f a 
    g4 \accent r r r
    d4. d8 a' g f4
    g4. g8 d' c bf4
    d,4\staccato d4 e f 
    g4. g8 d' c bf4
    d,4. d8 a' g f4
    a,4 a8 a bf4 c
    d4
    
    a8 a' bf, bf' c, c' 
    \break
  }
  \time 7/8
  \relative c{
    \repeat volta 2{
      d2  a4 a8 d4 d a4  a8
      bf2 a4 a8  bf4 bf a4 a8 
      \time 3/4
      g4 g a
    }
    \alternative{
      { bf4 bf4 c }
      { bf4 bf a4 }
    }
  }
  \break
  \time 4/4
  \relative c{
    g2 a bf a8 bf c cs 
  }
  \time 7/8
  \relative c{
    d4 d a a8
    d4 e f f8 
    bf4 bf a8 g e 
    bf'4 bf a a8
    \time 5/8
    g4. f4
    g4. f4
    \time 4/4
    d4. d8 f4 g8 a
    bf4 c8 cs d c bf a
    \time 5/8
    g4. f4 g4. f4
    \time 4/4 
    d4. d8 f4 g8 gs
    \time 2/4
    a4 c8 cs 
  }
  \time 7/8
  
  \relative c'{
    d2~ d4.~
    d2~ d4.
    \set Timing.beatStructure = #'(2 2 2 1)
    a8 g e a g e a g e a g e a g e a g e a g e a g e a g e
    a g e a g e a g e a g e a g e a g e
    a g e a g e a g e a g e a g e a g e
    \time 4/4
    a a g g f f e e 
  }
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  \break
  \relative c{
    \repeat volta 2{
      d2  a4 a8 d4 d a4  a8
      bf2 a4 a8  bf4 bf a4 a8 
      \time 3/4
      g4 g a
    }
    \alternative{
      {bf4 bf4 c }
      {bf2 a4}
    }
  }
  \time 4/4
  \relative c{
    g2 a g a8 a c cs
  }
  \time 7/8
  \relative c{
    d2 a4. d4 e f f8
    bf4 bf8 bf a4 a8 bf8 a g bf
    a a a
    \time 5/8
    g4. f4 g4. f4
    \time 4/4
    d4. d8 f4 g8 gs
    a4 c8 cs d c bf a
    \time 5/8
    g4. f4 g4. f4
    \time 4/4
    d4. d8 f4 g8 a
    \time 2/4
    bf4 c8 cs
  }
  \time 4/4
  \break
  \relative c'{
    d4 a d a
    d4 a d a
  }
  
  \relative c'{
    g d' g, d'
    d a d a
    \break
    a cs e ef
    d a d a
    g d' g, d'
    d a d a
    \break
    g a bf cs 
    d a d a
    g d' g, d'
    a c d e
    \break
    g, d' c bf 
    a c d e 
    g, d' c bf 
    a e' d c 
    \break
    g d' g, gs 
    a cs e a,
    g g d' d
    bf d f e
    \break
    d a d a
    g g d' e
    f e a, a
    g g d' d
    \break
    bf d f e
    d a d a
    g g d' d
    a cs d d 
  }
  \break
  \repeat volta 1{
    \relative c'{
      g g d' d g, g d' d
      g, g f f g8 a bf c d4 a 
      g d' g, d' d a d a 
      cs8 a a4 cs8 a a4
      g g d' d 
      a8 cs e ef d4 d
    }
  }
  \break
  \relative c'{
    g g d' d a a d d 
    g, g d' d a a d d 
    g, g d' d 
    r  a\accent r d8\accent d \accent
  }
  \bar "|."
}

chordNames={
  \chordmode{
    d2:m a4.:m d2:m a4.:m d2:m a4.:m d2:m a4.:m
    
    d2:m a4.:m d2:m a4.:m
    bf2 a4.:m bf2 a4.:m
    g2:m a4:m
      g2:m a4:m
      g2:m a4:m
     
    g2:m a:m
    g:m a4:m a4:7  d1:m

    d2:m c4. d2:m c4.
    bf2 a4.:m bf2 a4.:m
    g2:m a4:m
      g2:m a4:m
     

    g2:m a:m
    g:m a4:m a4:7  
    
    d2:m a:m d:m a:m 
    d:m a:m g:m a:m
    d:m a:m d:m a:m
    d:m a:m g:m a:m
    
    g1:m d:m g:m d:m
    g1:m d:m a2:m bf d1:m

    d2:m a4.:m d2:m a4.:m
    bf2 a4.:m bf2 a4.:m
    g2:m a4:m
      g2:m a4:m
      g2:m a4:m

    g2:m a:m
    g2:m a4:m a4:7  
    
    d2:m c4.
    d2:m c4.
    bf2 a4.:m
    bf2 a4.:m
    g4.:m d4:m 
    g4.:m d4:m 
    d1:m bf2 d:m
    
    g4.:m d4:m 
    g4.:m d4:m 
    d1:m a2:m 
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d2:sus2 d4.:7sus2
    d1:sus2

    d2:m a4.:m d2:m a4.:m
    bf2 a4.:m bf2 a4.:m
    g2:m a4:m
      g2:m a4:m
      g2:m a4:m

    g2:m a:m
    g:m a4:m a4:7 
    
    
    d2:m c4.
    d2:m c4.
    
    bf2 a4.:m
    bf2 a4.:m
    
    g4.:m d4:m
    g4.:m d4:m
    
    d1:m  bf2 d2:m
    
    g4.:m d4:m g4.:m d4:m
    d1:m a2:m d1:m d:m
    
    g:m d:m a:7 d:m
    g:m d:m g2:m a:7
    d1:m
    
    g:m a:m 
    g:m a:m
    g:m a:m
    g:m a:7
    
    g2:m d:m bf1
    d:m g:m d2:m a:7
    g2:m d:m bf1 d:m g2:m d:m a2:7 d:m
    
    g2:m d:m g2:m d:m
    g2:m d:m g2:m d4:m a:7
    g1:m d:m a:7
    g2:m d:m a:7 d:m
    
    g2:m d:m a:7 d:m
    g2:m d:m a:7 d:m
    g2:m d:m a:7 d:m
  }
}

\book{
  \bookOutputName "vodka_afinata_all"
  \score {
    << 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with { instrumentName = #"Clarinet" }{ 
        \set Staff.midiInstrument = "clarinet"
        \transpose c c { 
          \clarinet
        }
      }
  
      \new Staff  \with { instrumentName = #"Bass" }{
        \set Staff.midiInstrument = "acoustic bass"
        \transpose c c { 
          \bass
        }
      }
    >>
    \layout { }
    \midi { }
  }
}
 
\book{
  \bookOutputName "vodka_afinata_Bb"
  \score {
    <<    
    \new ChordNames { 
      \transpose c d { \chordNames }
    }
    \new Staff  \with { instrumentName = #"Clarinet (Bb)" }{ 
      \transpose c d { 
        \clarinet
      }
    }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "vodka_afinata_C"
  \score {
    <<    
    \new ChordNames { 
      \transpose c c { \chordNames }
    }
    \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
      \transpose c c { 
        \clarinet
      }
    }
    >>
    \layout { }
  }
}




\book{
  \bookOutputName "vodka_afinata_bass"
  \score {
    << 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
    
      \new Staff  \with { instrumentName = #"Bass" }{
        \set Staff.midiInstrument = "acoustic bass"
        \transpose c c { 
          \bass
        }
      }
    >>
    \layout { }
  }
}
