
\version "2.18.2"
\language "english"

\header {
  title = "Stille Tage"
  composer = "David Orlowsky Trio"
}


chordNames = {
  \chordmode {
    d1:m9 s d:m9 s d:m9 s d:m9 s
    d:m9 s d:m9 s d:m9 s d:m9 s
    
    d:m9 d:m/c d:m/bf d:m/a
    d:m/g d:m/f d:m/f d:m
    c/d  d:m c/d  d:m
    c/d  d:m c/d  d:m
 
    f c d:m a:7
    f c d:m a:7
    
    f f f c
    a:m d:m a:7 d:m
    
    c d:m c d:m
    c d:m c d:m
    
    f c d:m a:7
    f c d:m a:7
    
    r1 s s s
    g:m d:m a:7 d2:m d:7 
    g1:m d:m a:7 bf
    f  e2:m7.5- a:7 d1:m9 bf/d
    d:m d:dim d:m bf/d
    d:m d:dim a:7 s4 a:7 d:m s
  }
}


melody = \relative g' {
  \time 4/4
  \key d \minor  
  R1*7 | r2 r8 d d e | 
  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d d e | \break

  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d d d | \break


%  \repeat volta 2 {
%  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
%  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e |
%  }
%  \alternative {
%    {d2 r8  d d e |}
%    {d2 r8  d d d |}
%  }\break
  
%  \repeat volta 2{
%    c4 c4 c8 c d e | d4 d4~ d8 a'8 g f | 
%  }  
%  \alternative{
%    { e4 e e8 e f g | f2 r8 d d d }
%    { e4 e e8 e f e | d2 r8 d' d e }
%  }
  
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f |  e4 e e8 e f g | f2 r8 d d d 
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f |  e4 e e8 e f e | d2 r8 d' d e
  \break
  
%  \repeat volta 2{
%    f2 r8 a g f | e2 r8 g f e | d2 r8 f e d |
%  }
%  \alternative {
%    { cs2 r8 cs d e |}
%    { cs2 r8 d d e |}
%  }
  
  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 cs d e | \break
  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 d d e | \break
  \break
  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d,   d d | \break
  
%  \repeat volta 2{
%    c4 c4 c8 c d e | d4 d4~ d8 a'8 g f | 
%  }  
%  \alternative{
%    { e4 e e8 e f g | f2 r8 d d d }
%  { e4 e e8 e f e | d2 r8 d d e }
%  }

  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f | e4 e e8 e f g | f2 r8 d d d \break
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f | e4 e e8 e f e | d2 r8 d d e \break

  \break
  
  %\repeat volta 2{
  %  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d |
  %}
%  \alternative {
%    { cs2 r8 cs d e |}
%    { a2 r8 |}
%  }
  
  f2 f8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 cs d e | \break 
  f2 r8 a g f | c'2 e,  | d2 r8 f e d | a'2 r2 \break


  d,4 d8 e f e d4 | d8 e f e d4 d8 e | f e d e f a g f | e1  |  \break
  
%  \repeat volta 2{
%    g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | 
%  }
%  \alternative{
%    {d4 e f fs |}
%    {d d d8 c bf4 }
%  }
  
  g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d4 e f fs |\break
  g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d d d8 c bf4 \break
  
  c4 c c8 bf a4 | bf bf8 a g  f e a | d,2 r4 f'8 e | d2. f8 e | \break
  d2. f8 e | d2. f8 e | d2. f'8 e |  d2. f8 e | \break
  d2. f8 e | d2 r8 f8 e d | cs1 | r4 a4 d r 
  \bar "|."
}


bass ={
  \relative c {
    \time 4/4
    \clef F
    \key d \minor  
    
  
    d4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a \break
    d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a \break
    \bar "||"
    d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a \break
    d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a | d,4. d8 f4 a \break
    \bar "||"
    d,4. d8 f4 a | c,4. c8 g'4 f | bf,4. bf8 d4 f | a,4. a8 e'4 d \break
    g4. g8 c8 c d4 | f,4. f8 c'4 b | f4. e8 f4 a | d,4 d8 d~ d8 d8 d4 | \break
  
    d4 d8 d~ d8 d8 d4 | d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  \break
    d4 d8 d~ d8 d8 d4 | d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  \break
    
  %  \repeat volta 2{ f4 c f c | c g' c, g' | d a' d, a' | a e a e } 
  %  f c f c | f c f c | f c f c | c g' c g | \break
  %  a e c a | d d d2 | a'4 e gs a | d, a d d | \break
    
    f4 c f c | c g' c, g' | d a' d, a' | a e a e | \break
    f4 c f c | c g' c, g' | d a' d, a' | a e a e | \break
    
    f c f c | f c f c | f c f c | c g' c g | \break
    a e c a | d d d2 | a'4 e gs a | d, a d d | \break
  
    %\bar "||"
    
    c4 c8 c~ c8 c8 c4 | d4 d8 d~ d8 d8 d4  | c4 c8 c~ c8 c8 c4  | d4 d8 d~ d8 d8 d4  \break
    c4 c8 c~ c8 c8 c4 | d4 d8 d~ d8 d8 d4  | c4 c8 c~ c8 c8 c4  | d4 d8 d~ d8 d8 d4  \break
   
  %  \repeat volta 2 { 
  %    f4. a8 a4 c | c,4. c8 e4 g | d4. d8 f4 a | a,4. a8 e'4 a | \break
  %  }
    f4. a8 a4 c | c,4. c8 e4 g | d4. d8 f4 a | a,4. a8 e'4 a | \break
    f4. a8 a4 c | c,4. c8 e4 g | d4. d8 f4 a | a,4. a8 e'4 a | \break
    
    d,4 d8 e f e d4 | d8 e f e d4 d8 e | f e d e f a g f | e1  |  \break
    \bar "||"
    g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d4 e f fs | \break
    g d g d | d a' d, a' | e cs a e' | bf' f bf f |   \break
    f c f c | e bf' a e | d4 d8 d~ d8 d8 d4 | d4 d8 d~ d8 d8 d4  |\break
    d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4 \break
    d4 d8 d~ d8 d8 d4  | d4 d8 d~ d8 d8 d4  | a' e cs a | r a d r \bar "|."
  }
}


alto = \relative g {
  \time 4/4
  \key d \minor  
  
  R1*7 | r2 r8 d d e | 
  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d d e | \break

  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d d d | \break


  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f |  e4 e e8 e f g | f2 r8 d d d 
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f |  e4 e e8 e f e | d2 r2
  %d' d e
  \break
  

  %{
  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 cs d e | \break
  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 d d e | \break
  \break
  %}
  a'1 | c | d | e2~ e8 e f g  | \break
  a1 | g1 | f | e1   \break
  
  f,4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 g c d | \break
  %c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d,   d d | \break
  e1 | f2. e4 | a,2 c8 b a cs | d1 \break
  

  c,4 c4 c8 c d e | d4 d4~ d8 a'8 g f | e4 e e8 e f g | f2 r8 d d d \break
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f | e4 e e8 e f e | d2 r2 
  %r8 d d e

  \break
  
  %{
  f2 f8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 cs d e | \break 
  f2 r8 a g f | c'2 e,  | d2 r8 f e d | a'2 r2 \break
  %}

  a'1 | c | d | e  | \break
  a,1 | g | f | e1   \break


  d4 d8 e f e d4 | d8 e f e d4 d8 e | f e d e f a g f | e1  |  \break
  
  <<  
  {
  \voiceOne

  g'4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d4 e f fs |\break
  g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d d d8 c bf4 \break
  
  c4 c c8 bf a4 | bf bf8 a g  f e a | d,2 r2  | bf'8 f d bf'~ bf f d4  | \break
  }
  \new Voice{
  \voiceTwo
    bf'4 bf bf8 a g4 | a4 a a8 g f4 | g g g8 f e4 | f4 g gs a |\break
    bf4 bf bf8 a g4 | a4 a a8 g f4 | g g g8 f e4  | f f f8 e d4 \break
    e e e8 g f4 | g4 g8 f e d cs e | d2
  }
  >>
   a'8 f d a'~ a f d4 | af'8 f d af'~ af f d4 | a'8 f d a'~ a f d4 | bf'8 f d bf'~ bf f d4
   a'8 f d a'~ a f d4 | af'8 f d af'~ af f d4 |  r1 | r4 a' d r 
   
   %d2. f8 e | d2 r8 f8 e d | cs1 | r4 a4 d r 
  \bar "|."
}



clarinetB = \relative g'{
  \time 4/4
  \key d \minor  
  
  R1*7 | r2 r8 d d e | 
  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d d e | \break

  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 e a b | \break
  c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d d d | \break


  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f |  e4 e e8 e f g | f2 r8 d d d 
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f |  e4 e e8 e f e | d2 r2
  %d' d e
  \break
  

  %{
  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 cs d e | \break
  f2 r8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 d d e | \break
  \break
  %}
  a1 | c | d | e2~ e8 e f g  | \break
  a1 | g1 | f | e1   \break
  
  f4 f f8 e d e | f a f e d d d e | f4 f8 e d e f a | e2 r8 g c d | \break
  %c4 c c8 b a4 | d d8 c b a  gs4 | c8 b a b a gs f e | d2 r8  d,   d d | \break
  e1 | f2. e4 | a,2 c8 b a cs | d1 \break
  

  c,4 c4 c8 c d e | d4 d4~ d8 a'8 g f | e4 e e8 e f g | f2 r8 d d d \break
  c4 c4 c8 c d e | d4 d4~ d8 a'8 g f | e4 e e8 e f e | d2 r2 
  %r8 d d e

  \break
  
  %{
  f2 f8 a g f | e2 r8 g f e | d2 r8 f e d | cs2 r8 cs d e | \break 
  f2 r8 a g f | c'2 e,  | d2 r8 f e d | a'2 r2 \break
  %}

  a1 | c | d | e  | \break
  a,1 | g | f | e1   \break


  d'4 d8 e f e d4 | d8 e f e d4 d8 e | f e d e f a g f | e1  |  \break
  
  <<  
  {
  \voiceOne

  g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d4 e f fs |\break
  g4 g g8 f e4 | f4 f f8 e d4 | e e e8 d cs4 | d d d8 c bf4 \break
  
  c4 c c8 bf a4 | bf bf8 a g  f e a | d,2 r2  | bf'8 f d bf'~ bf f d4  | \break
  }
  \new Voice{
  \voiceTwo
    bf'4 bf bf8 a g4 | a4 a a8 g f4 | g g g8 f e4 | f4 g gs a |\break
    bf4 bf bf8 a g4 | a4 a a8 g f4 | g g g8 f e4  | f f f8 e d4 \break
    e e e8 g f4 | g4 g8 f e d cs e | d2
  }
  >>
   a'8 f d a'~ a f d4 | af'8 f d af'~ af f d4 | a'8 f d a'~ a f d4 | bf'8 f d bf'~ bf f d4
   a'8 f d a'~ a f d4 | af'8 f d af'~ af f d4 |  r1 | r4 a' d r 
   
   %d2. f8 e | d2 r8 f8 e d | cs1 | r4 a4 d r 
  \bar "|."
}



\book {
  \bookOutputName "stille_tage_all"
  \score {
    
    <<
      
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff \with {instrumentName = #"Clarinet (C)"}{
         \transpose c c {
           \tempo 4=160
           \melody
         }
      }

      \new Staff \with {instrumentName = #"Alto (C)"}{
         \clef C
         \alto
      }

      \new Staff \with {instrumentName = #"Cla 2 (C)"}{
         \transpose c c {
           \clarinetB
         }
      }

      \new Staff \with {instrumentName = #"Bass (C)"}{
         \transpose c c {
           \bass
         }
      }
  
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "stille_tage_C1"
  \score {
    
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff \with {instrumentName = #"Clarinet (C)"}{
         \transpose c c {
           \tempo 4=160
           \melody
         }
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "stille_tage_C2"
  \score {    
    <<      
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff \with {instrumentName = #"Cla 2 (C)"}{
         \transpose c c' {
           \clarinetB
         }
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "stille_tage_C2_clef_C"
  \score {
    
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }

      \new Staff \with {instrumentName = #"Alto (C)"}{
         \clef C
         \alto
      }  
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "stille_tage_Bb1"
  \score {
    
    <<
      
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      
      \new Staff \with {instrumentName = #"Clarinet (Bb)"}{
         \transpose c d {
           \tempo 4=160
           \melody
         }
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "stille_tage_Bb2"
  \score {    
    <<      
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      
      \new Staff \with {instrumentName = #"Cla 2 (Bb)"}{
         \transpose c d {
           \clarinetB
         }
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "stille_tage_bass"
  \score {    
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff \with {instrumentName = #"Bass (C)"}{
         \transpose c c {
           \bass
         }
      }
  
    >>
    \layout { }
  }
}
