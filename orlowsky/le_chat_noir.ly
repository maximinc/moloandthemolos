\version "2.18.2"
\language "english"

\header {
  title = "Le chat noir"
  composer = "David Orlowsky Trio"
}

global = {
  \set Score.skipBars = ##t
  \time 3/4
}

words = \lyricmode {
  
}

motifa=\relative c''{
  a4. bf8 a4 e'2 \appoggiatura {f16 e} d4 a2. g2.
}
motifba=\relative c'{
  f4. g8 f4 a2 g4 e2. r2.
}
motifbb=\relative c'{
  f4. g8 f4 a4 c a e2. r2.
}

motifc=\relative c'{
  d4. bf8 d e f2. d4. a8 d e f2.
}
motifda=\relative c''{
  a2 a4 a\mordent g f e2. r
}

motifdb=\relative c'{
  e2 c4 e2 g4 f8 e d2
}

motifabis=\relative c''{
  a4. bf8 a4 e'2 \appoggiatura {f16 e} d4 f2 d4 bf2.
}

motifbc=\relative c''{
  g4 bf d g,2. f4 a c e,2.
}

motifater=\relative c''{
  a4. bf8 a4 e'2 \appoggiatura {f16 e} d4 c2 d4 bf2.
}

motifbd=\relative c''{
  a4 g f g f e d2. r
}

motifdc=\relative c''{
  a2 bf4 a g f e2. r
}


motife=\relative c''{
  f4. a8 f e f4. a8 e d e4 a d,8 \mordent c d2.
}

motifeb=\relative c''{
  f4 a f8\mordent e f4 a e8\mordent d e4 a d,8\mordent c d2 r8 e f4 e d8 c \tuplet 4/3 {bf4 c d e} a,2.
}

motifec=\relative c''{
  f4. a8 f e f4 a e8\mordent d e4 a d,8 \mordent c  d2. \> 
}


motiffa=\relative c''{
  bf4. g8 bf d cs2. ~ cs4. a8 cs e f2.
}

motiffb=\relative c''{
  bf4.\mp  g8 bf d cs4. a8 f' e d2.
}

monteeA=\relative c'{
  a8\fff d f a d e
}

monteeB=\relative c'{
  a8 d f a d e  
}


monteeb=\relative c''{
  r4 cs8 d cs d 
}

motifhsuba=\relative c''{
  bf4. d8 f4 f2 e4 c e a a2 g4 bf,4. d8 f4 f2 e4 c e a g2.
  \break
  bf,4. d8 f4 f2 e4 a, c f e2 d4 bf4. d8 f4 f2 e4 d2. r2.
  \break
}
motifh=\relative c''{
  \motifhsuba
  bf,4. d8 f4 f2 e4 c e a a2 g4 bf,4. d8 f4 f4 e d c e a g2.
  \break
  bf,4. d8 f4 f2 e4 a, c f e2 \mordent d4 bf4. d8 f4 f2 e4 d2.^"Molo bondit sur une mouche"
}

motifi=\relative c''{
  f4. a8 f e f4. a8 e d e4. a8 d, c d2.
  bf4. g8 bf d cs2.~ cs4. a8 cs e f2.
  \break
  f4. a8 f e f4 a e8\mordent d e4 a d,8\mordent c d2. 
  bf'4. a8 g f e4 a g8\mordent f e2.
  a,,8 d f a d e
  \break
  f4. a8 f e f4. a8 e d e4 a d,8 \mordent c d2. g4. bf8 a g \tuplet 4/3 {f4 e f g} a4 d2~ d4 d4 d 
  \break
  d a a a f f f e e d \> r r 
  bf4. \mp g8 bf d cs4. a8 f' e d2.^"Molo s'allonge"
}

maintheme={
  \motifa
  \motifba
  \break
  \motifa
  \motifbb
  \break
  \motifc
  \motifda
  \break
  \motifc
  \motifdb r2.
}


clarinet={
  \key d \minor
  \maintheme
  
  \break
  \motifa
  \motifbb
  \break
  \motifabis
  \motifbc
  \break
  \motifater
  \motifbd
  \break
  \motifc
  \motifdc
  \break
  \motifc
  \motifdb 

  \monteeA
  \bar "||"
  \break
  \motife
  \motiffa
  \break
  \motife
  \motiffb
  \monteeB
  \break
  \motifeb
  \monteeB
  \break
  \motifec
  \motiffb
  \monteeb
  \break
  \bar "||"
  \motifh
  \monteeA
  \bar "||"
  \break
  \motifi
  \monteeb
  \break
  \motifhsuba
  \bar "||"

  \maintheme
  
  \transpose c c,{
    \motifabis
    \motifbc
    \break
    \motifabis
  }
  
  \relative c'{
    a4 g f g f e d2. r
  }
  \break
   
  \relative c'{
    a4^"Molo s'endort" g f g f e d2. r^"Purr purr purr"
  }
  \bar "|."
}

bass={
  \key d \minor
  %%%
  \clef "F"
  \relative c'{
    d2.~ d  e~ e f~ f g~ g \break
    f~ f d~ d c2.~ c2  a4 a2.~ a \break
    
    bf2. ~ bf2 c4 d2.~ d
    a2.~ a2 a4 a,2.~ a \break
    
    bf'2. ~ bf4 f'  e d2.~ d4 f4 e a,2.~ a4 f' e d2.~ d  \break
  }
  \relative c'{
    d2.~ d a2. c bf f' c~ c4 f e  \break
    d2.~ d bf2.~ bf4 d f g2. g f a, \break
    
    d2.~ d bf~ bf f' c d~ d \break
  }
  
  \relative c'{
    bf2. f4 bf c d2. bf4 d c f,2. ~ f4 f g a2. e4 a c  \break
    bf2. f4 bf c d2. bf4 d df c2. c  d~ d  \break  \bar "||"
  }
  \relative c{
    d2. \fff f a, d g4. g8 g g a4. a8 a4 a4 cs e, d f a \break
    d,2. d2 d4 a'2 e4 d2. g2 g4 g a e d f a d,2. \break
    
    d2. f a d,2. bf'4. bf8 bf4 bf4 bf g4 a g e a,2. \break
    d2. f a d4 \> e f g2. \mp a d, r \break \bar "||"
  }
  \relative c'{
    bf2.~ bf4 c d c2.~ c4 d4 e 
    bf2.~ bf4 c d c2.~ c4 d4 e  \break
    bf2.~ bf4 c d f2.~ f
    bf,2.~ bf4 f' e d2.~ d \break
  }
  r2. r r r r r r r  \break
  r2. r r r r r r^"Molo bondit sur une mouche" r \break
  
  \relative c{
    d2. \fff f a, d g4. g8 g g a4. a8 a4 a4 cs e, d f a \break
    d,2. d2 d4 a'2 e4 d2. g4. g8 g4 g g g a g e a,2.  \break
    
    d2. f a d,2. g4. g8 g4  g4 g g f a g f f e  \break
    d2.~ d a d \> g \mp a d^"Molo s'allonge" r \break
  }
  \relative c'{
    bf2.~ bf2. c~ c d~ d e~ e \break
    f~ f2 f4 f2.~ f bf,2.~ bf4 f' e d2. r \break \bar "||"
  }
  
  
  \relative c'{
    d2.~ d  e~ e f~ f g~ g  \break
    f~ f2 f4 d2.~ d c2.~ c4  c bf a2.~ a \break
    
    bf2. ~ bf2 c4 d2.~ d
    a2.~ a2 a4 a,2.~ a  \break
    
    bf'2. ~ bf4 f'  e d2.~ d4 f4 e a,2.~ a4 f' e d2.~ d    \break
  }
  \relative c{
    a2.~ a bf~ bf g~ g f a \break
    d~ d bf'~ bf 
    f,2. c' d r \break
    f,2.^"Molo s'endort" c' d r^"Purr purr purr"
    \bar "|."
  }
}


chordNames=\chordmode{
  d1.:m a:m7/e bf2. f c1./a
  d1.:m/f a:m f a:m
  bf d:m f a:m bf d:m a:m d:m
  
  d1.:m a:m7 f c
  d:m bf g:m f2. a:m
  d1.:m bf f2. c d1.:m
  
  bf d:m f a:m
  bf d:m c d:m
  
  d2.:m s a:m d:m
  g:m a:7 s d:m
  d:m s a:m d:m
  g:m a:7 d:m s 
  
  d:m s a:m d:m
  bf s a:m s
  
  d:m s a:m d:m 
  g:m a:7 d:m s
  
  bf s c s bf s c s
  bf s f s bf s d:m s
  bf s c s bf s c s
  bf s f s bf s d:m s
  
  d:m s a:m d:m g:m a:7 s d:m
  d:m s a:m d:m g:m s a:7 s
  d:m s a:m d:m g:m s f s
  
  d:m s a:m d:m g:m a:7 d:m s
  
  bf s c s bf/d s c/e s
  bf/f s f s bf s d:m s
  
  d1.:m  a:m bf2. f c1.
  d:m g:m9 f a:m
  
  bf d:m f a:m
  bf d:m a:m d:m
  
  d:m bf g:m f2. a:m
  d1.:m bf  
  f2. c d:m s
  f2. c d:m s
}



\book {
  \bookOutputName "le_chat_noir_all"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff  \with {
        instrumentName = #"Clarinet (C)"
      }{ 
        \global
        \transpose c c { 
           \clarinet
        }
      }
      
      \new Staff  \with {
        instrumentName = #"Bass (C)"
      }{ 
       \global        
       \bass
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "le_chat_noir_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff  \with {
        instrumentName = #"Guitar (C)"
      }{ 
        \global
        \transpose c c { 
           \clarinet
        }
      }
      
      \new Staff  \with {
        instrumentName = #"Bass (C)"
      }{ 
       \global        
       \bass
      }
    >>
    \layout { }
  }
}
\book {
  \bookOutputName "le_chat_noir_bass"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with {
        instrumentName = #"Bass (C)"
      }{ 
       \global        
       \bass
      }
    >>
    \layout {}
  }
}
\book {
  \bookOutputName "le_chat_noir_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      
      \new Staff  \with {
        instrumentName = #"Clarinet (Bb)"
      }{ 
        \global
        \transpose c d{ 
           \clarinet
        }
      }
     >>
    \layout { }
  }
}