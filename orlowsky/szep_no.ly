
\version "2.18.2"
\language "english"

\header {
  title = "Szep no"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 3/4
}


chordNames={
  \chordmode{
    a1.:m a:m 
    e:7 e:7
    e:7 e:7
    a1.:m e:7

    a1.:m a:m 
    d:m d:m
    a2.:m f e1.:7

    a:m a:m
    
    d:m d:m
    a:m a:m
    d:m d:m
    
    e:7 e:7 
    
    f:maj7 a:m
    bf a:m
    
    f:maj7
    fs:dim7
    g:13
    e:7 e:7
    e:7 e:7
    e:7 e:7
    
    e:7 s2.
    
    a1.:m a1.:m 
    a1.:m a1.:m 
    
    a1.:m a1.:m 
    a1.:m a1.:m 
    
    a1.:m a:m 
    e:7 e:7
    e:7 e:7
    a1.:m e:7

    a1.:m a:m 
    d:m d:m
    a2.:m a:m  b:m7.5- e:7
    
    s s e:7 s
    s s 
    a1.:m
    a:m a:m 
    s s2.
    d:dim7 s s s s d:dim7s
    
  }
}


clarinet={
  \key a \minor


  \relative c''{
    e4 f8 e ds e ds d c b c d e f e ds e a g f e ef d c 
    \break
    d4 d8 c b4~ b2. r r
    
    \break
    b8 as b c d e f e ds e f g gs g f e ds e f b, e ef d b
    \break
    c d e2~ e4. f8 e ds e2.
    r8 c b d c ds  \break

    e4 f8 e ds e ds d c b c d e f e ds e a g f e ef d c 
    \break
    d e f2 r2. r2. r4. 
    b,8 c d 
    \break
    e f e ef d c b c b a gs b 
    
    <<
      {f' e d c b d gs f e ds e gs
    \break
    a2.~ a2. r r
      
      }
      \\
      \relative c{
        e4 gs b e d c a2 g4 a2 g4 a4 a8 a a a a4
      }
    >>
    
    \bar "||"
    \break
    f4 f8 e f g f4 f e f f8 e f g f4 f d8 ds
    
    \break
    e4. f8 e ds e d c b c d e2.
    a,8 b c d ds e 
    
    \break
    f4 f8 e f g f4 f e f f8 e f g f4 f d8 ds
    
    \break
    <<
    {e4. f8 e ds e d c b c d e2.}
    \\
    \relative c' {
      gs4 b d f e4. c8 b2.
    }
    >>
    gs,8 a b c d ds
    
    \break
    e2 f4 e4. f8 e ds e2 c4 a4. a8 b c \break
    d4 d e f f8 e d4 c2.  r4. e,8^\pp  a b \break
    c4 c8 b4 a8 c4 b a 
    d4 d8 c4 b8 d4 c b 
    \break
    e^"Ralentir" f e d  \grace{e16 d } c8 b c d
    
    e4^"I'd rather be ... free ee- ee" \grace{f16 e} ds4 e 
    \bar "||"
    \break
    
    \grace {e16} f8 e ds4 e4
    \grace {e16} f8 e ds4 e8 c
    
    c b b4
    \grace {e16} f8 e ds4 e8 c c b b a a gs gs4 \break
    
    gs4 f8 gs f4 e8 d d  c8 c b b2 s4
    
    b8 c b e4 d8 \break 
    c e d c c b b g g e e4 e b' e b' e b' e e2~\break 
    \bar "||"
    e2.~ e2.~ e2.~ e2.~
    e2.~ e2.~ e2.~ e2.
    
    r r r r r r r r
    \break
    \bar "||"
  }
  \relative c''{
    
    <<
    {e4 f8 e ds e ds d c b c d e f e ds e a g f e ef d c 
    \break
    d4 d8 c b4~ b2. r r}
    \\
    {c4 d8 c b c b bf a a a b c c c c c c c c c c b a b4 b8 a gs4  }
    >>
    
    \break
    <<
      {b8 as b c d e f e ds e f g gs g f e ds e f b, e ef d b}
      \\
      {}
    >>
       
    \break
    c d e2~ e4. f8 e ds e2.
    r8 c b d c ds  \break

    e4 f8 e ds e ds d c b c d e f e ds e a g f e ef d c 
    \break
    d e f2 r2. r2. r4. 
    b,8 c d 
    \break
    e f e ef d c b c b a gs b f' e d c b d gs f e ds e gs
    \break
    b8 bf a gs b a gs f e d gs f e2.~ \trill e2.
    \break
    
    e4\staccato r2 r4 e8 f g gs a2.~ a2.~ \break
    a2.~ a2.~ a2.~ a2.~ \break
    a4 r2 c,4 r2 e4 r2 
    gs2 b8 gs b gs f e d e f e d c' b a gs gs f f e d e2. \glissando e'4 \staccato r r \bar "|."
  }
}

bass={
  \clef F
  \key a \minor
  \relative c'{
    r2. r r r r r r r
    \break
    r2. r r r r r r r
    \break
    a b c cs d c b bf
    
    \break
    a4-> r r f-> r r e-> r r gs-> r gs
    a g e d c b a r e' a r r
    \break
  }
  \relative c{
    d2 a'4 d,2 a'4 d,2 a'4 d,2 a'4
    a,2 e'4 a,2 e'4 a,2 e'4 a,2 e'4
    \break
    d2 a'4 d,2 a'4 c,2 c'4 c,2 r4
    b2 b4 b d ds e2. r
    \break
    
    f2 c'4 f,2 c'4 
    a,2 e'4 a,2 e'4
    bf2 f'4 bf,2 f'4
    a,2 e'4 
    a,2 e'4
    
    \break
    
    f4. b4 b8 f4 b a
    fs4. b8 b4 fs c' b
    g4.^"suivre Fanny qui ralentit" g8 g4 g g g
    \break
    <e, e'>2.~ e2.~ e2.~ e2.~ e2.~ e2.~ 
    \break
    e2.~ e2.~ e2.~
    e2.~ e2.~ e2.
    
    <<
    { e2.~ e2.~ e2.~ }
    \\
    {\new CueVoice{
      \relative c {e4^"Fanny!" b' e b' e,,^"8ve" b' e e2~}
     }
    }
    >>
    \bar "||"
  }
  \relative c'{
    \break
    a4 e g a e g a e g a e g a e g a e g a e g a e g
    \break
    a4 e g a e g a e g a e g a e g a e g a e g a e g
    \break
    a4 e g a e g a e g a e g 
    
    e b d e b d e b d e b d 
    \break
    e b d e b d e g d e gs g
    a e g a e g e b d e b d
    
    \break
    a' e g a e g a e g a e c
    d a c d  a c b d e f d b 
    
    \break
    a r r c r r b r r fs' a as b r b a r a e r b' e, b' e,
    
    
    e r r r2.
    \break
    a,4. c ds e a, c ds e a,  c ds e 
    \break
    a,4 r2 c4 r2 e4 r2
    
    d2.~ d2.~ d2.~ d2.~ d2. d4-> r r \bar "|."
  }
}




\paper {
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 6)
       (padding . 0.4)
       (stretchability . 60))
} 

\book {
  \bookOutputName "szep_no_all"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
        \global
        \transpose c c { 
          \clarinet
        }
      }    
      \new Staff  \with {instrumentName = #"Bass (C)"}{ 
        \global
        \bass
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "szep_no_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Clarinet (Bb)" }{ 
        \global
        \transpose c d { 
          \clarinet
        }
      }    
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "szep_no_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
        \global
        \transpose c c { 
          \clarinet
        }
      }    
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "szep_no_bass"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with {instrumentName = #"Bass (C)"}{ 
        \global
        \bass
      }
    >>
    \layout { }
  }
}
