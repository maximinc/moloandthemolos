
\version "2.18.2"
\language "english"

\header {
  title = "Satin"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 12/8
}


chordNames={
  \chordmode{
    s1. s s s
    s s s s
    s s s s s
    
    a2.:m d4.:m e:7
    a2.:m d4.:m e:7
    a:m9 d:sus4 e:7 e:7/d
    a1.:m

    a2.:m d4.:m e:7
    a2.:m d4.:m e:7
    a:m9 d:sus4 e:7 e:7/d
    a2.:m a2.:m6
    
    d2.:m d2.:m6
    a2.:m a2.:m
    d2.:m f:7 e1.:7
    
    a2.:m d4.:m e:7
    a2.:m d4.:m e:7
    a:m9 d:sus4 e:7 e:7/d
    a1.:m
    
    %g4.:m g4.:m/d g4.:m/ef g4.:m/d
    %g4.:m g4.:m/d g4.:m/ef g4.:m/d
    
    g1.:m g1.:m
    
    g2.:m d:7 g:m c4.:m d1.:7.9-
    
    g1.:m g2.:m d:7.9-
    
    g2.:m c4.:m d2.:7.9-
    g4.:m

    s1. s s s s s s s s s

    a2.:m d4.:m e:7
    a2.:m d4.:m e:7
    a:m9 d:sus4 e:7 e:7/d
    a2.:m a2.:m6

    d2.:m d2.:m6
    a2.:m a2.:m
    d2.:m f:7 e1.:7

  }
}




themeA={
    e8 a8. b16 c8 e c d c b 
    << 
      \new CueVoice{
        \stemUp
        \relative c'''{
          gs8 e d c4.
        }
      }
      \\
      {gs4.e8 a8. b16 }
    >>
    c8 e g d e f e fs gs \break
    b a8. e16 g8 fs8. f16 f8 e b gs8. d'16 c b
    a8 c e a c b 
    <<
      {a4. r4.}
      \\
      \new CueVoice{
        \stemDown
        \relative c'{
          a8 e' a c e c
        }
      }
    >>
      \break
}

clarinet = {
  \key a \minor
  \relative c'{
    <a e'' a c>4. e'''4. <c, f a e'>4. d' <e,,, b'''>1.\fermata 
    <a e'' a c>4. e'''4. <c, f a e'>4. d' <e,,, b'''>1.\fermata
    
    a8 c' e a4.  e,8 c' e a4. 
    f,8 d' f a4.
    d,,8 d' f b4.
    
    a,,8 c' e a4.  e,8 c' e a4. 
    f,8 d' f a4.
    d,,8 d' f b4.
    %\break
    
    <a,, c' e c'>4 b''8 a e c b ds, fs c' fs a  
    <f, b d a'> g' d b4.
    <e,, b'' e>8 c'' d b4.
    <e,, b'' e>8 c'' d b4.
    
    e,,8 b'' c d e fs gs a b d c b
    <a,, c' e b'>8 a'' b a e c a1. \fermata
    \break
    \bar "||"

  }
    \relative c{
      \themeA
    }
    \relative c'{
      \themeA
    }
    \relative c'{
      d8 f16 e d8 a'4. d,8 f16 e d8 b'4.
      e16\mordent d c b c d  c8 b g~ g b a g f e \break
      d8 f16 e d8 a'4. ef8 a16 g f8 c'4.
      
      b8^"Ralentir" c d c b a gs f e d c b 
    }
    \relative c{
      \themeA
    }
    \bar "||"
    \key g  \minor
    r2. r4. r8 
    \relative c''{
      d ef ef4 d8~ d a bf 
      <<
        { g4. r4. }
        \\
        \new CueVoice {
          \stemDown
          s8 ef_"M" d s bf g
        }
      >>
      
      ef''8 d ef g a bf a bf c a
      d, ef 
      \break
      \time 9/8 ef4 d8~ d g, a bf4.
      \time 12/8
      ef8. d16 c bf a8 bf c d e fs c d ef \break
      ef4 d8~ d a bf g4. r4.
      ef'8 d ef g a bf a bf c a
      d, ef 
      \time 9/8 ef4 d8~ d g, a bf4.
      %\time 12/8
      c8. bf16 a g fs8 a fs
      << {g4. }
         \\
         \new CueVoice{
           \stemDown
         g,8 bf' <d g> }
      >>
   }
      \break
      \bar "||"	
      \time 12/8
      \key a \minor
      \relative c'{
        a <a' c e> f'
        a,, <a' c fs> f'
        a,, <a' c e> f'
        a,, <a' c fs> g'
        
        a,, <a' c e> f'
        a,, <a' c fs> f'
        a,, <a' c e> f'
        a,, <a' c fs> g'
        
        d, <d' f af> a'
        d,, <d' f af>4
        d,8 <d' f af> a'
        d,, <d' f af>4
        \break
        a,8 <a' c e> f'
        a,, <a' c fs> g'
        a,, <a' c fs> f'
        a,, <a' c e> ef'
      
        a,, <a' c e> \staccato f' \staccato 
        a,, <a' c fs> f'
        a,, <a' c e> f'
        a,, <a' c fs> g'
        
        a,, <d' f b> c'
        a,, <d' f af> a'
        a,, <d' f b> c'
        a,, <d' f af> a'

        \break
        <e,, c'' e b'> a'' e c a e b c d b4.
        
        b8 c d b4.
        e,8 b'' c d e fs gs a b d c b
        <a,, c' e b'>8 a'' b a e c a1. \fermata
        \break
        \bar "||"
      }

    \relative c{
      \themeA
    }
    \relative c''{
      a8 g16 f e8 d4.
      a'8 g16 f e8 d4.

      e'16\mordent d c b c d  c8 b g c b a g f e \break
      d8 f16 e d8 a'4. ef8 a16 g f8 c'4.
      
      b8 c d c^"Ralentir" b a gs f e d c b 
      \bar "||" \break
    }
    \relative c''{
      <a, e' > a'8. b16 c8 e c 
      <d, a' d> c' b gs e d c e16 a b8 c e g
      <d, a' d>  e' f <e,, d' gs b e > fs'' gs  \break
      <a,, fs' c' e b'> a''8. e16
      <d, a' d g>8 fs' f <e,, d' gs b f'>  e'' b <e,, d' gs>8. d''16 c b
      
      <a, a'>8 c' e a c b a, c e  <a,, fs' c' e a>4.\fermata
      
      \bar "|."
    }
    
}

themebass={
    a2. d4. e4.
    c2. d4. e4.
    a, d e d
    a4. ~ a4 e'8 a2.
    \break
}

themebassB={
    d4. a'8 d e f4. d,4. 
    a4.  e'8 a b c4. a,4.
    
    d4. a'8 d e f2. e1.^"Ralentir"
    \break
}

bass = {
  \clef "F"
  \time 12/8
  r1. r r r r r r r r r r r r \fermata 
  \bar "||"
  \break
  \relative c{
    \themebass
  }
  \relative c{
    \themebass
  }
  \relative c{
    %a,2. d4. e4.
    %c2. d4. e4.
    %a, d e d
    %a4. ~ a4 e'8 a2.
    
   \themebassB
  }
  \relative c{
    a2. d4. e4.
    c2. d4. e4.
    a, d e d
    c4. ~ c4 b8 a2.
    \bar "||"
    \break
    \key g \minor
    g4. d' ef d 
    g,4. d' ef d 
    g, d' d a'
    \time 9/8
    g d c
    \break
    \time 12/8
    d a' d,4 d8 a'4 a8
    g4. d ef d 
    g d d a'4 a8
    \time 9/8
    g4. d c
    d a' g
    \bar "||"
    \time 12/8

    \key a \minor
    \break
    a e a e'
    a, e a e'
    
    d, f d' b 
    e c  b, c
    \break
    e, e e  e
    
    a gs e f
    e2. e'
    r1.^"" r r\fermata
    \bar "||"
    \break
    
  }
  \relative c{
    \themebass
  }
  \relative c{
    \themebassB
  }
  r1. r r r
  \bar "|."
}


\book{
  \bookOutputName "satin_all"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Claritar (C)"
      }{ 
        \global
        \transpose c c { 
          \clarinet
        }
      }
      
      \new Staff  \with {
        instrumentName = #"Bass (C)"
      }{ 
        \global
        \bass
      }
    >>
    \layout { }
  }
}

\paper{
  %system-system-spacing = #'((basic-distance . 0.1) 
  %                           (padding . 1))
  top-margin = 2
  bottom-margin = 2
}


\book{
  \bookOutputName "satin_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Claritar (C)"
      }{ 
        \global
        \transpose c c { 
          \clarinet
        }
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book{
  \bookOutputName "satin_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Claritar (Bb)"
      }{ 
        \global
        \transpose c d { 
          \clarinet
        }
      }
    >>
    \layout {
      #(layout-set-staff-size 16)
    }
  }
}


\book{
  \bookOutputName "satin_bass"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Bass (C)"
      }{ 
        \global
        \bass
      }
    >>
    \layout { }
  }
}
