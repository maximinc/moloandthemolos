
\version "2.18.2"
\language "english"

\header {
  title = "Anderland"
  composer = "David Orlowsky Trio"
}

global = {
  \set Score.skipBars = ##t
  \time 4/4
}

clarinet={
  \tempo 4=140
  \key d \minor
  r1 r r2 r4. 
  \relative c'{
    d16 \f d d8 a' d f a4. cs8 d a gs f e2 e8 g e g e4 e f8 e\mordent d f a4. d,,16 d  \break
    d8 a' d f a4. cs8 d bf a g bf2 a8\mordent a a a a g f g a2~ a4 r8 d,,16 d  \break
    d8 a' d f a4. cs8 d bf a g e'4. cs8~ cs bf a g a g f e d f d f a4. cs8 \break
    d bf a g ef'2 d8 a f d \appoggiatura f16 e8 g f e \time 2/4 d4 r8
  }
  \relative c'' {
    ef16 d 
    \bar "||"
    \time 4/4
    \break
    c8^"!" d ef ef ef4. c'8 c bf a g bf2 a8  fs ef c d c bf a d2~ d4. ef16 d \break
    c8 d ef ef ef4. c'8 c bf a g d'2 a8 fs ef c d c' bf a  g1 \> \bar "||" \break
  }
  \relative c'{
    r2 \p r8  d e f g f f g~ g4 r
    f8 e e f~ f4 r 
    e8 d d e8~ e4 r r2 r8 d8 e f  \break
    g f f g~ g4 r
    f8 e e f~ f4 r 
    e8 d d cs8~ cs2 r2 r8 e \mp f g \break
    af^"!" g g af~ af4 r g8 f f g~ g4 r f8 e e f~ f4 r r2 r8 e f g \break
    af g g af~ af4 r g8 f f g~ g4 r f8 e e c'~ c bf a4 ~ a2 r4 g8\mf a  \break
    bf a g bf~ bf2 d8 c bf d~ d2 \appoggiatura e16 f8 e d f~ f2~ f2 r4 g,8 a \break
    bf a g bf~ bf2 d8 c bf d~ d2 f8 e d d~ d2 r1 \bar "||" \break

    
  }
  \relative c'{
    \new CueVoice{
      <f af c>1^"Xim"
      <f af df>
      <f bf d>
      <g bf d>
      <af c f>
      <f a e'>
      <c' ef g>
      <ef gf bf>
      <df f c'>
      <ef g c>
    }
  }
  \break
  \relative c'''{
    \new CueVoice{
    df16^"Maximin" c bf af g f ef df c bf af g f ef df c df8 f~ f af8~ af2
    d8 f d c'~ c bf~ bf g~ g4 r4 r4. af8 c d~ d f~ f4 r \break
    \tuplet 3/2 {e4 d a~} a2
    c8 ef c g~ g2
    af bf c df
    \tuplet 3/2 {d4 c g d c g} \break
    }
  }
  <<
  \relative c''{
    c16^"Fanny" f af f c f af16 f c f af16 f c f af16 f
    df f af16 f df f af16 f df f af16 f df f af f
    bf, d f d bf d f d bf d f d bf d f d
    g, bf d bf g bf d bf g bf d bf g bf d bf 
    f af c af f af c af f af c af f af c af 
    %e fs g fs e fs g fs e fs g fs e fs g fs 
    d, e f e d e f e d e f e d e f e
    c d ef d c d ef d c d ef d c d ef d 
    ef f gf f ef f gf f ef f gf f ef f gf f
    bf, c df c bf c df c bf c df c bf4
    c16 d ef d c4 r2
  }
  \new Staff {
  \once \omit Staff.TimeSignature
  \relative c' {
    \key d \minor
    r8^"suite Maximin" f g4 f r
    r8 g~ g g g af f4
    r8 c'~ c bf af4 f
    g4. af8 bf c af bf \break
    r8 d~ d c~ c d~ d ds e4 fs gs a c d ef f fs
    b,16 bf fs f ef4 r \break
    r2 r8 bf'~ bf bf8 bf4 g r2
  }

  }
  >>

  \relative c'{
    c16 d ef d c4 r4. d16 \ff d
    
    \bar "||"
    d8 a' d f a4. cs8 d a gs f e2 e8 g e g e4 e f8 e\mordent d f a4. d,,16 d  \break
    d8 a' d f a4. cs8 d bf a g bf2 a8\mordent a a a a g f g a2~ a4 r8 d,,16 d  \break
    d8 a' d f a4. cs8 d bf a g e'4. cs8~ cs bf a g a g f e d f d f a4. cs8 \break
    d bf a g ef'2 d8 a f d \appoggiatura f16 e8 g f e \time 2/4 d4 r8

  }

  \relative c'' {
    ef16 d 
    \bar "||"
    \time 4/4
    \break
    c8 d ef ef ef4. c'8 c bf a g bf2 a8  fs ef c d c bf a d2~ d4. ef16 d \break
    c8 d ef ef ef4. c'8 c bf a g d'2 a8 fs ef c d c' bf a 
    
    \time 6/8 
    \tempo 4. = 62
    g2.  \p
  }

  \relative c'{
    r4. d'8^"Luc" e f 
    \bar "||" \break
    g f f g4.
    f8 e e f4.
    e8 d d e4. r4. d8 e f  \break
    g f f g4.
    f8 e e f4.
    e8 d d cs4. r4. e,8 f g \break
    \time 4/4
    \tempo 4 = 130
    <<
    {
      c1 bf a~ a
      c1 ef f~ f
      f2. g a r2 g8 a 
      bf a g bf4.
      d8 c bf d4. f8 e d d~ \< d2~ d2 \ff
    } 
    \\
    {af,8 g g af~ af4 r g8 f f g~ g4 r f8 e e f~ f4 r r2 r8 e f g \break
    af g g af~ af4 c g8 f f g~ g4 r f8 e e c'~ c bf a4 ~ a2 r4 g8 a  
    \break
    \time 6/8

    \tempo \markup {
      \concat {
      (
      \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
      )
      }
    }
    bf a g bf4. d8 c bf d4. \appoggiatura e16 f8 e d f4.~ f  r8 g,8 a \break
    bf a g bf4. d8 c bf d4. \time 4/4 f8 e d d~ d2~ d2.
    }
    >>
  }
   \relative c'{
    r8 d16 d
    \bar "||" \break

    d8 a' d f a4. cs8 d a gs f e2 e8 g e g e4 e f8 e\mordent d f a4. d,,16 d  \break
    d8 a' d f a4. cs8 d bf a g bf2 a8\mordent a a a a g f g af1~ af2~ af4. d,,16 d \break

    d8 a' d f a4. cs8 d bf a g e'4. cs8~ cs bf a g a g f e d f d f a4. cs8 \break
    \time 6/4 d bf a g ef'1  \time 4/4 d8 a f d \appoggiatura f16 e8 g f e \tuplet 3/2 {d ds e f e ef} d4 r \bar "|."
   }

}

bass={
  \key d \minor
  \relative c'{
    r2 a8 g f e 
    d4 a d a d a d a
    \bar "||" \break
    d a d a d a e' b e b e b d a d a  \break
    d a d a g' d g d a' e a e f c f f8 e \break
    
    d4 a d a g' d e b a a cs cs d a d a \break
    g' d g g  d d e e \time 2/4 d d8 d16 cs \break
    \time 4/4
    c4 g' c, g' c, c  g' g c, c fs fs g8 d g a bf g a bf \break
    c4 g ef d c f8 fs g4 g c, c fs fs g \> d g d \bar "||" \break
    g \p d	g a 
    bf2. c4 d2. c4 a2. a4 a2. a4 \break
    bf2. c4 d2. c4 a2. a4 a2. a4 \break
    f\mp c' f, c' c, g' c, g' d a' d, a' d, a' d, a' \break
    f c' f, c' c, g' c, g' d a' d, a' d, a' d, g8\mf a \break
    bf4 f bf a g d g f8 e d4 a d a d a  d g8 a \break
    bf4 f bf a g d g d d a d a d a d e  \bar "||"  \break
  }
  
  \relative c{
    f4.^"Élésolo!" af8 g f ef d df4. f8 af4 af8 a bf4. bf8 c d c bf g4 g8 a bf4 bf8 a \break 
    af4 af8 f~ f f af4 a4 f8 e d4. d8 ef d c d ef g bf c bf gf f ef~ ef8 d ef f \break
    bf4 bf8 f df4 df c8 d ef g bf c bf g \break
  }
  \relative c{
    f4 c' f, c' df af df af
    bf f bf f g d g d
    f4 c' f, c'  \break
    d, a' d, a' 
    c, g' c, g'
    ef bf' ef, bf'
    bf f bf f
    c g' c, g' \break
    
    f c' f, c'  df af df af
    bf f bf f g d g d \break
    f c f c
    d a d a 
    c g' c, g'
    ef bf' ef, bf' \break
    bf f bf f
    c g' c, g' 
    c, g' a8 g f ef \break
  }
  \relative c{
    d4\ff a d a d a e' b e b e b d a d a \break
    d a d a g' d g d a' e a e f c f c \break
    d a d a g' d e e a, b cs cs d a d a \break
    g' d g g d d e e \time 2/4 d   r8 d16 df \bar "||" \break
  }
  \relative c{
    \time 4/4
    c4 g' c, g' c, f8 fs g4 g
    c, ef fs a g d g8 g a bf \break
    c4 g ef d c f8 fs g4 g c, c fs fs 
    \time 6/8 
    \tempo 4. = 62
    g4\p g8 g4 g8 g4 g8 g4 a8 
    \bar "||"
    \break
  }
  \relative c'{
    bf4. bf4 c8 d4. d4 c8
    a4.  a4 a8 a4. a4 a8 \break
    
    bf4 bf8 bf4 c8 d4 d8 d4 c8 a4 a8 a4. a4 a8 a4. \break
    \time 4/4
    \tempo 4 = 130
    f4 c' f, c' c, g' c, g' d a' d, a' d, a' d, a' \break
    f4 c' f, c' c, g' c, g' d a' d, a' d, a' d, a' \break 
    \time 6/8
    \tempo \markup {
      \concat {
      (
      \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
      )
      }
    }
    r4. bf4 a8 g4. g4 g8
    d4 d8 a4 a8 d4 d8 d4 c8 \break
    bf4 f'8 bf4 a8 g4 g8 g4 g8
    \time 4/4
    d4 a d a d a d a
  }
  \bar "||"
  \break
  \relative c{
    d a d a d a e' b e b e b
    d a d a \break
    
    d a d a g' d g d
    a' e a e f c f c f c f c \break
    d a d a g' d e e a, a cs cs d a d a \break
    g' d g d g d d \accent r g \accent r d\accent a\accent d\accent r \bar "|."
  }
}

chordNames=\chordmode{
  s1 d1:m d1:m
  d1.:m  e:m d1:m
  d1:m g:m a:m f 
  d1:m g2:m e:m a1:7
  d:m
  g:m d2:m a:7/e d2:m 
  
  c1:m c2:m  g:m d:7.9-/c d:7.9-/fs g1:m
  c1:m c2:m  g:m d:7.9-/c d:7.9-/fs g1:m
  
  s
  g1:m/bf d:m a:m a:m9
  g:m/bf d:m a2:m a2:7 a1:7
  
  f:m c:m d:m d:m
  f:m c:m d:m d:m
  bf g:m d:m d:m
  bf g:m d:m d:m
  
  f:m df bf g:m
  f:m d:m9 c:m ef:m bf:m9 c:m
  
  f:m df bf g:m f:m
  d:m c:m ef:m bf:m  c:m
  
  f:m df bf g:m f:m
  d:m c:m ef:m bf:m c:m c:m
  
  d1.:m  e:m d1:m
  d1:m g:m a:m f 
  d1:m g2:m e:m a1:7
  d:m
  g:m d2:m a:7/e d2:m 

  c1:m c2:m  g:m d:7.9-/c d:7.9-/fs g1:m
  c1:m c2:m  g:m d:7.9-/c d:7.9-/fs g1.:m
  
  g2.:m/bf d:m a:m a:m9
  g2.:m/bf d:m a4.:m a:7 s s
  
  f1:m c:m d:m d:m
  f:m c:m d:m d:m
  
  bf2. g:m d:m s
  bf2. g:m d1:m s
  d1.:m e1.:m d1:m
  d:m g:m a:m f s
  d1:m g2:m e:m a1:7 d:m
  d2:m g1:m  d2:m g:m d4:m  a:7 d:m s

}



\book {
  \bookOutputName "anderland_all"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Clarinet (C)"
      }{ 
         \global
         \transpose c c { 
           \clarinet
         }
       }
  
       \new Staff  \with {
         instrumentName = #"Bass (C)"
       }{ 
           \global       
           \clef "F"
           \bass
        }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "anderland_bass_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with {
         instrumentName = #"Bass (C)"
      }{ 
         \global 
         \clef "F"
         \bass
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "anderland_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Clarinet (Bb)"
      }{ 
         \global
         \transpose c d { 
           \clarinet
         }
       }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "anderland_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff  \with {
        instrumentName = #"Guitar (C)"
      }{ 
         \global
         \transpose c c { 
           \clarinet
         }
       }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "anderland_bass_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      \new Staff  \with {
         instrumentName = #"Bass (Bb)"
      }{ 
         \global        
         \transpose c d' { \bass }
      }
    >>
    \layout { }
  }
}
