
\version "2.18.2"
\language "english"

\header {
  title = "Indigo"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 6/8
}


\paper {
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 6)
       (padding . 0.4)
       (stretchability . 60))
} 




chordNames = {
  \chordmode {
    bf2.:m  gf f:sus4 f:m
    bf2.:m  gf/ef f:sus4/df f:m
    
    bf:m ef:m df f:m
    bf:m ef:m gf df
    
    gf bf:m df f:m
    
    bf:m ef:m df f4.:m
    
    gf2. bf:m gf4. f bf2.:m
    
    s4. s
    gf s s
    bf:m s gf s
    s bf:m s
    
    gf s s bf:m s
    gf  s s bf:m s
    
    gf4. bf:m gf bf:m ef:m b af s
    gf4. bf:m gf bf:m ef:m b af s
    
    df2. bf:m  bf4.:m df bf2.:m
    
    gf4. bf:m gf bf:m b ef:m af
    
    bf2.:m ef:m df f:m
    bf:m ef:m gf  df
    gf bf:m  df f4.:m
    
    gf2. bf:m gf4. f bf2.:m
    
    fs:m cs fs:m/d cs 
    fs:m/d  cs b:m cs
    
    a e d cs:m
    a e d fs:m
    
    gf4. bf:m gf bf:m ef:m b af s
    gf4. bf:m gf bf:m ef:m b af s

    df2. bf:m bf4.:m df bf2.:m
    gf4. bf:m gf bf:m b ef:m af
    
    bf2.:m ef:m df f:m
    bf:m ef:m gf  df
    gf bf:m  df f4.:m
    
    bf2.:m ef:m df f4.:m

    gf2. bf:m  gf4. f bf2.:m
    gf4. f bf2.:m

  }
}

themeC= \relative c'{
   df'8 bf4 df8 bf4 df8 c bf c bf4
   ef8 bf4 ef4 gf16 f ef8 c df ef df c \break
   
   df8 bf4 df8 bf4 df8 c bf c bf4
   ef8 bf4 ef4 gf16 f ef8 c df ef df c  \break
   
   af4. f4 af8 bf4. r4.
   bf8 c bf af f af 
   
   <<
   { bf4. r4.}
   \\
   \new CueVoice {
     \relative c{
       r8 f16 bf df8 f df bf
     }
   }
   >>
   \break

}

clarinet = \relative c'{
   \key bf \minor  
   
   \new CueVoice{
     \relative c'{
       <bf' df>8^"Maximin" f <bf df>8 f <bf df>8 f 
       <bf df>8 gf <bf df>8 gf <bf df>8 gf 
       <bf c>8 f <bf c>8 f <bf c>8 f 
       <af c>8 f <af c>8 f <af c>8 f 

       <bf df>8 f <bf df>8 f <bf df>8 f 
       <bf df>8 gf <bf df>8 gf <bf df>8 gf 
       <bf c>8 f <bf c>8 f <bf c>8 f 
       <af c>8 f <af c>8 f <af c>8 f 
     }
   }
   
   %r4. r r r r r r r
   %r4. r r r r r r r
   \bar "||" \break
   bf4.~ bf8 df bf gf'4 ef8~ ef4.
   df4 f8~ f8 ef8 df c4.~ c8 df8 c  \break
   bf4.~ bf8 df bf gf'4 ef8~ ef4.
   bf'8 bf16 af gf8 gf bf gf f f16 ef df8 df c df \break
   bf4.~ bf8 bf' gf df'4 bf8~ bf4. f'8 f16 ef df8 df ef c8~ c4. r4. \break
   
   bf,4.~ bf8 df bf gf'4 ef8~ ef4.
   df4 f8~ f8 ef8 df \time 3/8 c4. \break
   
   \time 6/8
   bf4.~ bf8 bf' gf df'4 bf8~ bf4. df8 c bf a bf c 
   
   <<
   {bf4. r4. }
   \\
   \new CueVoice {
     \stemDown
     \relative c'{ bf8_"Max" f' bf df bf f' } 
   }
   >>
   \break
   \time 3/8
   \bar "||"
   <<
     {r4. }\\
   \new CueVoice {
     \stemDown
     \relative c'{ df8 bf f } 
   }
     
   >>
   r4
   bf,16 af bf4.~ bf4 bf16 af bf4 c8 df4.~ df4 bf16 af \break
   bf4.~ bf4 bf16 af bf4 c8 df f,4~ f4 bf16 af bf4.~ bf4 bf16 af \break
   
   
   bf4 c8 df4.~ df4 bf16 af bf4.~ bf4 bf16 af bf4 c8 
   <<
   \new CueVoice {\stemUp 
   \relative c''{bf8^"Max" df f bf4.}   
   }
   \\
   {df16 c bf4~ bf4. }
   >>
   
   \break
   \bar "||"
   
   \time 6/8

  \themeC

   \relative c'' {
   df,8 bf4 df8 bf4 df8 c bf c bf4
   ef8 gf4 ef8 gf4 \time 3/8 ef8 df c 
   
   \break
   \bar "||"	
   \time 6/8
   bf4.~ bf8 df bf gf'4 ef8~ ef4.
   df4 f8~ f8 ef8 df c4.~ c8 df8 c  \break
   bf4.~ bf8 df bf gf'4 ef8~ ef4.
   bf'8 bf16 af gf8 gf bf gf f f16 ef df8 df c df \break
   bf4.~ bf8 bf' gf df'4 bf8~ bf4. f'8 f16 ef df8 df ef c8~ \time 3/8 c4.  \break
   
   \time 6/8
   bf4.~ bf8 df8 bf df4 bf8~bf4. df8 c bf a bf c bf4. r4. \break
   }

  \key fs \minor
  <<
  \relative c''{
      a'8 gs gs fs4 gs8 gs8. fs16 f8 f4.  a8 gs fs fs f fs gs4. r4.  \break
      a8 gs gs fs4 gs8 gs8. fs16 f8 f4.  d4  b8 fs'4 f8 cs4. r4. \break
      
      a,8. b16 cs8 cs4. e8 b a gs4. fs4 d8 fs4 b8 gs4. r4. \break
      a8. b16 cs8 cs4. e8 b a gs4. fs8 d fs a4 gs8 fs4.  r4. \break
      
  }
  \\
  \relative c'{
    fs8 gs a fs gs a gs fs f gs fs f
    fs8 gs a fs gs a gs fs f cs4.
    fs8 gs a fs gs a gs fs f gs fs f
    fs4. d cs~ cs
  }
  >>
  
  \key bf \minor
  \themeC

   df'8 bf4 df8 bf4 df8 c bf c bf4
   ef8 gf4 ef8 gf4 \time 3/8 ef8 df c  \break

  \time 6/8
   bf4.~ bf8 df bf gf'4 ef8~ ef4.
   df4 f8~ f8 ef8 df c4.~ c8 df8 c  \break
   bf4.~ bf8 df bf gf4 ef8~ ef4.

   bf'8 bf16 af gf8 gf bf gf f f16 ef df8 df c df \break
   bf4.~ bf8 bf' gf df'4 bf8~ bf4. f'8 f16 ef df8 df ef c8~ \time 3/8 c4. \break
   
   \time 6/8 
   bf4.~ bf8 df bf gf'4 ef8~ ef4. 
   df4 f8~ f8 ef8 df \time 3/8 c4. \break
   
   \time 6/8
   
   bf4.~ bf8 bf gf df4 bf8~ bf4.
   
   df8 c bf a bf c 
   <<
     \relative c'''{
       \new CueVoice{
         \stemUp
         bf8 f df bf4.
       }
     }
     \\
     {bf4. r4.}
   >>
   
   df'8^"Ralentir" c bf a bf c bf4. r4. \bar "|."

}


bass=\relative c{
  \clef "F"
  
  \key bf \minor  
   
  \new CueVoice{
    \relative c{
      <bf' df>8^"Maximin" f <bf df>8 f <bf df>8 f 
      <bf df>8 gf <bf df>8 gf <bf df>8 gf 
      <bf c>8 f <bf c>8 f <bf c>8 f 
      <af c>8 f <af c>8 f <af c>8 f 
    }
  }
  \break
  df'4. bf ef gf f df f c \bar "||"
  \break
  df4. bf ef gf f df f c     
  \break
  df4. bf ef gf gf df df f
  \break
  gf df
  bf df df f f c
  \break
  df bf ef gf f df \time 3/8 f, 
  
  \break
  \time 6/8
  gf df' bf df gf, f bf~ bf 
  
  \break
  \time 3/8
  r4. 
  \new CueVoice{
    \relative c{
      r4 bf16^"Fanny" af bf4.
    }
  }
  
  gf~ gf
  bf~ bf
  r4.
  gf~ gf
  \break
  bf~ bf
  r4.
  gf  f
  bf  c df gf, f bf~ bf
  \break
  
  \bar "||"
  
  \time 6/8
  
  r r r r r r r r
  \break
  df df4 df8 df4. df
  ef ef4 ef8 ef4. ef

  \break
  df,~ df4 c8 bf4.~ bf
  
  bf4 c8 df4 c8 bf4.~ bf
  \break
  
  gf'' f gf bf, b ef  \time 3/8 af,
  
  \break
  
  \time 6/8
  
  df bf ef gf f df f c
  df bf ef gf gf df df f  
  \break
  gf df bf df df f \time 3/8 f
  
  \time 6/8
  gf, df' bf df gf, f bf r4.
  
  
  \bar "||"
  \break
  \key fs \minor
  fs~ fs cs'~ cs d~ d cs~ cs
  d~ d cs~ cs
  b~ b cs~ cs
  \break
  a~ a e~ e d~ d cs~ cs
  a ~ a e'~ e d~ d fs~ fs
  
  \break
  \key bf \minor
  df' df df df ef ef ef~ ef
  r r r r r r r r
  
  \break
  df,~ df4 c8 bf4.~ bf
  bf4 c8 df4 c8 bf4.~ bf
  
  \break
  gf'' f gf bf, b ef \time 3/8 af,
  
  \time 6/8
  \break
  df bf ef gf f df f c
  df bf ef gf gf df df f 
  \break
  gf df 
  bf df df f \time 3/8 f
  \time 6/8
  \break
  df bf ef gf f df \time 3/8 f,
  \time 6/8
  \break
  gf~ gf bf~ bf gf f bf r
  
  gf,^"(ralentir)" f bf r \bar "|."  
}


\book {
  \bookOutputName "indigo_all"
  \score {
    <<
    \new ChordNames { 
         \transpose c c { \chordNames }
    }  
    \new Staff  \with {
      instrumentName = #"Clarinet (C)"
    }{ 
      \global
      \transpose c c { 
        \clarinet
      }
    }  
    \new Staff  \with {
      instrumentName = #"Bass (C)"
    }{ 
      \global
        \bass
    }
    >>
  \layout { }
  }	
}

\book {
  \bookOutputName "indigo_bass"
  \score {
    <<
    \new ChordNames { 
         \transpose c c { \chordNames }
    }  
    \new Staff  \with {
      instrumentName = #"Bass (C)"
    }{ 
      \global
        \bass
    }
    >>
  \layout { }
  }	
}

\book {
  \bookOutputName "indigo_C"
  \score {
    <<
    \new ChordNames { 
         \transpose c c { \chordNames }
    }  
    \new Staff  \with {
      instrumentName = #"Guitar (C)"
    }{ 
      \global
      \transpose c c { 
        \clarinet
      }
    }  
    >>
  \layout { }
  }	
}

\book {
  \bookOutputName "indigo_Bb"
  \score {
    <<
    \new ChordNames { 
         \transpose c d { \chordNames }
    }  
    \new Staff  \with {
      instrumentName = #"Clarinet (Bb)"
    }{ 
      \global
      \transpose c d { \clarinet }
    }  
    >>
  \layout { }
  }	
}

