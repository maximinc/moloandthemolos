
\version "2.18.2"
\language "english"

\header {
  title = "Panta Rhei (C)"
  composer = "David Orlowsky Trio"
}



global = {
  \set Score.skipBars = ##t
  \time 2/4
}

chordNames={
  \chordmode{
    b2:m  s g fs:m s g cs s b:m fs:m s b:m s e:m fs:m s g a s b:m s
    fs:m s c  fs:m  s s s4 c fs2:m s cs:m s b:m cs:m s cs:m s4 g4 fs2:m s 
    b:m s g fs:m s g cs s b:m fs:m s
    b:m s e:m fs:m s g a s b:m
  }
}

clarinet={
  \tempo 2=106
  \key b \minor
  \relative c''{
    b4\pp cs d b g b fs2~ fs4 fs g fs cs2~ cs4 cs d cs fs2~ fs \break
    b4\p cs d b g b fs2~ fs4 fs g fs cs2 \> cs2 b~  \pp b4^"Fine"
    
    b'  \break \bar "||"
    a\mf fs fs e e fs fs2~ fs4
    b \p 
    a fs fs e fs2~ fs4 fs  \mf 
    \break 
    e cs cs b b cs cs2~ cs4 fs e cs cs b cs2~ cs \bar "||"
    \break
    b'4 cs d b g b fs2~ fs4 fs\p g fs cs2~ cs4 cs d cs fs2~ fs \break
    b4 cs d b g b fs2~ fs4 fs g fs cs2 cs \>
    \break
    \repeat volta 2{
      b2~ \pp b^"Open for solo, after solo D.C. al Fine"
    }
  }
}


\book{
  \bookOutputName "pantha_rhei_Bb"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
  
      \new Staff \with {instrumentName = #"Clarinet (Bb)"}{
        \global
        \clarinet
      }
    >>
    \layout {}
  }
}

\book{
  \bookOutputName "pantha_rhei_bass"
  \score {
    <<
      \new ChordNames { 
        \transpose d c { \chordNames }
      }
  
      \new Staff \with {instrumentName = #"Bass (C)"}{
        \global
        \clef "F"
        \transpose d c, { 
           \clarinet
        }
      }
    >>
    \layout {}
  }
}

\book{
  \bookOutputName "pantha_rhei_C"
  \score {
    <<
      \new ChordNames { 
        \transpose d c { \chordNames }
      }
  
      \new Staff \with {instrumentName = #"Clarinet (C)"}{
        \global
        \transpose d c { 
           \clarinet
        }
      }
    >>
    \layout {}
  }
}