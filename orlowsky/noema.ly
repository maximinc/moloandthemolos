
\version "2.18.2"
\language "english"

\header {
  title = "Noema"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 3/4
}






chordNames = {
  \chordmode {
    d2.:m d:m g:m g:m d:m a:m/c s
    d2.:m d:m g:m g:m d:m a:m/c s
    d2.:m d:m g:m g:m d:m a:m/c s
    
    bf d:m bf d:m bf d:m a:7 d:m
    
    d:m a:m d:m a:m d:m a:m s
    d:m a:m d:m a:m d:m bf d:m s
    
    d2.:m d:m g:m g:m d:m a:m s
    d2.:m d:m g:m g:m d:m a:m s
    
    bf d:m ef d:m
    g:m d:m/f a:7/e d:m
    
    d:m a:m d:m a:m d:m a:m s
    d:m a:m d:m a:m d:m bf d:m s
    
    d1:m d:m a:m
    d1:m d:m a:m
    
    d2:m a:m d:m a:m a:m s
    d2:m a:m d:m a:m a:m s
    

    s2.
    
    bf2. d:m ef d:m
    g:m d:m/f a:7/e
    d:m a:m
    
    d:m d:m g:m g:m d:m a:m
    d:m d:m g:m g:m d:m a:m
    d:m d:m g:m g:m d:m a:m
    a:m
    
    bf d:m bf d:m
    bf d:m a:7/cs d:m
  }
}


bass = {
  \global
  \clef "F"
  \key d \minor  
  \relative c{
  r2. r r r r r r  \break
  \bar "||"
  r2. r r r r r r  \break
  d'2. d4 e f g2. g4 f e 
  d2. a2. r2. \break
  
  bf2 bf4 d2 d4 bf2 bf4 d2.
  bf2 bf4 d2 d4 cs2 cs4 d2. \break
  \bar "||"
  
  d2. a2. d2 e8 f e4 d c d2 c4  e2. a,2. \break
  d2 d4 a2. d2 e8 f e4 d c d2 d4  bf2 bf4 d2.~ d2 r4 \break
  
  \bar "||"
  
  d,2 d4 d4 e f g2 g4 g4 f e 
  d2 d4 a' e g a2.  \break
  
  d,2 a4 d2 d4 g2 d4 g2 g4
  d2 d4 a' e g a2. \break
  bf2. d, ef d2 d4
  g2. f   e2 a,4 d2. \break
  \bar "||"
  
  d2 d4 a2 a4 d2 e8 f e4 d c
  d2 d4 e2 e4 a2 a4 \break
  d2 d4 a2 a4 d2 e8 f e4 d c
  d2. bf \break d~ d4 \fermata
  r4 
  
  \new CueVoice {
     \stemUp c8^"Fanny" d
  }
  \bar "||"
  \time 4/4
  d2^\pp a'4 f d2 a'4 f
  a2 e 
  d2 a'4 f d2 a'4 f
  a,1
  d,2 e f e a,1
  d2 e f e a2 \fermata r2 \bar "||" \break
  
  \time 3/4
  \new CueVoice {
    r4^"Tempo++" e8^"Fanny" f g a 
  }
  bf,2. d2 d4 ef2 ef4 d2 d4 \break
  g2 g4 f2 f4 e2 a,4 d4. d8 c bf a2. \break
  
  d4. d8 a4 d4. d8 a'4
  g4. g8 d4 g4. g8 f e 
  d4. d8 c bf a4 a a \break
  
  d4. d8 a4 d4. d8 a'4
  g4. g8 d4 g4. g8 f e
  d4. d8 c bf a4 a a \break

  d4. d8 a4 d4. d8 a'4
  g4. g8 d4 g4. g8 f e \break
  d4. d8 c bf a4^"ralentir" e' g a4 

  \new CueVoice {
    e8^"Fanny" f g a
  }
  \break
  bf2 c4 d2.
  bf2 c4 d2.
  bf2 c4 d2.
  cs2. d2. \fermata \bar "|." 
  }
}

alto = {
  \global
  \key d \minor  
  \relative c''{
  r2. r r r r r \break
  r2 a8^"Fanny" f
  \bar "||"
  e4. d8 a' f  e4. d8 d' bf a4. g8 d' bf a4. g8 f e  \break
  f4. a8 f e c2.~ c4 r a'8 f \break
  e8 f e d a' f e f e d d' bf a bf a g d' bf a bf a\mordent g f e  \break
  f4. a8 f e c2.~ c4 \new CueVoice {e8^"Fanny" f g a}  \break
  
  r8 bf,^"Léo" g d d4 r8 a' f d d4 r8 bf' a bf c f d4 a8 f d4 \break
  %bf4. g8 bf g a4 f d bf'4. g8 bf g d'4 a f \break
  bf''2. (bf8) f e f g f e4. cs8 a4 d2. \break
  %bf''4. g8 bf g f4 a f e a a, d2 r4 \break
  \bar "||"
  a'4 g f e2 c4 c d2 r2.
  <a'f>4^"Léo en bas"  <e g> <d f> <c e>2 c4  a2. \break
  a'4 g f e2 c4 c d2 r2. 
  
  a'4 bf a bf2 f4 d2.~ d2 a8^"LéoLuc" f \break
  \bar "||"
  e4. d8 a' f  e4. d8 d' bf a4. g8 d' bf a4. g8 f e  \break
  f4. a8 f e c8 c' a2~ a2 f4 \break
  e4. d8  a' f e d d'4. bf8 a4. g8 d' bf
  a4. g8 f e \break 
  f4. a8 f e c2 r4  r e8 f g a \break
  
  %d ds e f fs16 g gs a  \break
  bf4. g8 bf g a4 f d bf'4. g8 bf g d'4 a f \break
  bf4. g8 bf g f4 a4. f8 e4 a a d,2 r4 \break
  
  
  \bar "||"
  a''4 g f e2 c4 c d2 r2 \tuplet 3/2 {r8 g gs} \break
  a4 g f e2 c'4  a4  r r \break%b16 c d ds e f fs g gs \break
  a4 g f e2 c4 c8 d a2 r2 d16 e f g  \break 
  a4~ \tuplet 3/2 {a8 g f e d c } bf2 bf8 f
  d2.~ \fermata d4 r4 c'8^"Fanny" d
  \bar "||" \break
  \time 4/4
  f d~ d8 f d c a c f d~ d f d c a c~  c2 r4  c8 d  \break
  f d~ d8 f d c a c f d~ d f d c a c a2 r4 c8^"LéoLuc" d    \break
  f d~ d8 f d c a c f d~ d f d c a c~  c2 r4  c8 d  \break
  f d~ d8 f d c a c f d~ d f d c a c a2 \fermata r2 \break
  \bar "||"
  \time 3/4 
  r4^"Tempo++" \new CueVoice { e8^"Fanny" f g a } bf4. g8 bf g a4 f d bf'4.  g8 bf g d'4 a f  \break 
  bf4. g8 bf g f4^"t++" a f e a a d,2. r4 a'4^\fff f  \break
  e4.\trill d8 a' f e f e d d'4 bf8 a g a bf a \break 
  bf a\mordent g f \tuplet 3/2 {e f g} 
  a4. g8 f e a4 a f  \break
  e4.\trill d8 a' f e4.\trill d8 d' bf bf  a g a bf a \break
  bf a \mordent g f \tuplet 3/2 {e f g}
  a4. g8 f e a4 a f  \break
  e4.\trill d8 a' f e f e d d'4 bf8 a g a bf a \break 
  bf a \mordent g f \tuplet 3/2 {e f g}
  a4. g8 f e a2. \break
  
  
  r4 \new CueVoice { e'8^"Fanny seule" f g a} bf4. g8 bf g a4 f d bf'4.  g8 bf g d'4 a f  \break 
  bf4. g8 bf g f4^"t--" <e a>^"Léo en bas" <d f> <cs e> <e a> <cs a> d,2. \fermata \break
  
  \bar "|."
  }
}


clarinet = {
  \global
  \key d \minor  

  \relative c''{
  r2. r r r r r \break
  r2 a8 f
  \bar "||"
  e4. d8 a' f  e4. d8 d' bf a4. g8 d' bf a4. g8 f e  \break
  f4. a8 f e c2.~ c4 r a'8 f \break
  e8 f e d a' f e f e d d' bf a bf a g d' bf a bf a\mordent g f e  \break
  f4. a8 f e c2.~ c4 e8 f g a  \break
  bf4. g8 bf g a4 f d bf'4. g8 bf g d'4 a f \break
  bf4. g8 bf g f4 a f e a a, d2 r4 \break
  \bar "||"
  a'4 g f e2 c4 c d2 r2.
  a'4 g f e2 c4  a2. \break
  a'4 g f e2 c4 c d2 r2. 
  
  a'4 bf a bf2 f4 d2.~ d2 a'8^"Luc" f \break
  \bar "||"
  e4. d8 a' f  e4. d8 d' bf a4. g8 d' bf a4. g8 f e  \break
  f4. a8 f e c8 c' <a a'>2~ <a a'>2 f4 \break
  e4. d8  a' f e d d'4. bf8 a4. g8 d' bf
  a4. g8 f e \break 
  f4. a8 f e c4. b4 \mordent c8
  
  d ds e f fs16 g gs a  \break
  bf4. g8 bf g a4 f d bf'4. g8 bf g d'4 a f \break
  bf4. g8 bf g f4 a4. f8 e4 a a, d2 r4 \break
  
  
    \bar "||"
  a'4 g f e2 c4 c d2 r2 \tuplet 3/2 {r8 g gs} \break
  a4 g f e2 c'4  a8. b16 c d ds e f fs g gs \break
  a4 g f e2 c4 c8 d a2 r2 d16 e f g  \break 
  a4~ \tuplet 3/2 {a8 g f e d c } bf2 bf8 f
  d2.~ \fermata d4 r4 c8^"Fanny" d
  \bar "||" \break
  \time 4/4
  f d~ d8 f d c a c f d~ d f d c a c~  c2 r4  c8 d  \break
  f d~ d8 f d c a c f d~ d f d c a c a2 r4 c'8^"Luc" d    \break
  f d~ d8 f d c a c f d~ d f d c a c~  c2 r4  c8 d  \break
  f d~ d8 f d c a c f d~ d f d c a c a2 \fermata r2 \break
  \bar "||"
  \time 3/4 
  r4^"Tempo++" e8^"Fanny" f g a bf4. g8 bf g a4 f d bf'4.  g8 bf g d'4 a f  \break 
  bf4. g8 bf g f4^"t++" a f e a a, d2. r4 a''4^\fff f  \break
  e4.\trill d8 a' f e f e d d'4 bf8 a g a bf a \break 
  bf a\mordent g f \tuplet 3/2 {e f g} 
  a4. g8 f e a,4 a' f  \break
  e4.\trill d8 a' f e4.\trill d8 d' bf bf  a g a bf a \break
  bf a \mordent g f \tuplet 3/2 {e f g}
  a4. g8 f e a,4 a' f  \break
  e4.\trill d8 a' f e f e d d'4 bf8 a g a bf a \break 
  bf a \mordent g f \tuplet 3/2 {e f g}
  a4. g8 f e a,2. \break
  
  
  r4 e8 f g a bf4. g8 bf g a4 f d bf'4.  g8 bf g d'4 a f  \break 
  bf4. g8 bf g f4^"t--" a f e a a, d2. \fermata \break
  
  \bar "|."
  }
}

\book{
  \bookOutputName "noema_all"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }  
      \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
        \transpose c c { 
          \clarinet
        }
      }
      \new Staff  \with { instrumentName = #"Alto (C)" }{ 
        \transpose c c' { 
          \alto
        }
      }
      \new Staff \with { instrumentName = #"Bass (C)"}{ 
        \bass 
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "noema_bass"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }  
      \new Staff \with { instrumentName = #"Bass (C)"}{ 
        \bass 
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "noema_C1"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }  
      \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
        \transpose c c { 
          \clarinet
        }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "noema_C2"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }  
      \new Staff  \with {instrumentName = #"Alto (C)"}{ 
        \transpose c c { 
          \key d \minor  
          \alto
        }
      }
    >>
    \layout { }
  }
}


\book{
  \bookOutputName "noema_C2_C_clef"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }  
      \new Staff  \with {instrumentName = #"Alto (C)"}{ 
        \clef C
        \transpose c c { 
          \key d \minor  
          \alto
        }
      }
    >>
    \layout { }
  }
}


\book{
  \bookOutputName "noema_Bb1"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }  
      \new Staff  \with { instrumentName = #"Clarinet (Bb)" }{ 
        \transpose c d { 
          \clarinet
        }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "noema_Bb2"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }  
      \new Staff  \with { instrumentName = #"Clarinet (Bb)" }{ 
        \transpose c d' { 
          \alto
        }
      }
    >>
    \layout { }
  }
}

