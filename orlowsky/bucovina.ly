
\version "2.18.2"
\language "english"

\header {
  title = "Bucovina"
  composer = "David Orlowsky Trio"
}


global = {
  \set Score.skipBars = ##t
  \time 7/8
}

\paper {
  system-system-spacing = #'((basic-distance . 10.1) (padding . 0))
} 



chordTheme={
  \chordmode{
    f2:m s4. s2 s4.
    gf2 s4. f2:m s4.
  }
}
chordThemeMod={
  \chordmode{
    f2:m s4. f2:m s4.
    gf2 s4. f2:m s4.
  }
}

chordBridge={
  \chordmode{
    bf1:m f:m gf   f2:m s4.
    bf1:m f:m gf   f2:m s4.
  }
}

chordBridgeB={
  \chordmode{
    gf2 s4. df2 s4. gf2 s4.  
  }
}

chordThemeC={
  \chordmode{
    gf2 s4. f2:m s4. bf2:m s4. 
      c2 f4.:m
      c2 f4.:m
      
    gf2 s4. df2:6  df4.:maj7 gf2 s4. f2:m s4.
    gf2 s4. df2:6  df4.:maj7 gf2 s4. f2:m s4.
    gf2 s4. df2:6  df4.:maj7 gf2 s4. f4:m s4.
    s4 s4. s4 s4. s4 s4.
  }
}

chordThemeFiveA={
  \chordmode {
    bf4:m s4. df4:m s4.
    gf4 s4. f4.:m
    
    bf4:m s4. df4:m s4.
    c4 s4. f4:m s4.
    gf2 s4. f2:m s4.
  }
}

chordNames = {
  \chordmode {

    f2:m s4. s2 s4. f2:m s4. s2 s4.
    f2:m s4. s2 s4. f2:m s4. s2 s4.
    f2:m s4. s2 s4. f2:m s4. s2 s4.
    df2 s4. s2 s4.
    
    f2:m s4. s2 s4.
    gf2  s4. s2  s4.

    f2:m s4. s2 s4.
    gf2  s4. s2  s4.
    
    \chordTheme
    \chordTheme
    \chordTheme
    
    
    f2:m s4. s2 s4.
    
    \chordBridge
  
    \chordTheme
    \chordTheme

    \chordThemeMod
    f2:m s4. f2:m s4.

    \chordBridge

    \chordThemeC
    
    \chordThemeFiveA
    \chordThemeFiveA
    \chordThemeFiveA
    \chordThemeFiveA
    \chordThemeFiveA
    
    df1 s1 ef1 s
    f:m ef c:m s
    df1 bf:m af s
    
    bf:m ef c:m s
    f:m c:m f:m s
    
    af f:m g:m s
    df bf:m c:m s
    
    af f:m g:m s
    f:m c:m f:m6- s
    
    f1:m6- s1 c:13- s1 bf s1 d:m s1
    
    df s1 c:7.9- s1
    bf s1 d:m s1
    df s1 c:7.9- s1
    bf s1 d:m s1

    df1 s1 ef1 s
    f:m ef c:m s
    df1 bf:m af s
    
    bf:m ef c:m s
    
   
    
    af f:m g:m s
    df bf:m c:m s
    
    af f:m g:m s
    f:m c:m f:m9 s
    
    f4.:m s2 s4. s2 f4.:aug s2 s4. s2
    f4.:m s2 s4. s2 f4.:4 s2 s4. s2

  \chordTheme
  \chordTheme
  \chordTheme
  \chordTheme
  \chordTheme
  f4.:m s4 bf4.:m s4. s4.
  gf1 f:m
  }
}

riffFa ={
  g'8 af f c f c f 
  g8 af f c4 c4
  df2~ df4.~
  df2~ df4.
}

riffFb ={
  g'8 af f c f c f 
  g8 af f c8 f c8  df16 c
  bf2~ bf4.~
  bf2~ bf4. 
}


riffTaa={
  \set Timing.beatStructure = #'(3 2 2)
  g8 af f c f c4 
  g'8 af f c4 f4
  c'8 df bf gf4 gf
  af8 g f f2  
}
riffTab={
  g8 af f c f c4 
  g'8 af f c4 f
  c'8 df bf gf4 bf8 af
  g af g f f4.  
}
riffTa ={
  
  \riffTaa
  \break
  \riffTab
  \break
}



themefirst = {
  c8^"Db5" df c df f e\mordent df e df c bf c bf c df 
  
  \time 3/8
  c^"C5" af f \break
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  c'8^"Db5" df c df f e\mordent df e df c e f e f g 
  
  af^"C5" g f c af \break
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  gf^"Db5" af gf af bf af gf 
}

themef = {
  c8 df c df f e\mordent df e df c bf c bf c df 
  
  \time 3/8
  c af f \break
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  c'8 df c df f e\mordent df e df c e f e f g 
  
  af g f c af \break
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  gf af gf af bf af gf 
}



themeS={

  df2. ef4 f2. af4 g1 r1 \break
  
  c2.~ c16 df bf af g2. af4 g1 r1 \break
  
  f2.~ f16 ef df c df2. c16 bf af bf
  c1 r1 \break
  
  df2~ \tuplet 3/2 {df4 bf af} g2~g8. f16 af8. f16
  g1 r1 \break
}


clarinet={
  \set Timing.beatStructure = #'(3 2 2)
  \relative c''{
    \new CueVoice {
      <c f c'>4. <c f c'>4 <c f c'>4
    }
  }

  r4. r2 r4.  r2 r4. r2 \break
  \bar "||"  
  \relative c''{
    \riffFa
  }
  r4. r2 
  r4. r2 
  \break
  \relative c''{
    \riffFb 
  }
  \break
  r4. r2
  r4. r2 
  
  r4. r2 r4.  r2 r4.  r2 r4. r2
  r4. r2 r4.  r2
  %\bar "||" 
  \break
  
  \relative c'''{
    \repeat volta 2 {
      \riffTa
    }
  }
  \relative c'''{
    g8 af f c4 c8 g'8~
    g8 af f c4  f
    c'8 df bf gf4 bf
    af8 g f f2  
    g8 af f c4 c4
    g'8 af f c4 f \break
    
    \time 4/4
    df'8 c df ef f2
    f8 e f g af2	
    c8 df bf gf4 bf8 af gf 
    \time 7/8
    \set Timing.beatStructure = #'(3 2 2)
    f4 af8 g4 f  \break
    
    \time 4/4
    df8 c df ef f2~
    f8 e f g af2	
    gf8 af bf gf4 bf8 af gf 
    \time 7/8
    \set Timing.beatStructure = #'(3 2 2)
    f4.~ f2
    \break
  }
  \relative c'''{
    \riffTa
  }
  \bar "||"
  \relative c'''{
    g8 af f c4 c4
    g'8 af f c4  f
    c'8 df bf gf4 gf
    af8 g f f2  
    g8 af f c4 c4
    g'8 af f c4 f \break
    
    \time 4/4
    df'8 c df ef f2
    f8 e f g af2	
    c8 df bf gf4 bf8 af gf 
    \time 7/8
    \set Timing.beatStructure = #'(3 2 2)
    f4 af8 g4 f  \break
    
    \time 4/4
    df8 c df ef f2~
    f8 e f g af2	
    gf8 af bf gf4 bf8 af gf 
    \time 7/8
    \set Timing.beatStructure = #'(3 2 2)
    f4.~ f2
  }
  \break
  
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  \relative c'''{
    \repeat volta 2{
      df8^"1 3 5." bf df bf df c bf 
      af f af f af g f 
      df'8 bf df bf  df bf f'
    }
    \alternative{
      {e\mordent c e g af g f }
      {e\mordent c e g af4. }
    }
    
    \break
    <<
    {bf8 gf bf gf bf af gf
    f8   df f af g f df
    bf'8 gf bf gf bf df bf
    c2~ c4.}
    \new CueVoice{
      \relative c''{
        <df gf bf>4 s s4.
        <df f bf>4 s <c ef af>4.
      }
    }
    >>
    \break
    bf8 gf bf gf bf af gf
    f   df f af g f df
    bf'8 gf bf gf bf df bf
    c4  f4~ f4.
    \break
  
    bf,8 gf bf gf bf af gf
    f   df f af g f df
    bf'8 gf bf gf bf af gf
  }
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  
  \relative c'''{
    <<
    {f4~ f4. r4 r4. r4 r4. r4 r 4. }
    \\
    \new CueVoice {
      \stemDown f,8_"Maximin" c f c af 
            f'8 c f c af 
            f'8 c f c af 
            f'8 c f c af 
     }  
    >>
  }
  \bar "||"\break
  
  
  
  %{
  c8 df c df f e\mordent df e\mordent df c bf c bf c df 
  
  \time 3/8
  c af f \break
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  c'8 df c df f e\mordent df e\mordent df c e f e f g 
  
  af g f c af 
  \time 7/8
  \set Timing.beatStructure = #'(2 2 3)
  gf af gf af bf af gf 
  %}
  \relative c'''{
    \themefirst
    f2^"C5"  r4.
  }
  
  \break

  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  \relative c'''{
    \themef
    f4 af8 g4 r \break
  }
  
  
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  \relative c''{
    \themef
    f2 r4. \break
  }
  
  
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  \relative c''{
    \themef
    f4 af8 g4 r \break
  }
  
  
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  \relative c''{
    \themef
    f'4 af8 c4 f \break
  }
  
  \bar "||"
  \time 4/4
  
  \relative c'''{
    \themeS
  }
  
  \relative c''{
    f2.~ f16 c f af g2.~ g16 c, af' g
    f1 r1 \break
    
    c'2.~ c16 af c ef f2~ \tuplet 3/2 {f4 g ef }
    d1 r1 \break
    
    f2.  ef4 \tuplet 5/4 {df4 c4 df4 c c } \tuplet 3/2 {bf af g~ } g2 r1 \break
  
    c2.~ c16 af c ef f2~ \tuplet 3/2 {f4 g ef }
    d1 r1 \break
  
    f,2.~ f16 c f af g2.~ g16 c, af' g
    
    <<
      \new CueVoice { \stemUp
        f'8 df' af df g, df' af df }
      \\
      {f,,1}
    >>
    \new CueVoice {f'8 df' af df g, df' af df }
    \break
  }
  \pageBreak
  \relative c'''{
    \new CueVoice {
      f8 df' af df g, df' af df
      f,,8 df' af df g, df' af df 
      c, e g e af e g e 
      c e g e af e g e 
      d f' f, f'  bf, f' bf, f
      d f' f, f'  bf, f' bf, f
      d e' f, e' a, e' f, e' d, d' f, d' <d, f bf>2
    }
  }
  \break

  \relative c''{
    af8 f af bf c bf af f af bf c bf af f af bf c bf af f af bf c bf 
    a f a bf c bf a f \break
    a bf c bf a f a bf c bf a f a bf
    d bf a f a bf d bf a f a bf c bf a f a bf 
    \break
    c bf af f af bf c bf af f af bf c bf af f af bf c bf a f a bf c bf a f a bf 
    d bf \break
    a f a bf d bf a f a bf d bf a f a bf c bf a f a bf c bf a4 df'4~ df2~ \break
  }
  \relative c'''{
    \themeS
  }

  \relative c'''{
    c2.~ c16 af c ef f2~ \tuplet 3/2 {f4 g ef }
    d1 r1 \break
  
    f2.~ f16 ef df c df2~ df8. c16 bf af g f
    g1 r1 \break
  

    c2.~ c16 af c ef f4 f \tuplet 3/2 {f4 g ef }
    d1 r1 \break
    
    f,2. r8 c f af g4 c, af' g f~ f2 r1 \break
  }
  
  \time 7/8
  \set Timing.beatStructure = #'(3 2 2)
  r4. r2 r4. r2 r4. r2 r4. r2 
  r4. r2 r4. r2 r4. r2 r4. r2
  \break
  
  \relative c'''{
    \riffTa
  
    g8 af f c f c4 
    g'8 af f c4 f4
    c'8 df bf gf4 af~
    af8 g f f2   \break
  
    g8 af f c f c4 
    g'8 af f c4 f
    c'8 df bf gf4 bf8 af
    g af g f f4.   \break
  
    g8^\fff af f c f c f 
    g8 af f c4 f
    c'8 df bf gf4 gf af8 g f f2 \break
    
    \time 5/8
    g8 af f c4 
    \time 9/8
    df'8 c df f e f af g af 
    \time 4/4
    c df  bf gf4 bf8 af gf f4 r2.
  }
  \bar "|."
}

 
bassA=\relative c{
  f4. g4 af bf4. c4 bf 
  gf4.~ gf2~ gf4.~ gf2
}

bassB=\relative c{
  f4. g4 af
  bf4. c4 bf 
  gf4. bf4 c
  f,4. af4 g 
}

bassW = \relative c'{
  bf4 c df ef
  f ef df c 
  gf4. bf4 c4.
  \time 7/8
  \set Timing.beatStructure = #'(3 2 2)
  f,4. c'4 bf
  \break
  \time 4/4
  bf4 c df ef
  f ef df c 
  gf4. bf4 c4.
  \time 7/8
  f,4. af4 g
}
bassWb = \relative c'{
  bf4 c df ef
  f ef df c 
  gf4. bf4 c4.
  \time 7/8
  \set Timing.beatStructure = #'(3 2 2)
  f,4. df'4 c
  \break
  \time 4/4
  bf4 c df ef
  f ef df c 
  gf4. bf4 c4.
  \time 7/8
  f,4. f4 f
}


bassF = \relative c{
  bf2 c8 df2 f8 gf2~ gf8
  \time 3/8
  f4.
  \time 5/8 \break
  bf,2 c8 df2~ df8
  c2 g'8 f2~ f8
  
  \time 7/8
  gf2 af4 gf8
  
}

bass = \relative c{
  \set Timing.beatStructure = #'(3 2 2)
  r4. r2 r4.  r2 r4.  r2 r4. r2 \bar "||"  
  r4. r2 r4.  r2 r4.  r2 r4. r2
  r4. r2 r4. r2 
  \break
  r4.  r2 r4. r2 
  r4.  r2 r4. r2  \break
  
  \bassA   
  \bassA 
  \break
  
  \repeat volta 2{
    \bassB
    \bassB
  }
  \break
  \bassB
 
  f4. g4 af
  bf4. c4 bf \break
  
  \time 4/4
  \bassW \break
  
  \bassB \break
  \bassB \break
  \bassB
  
  f4. g4 af
  bf4. df4 c \break
  
  \time 4/4
  \bassWb
  \break
  
  \repeat volta 2{
    gf2 bf4.   c2 f,4.
  
    df'2  bf4.
    
  }
  \alternative {
    {c2 f, 4.}
    {c'2 f, 4.}
  }
  \break
  
  
  gf2 bf4. df2 af4.
  gf2 bf4. c4 c8 c4 c \break
  
  gf2 bf4. df2 af4.
  gf2 bf4 f8  f4 f8 f4 f \break

  gf2 bf4. df2 af4.
  gf2 bf4. 
  
  \time 5/8
  \set Timing.beatStructure = #'(2 3)
  c4~ c4.


  r4 r4.
  r4 r4.
  r4 r4. \break

  \bar "||"
  
  df4~ df4.~ df4~ df4.~ df4~ df4.
  \time 3/8 
  c4.
  \break
  \time 5/8
  df4~ df4.~ df4~ df4.~ df4~ df4. c4~ c4.
  
  
  \time 7/8
  df2~ df4. 
  
  c2~ c4. 
  \break
 
  \time 5/8
  \transpose c c' {
    \bassF
  }
  
  f4 f8 f4 f \break
  
  \time 5/8
  \transpose c c' {
    \bassF
  }  
  f4 f8 f4 f \break
  
  \time 5/8
  \bassF
  f,4 f8 f4 f
  \break
  
  \time 5/8
  bf,4 bf4. df4 df4.
  gf4 gf4.
  \time 3/8 f4.\break
  \time 5/8
  bf,4 bf4. df4 df4.
  c4 c4. f4 f4. 
  
  \time 7/8
  gf4 gf4 gf4. 
  f4. f4 f4
  \break
  
  \time 4/4
  df'4 af df4 af df4 af df4 af
  ef' bf ef bf ef bf ef bf \break
  f' c f c ef bf ef bf c g c g c g c g
  
  \break
  df' af df af bf f' bf, f' af, ef' af, ef' af, ef' af, ef'
  \break
  
  bf f' bf, f' ef bf ef bf c g c g c df ef e
  \break
  f c ef df c af df ef 
  f c f c f c c bf
  
  \break
  af ef' af, ef' f c f c g' d g d g d g d
  
  \break
  df af' df, c bf f' bf, b 
  c g' c, g' c, g' c, g'
  
  \break
  af, ef' af, ef' f c f c 
  g' d g f ef d ef e 
  \break
  f c ef df c af df ef 
  
  f1~ f4 f, bf c
  df2. c4 bf c df bf
  \break
  c2 g~ g4 e g a
  bf2. d4 c bf f ef
  d2 a'2 r4 f bf r
  
  \bar "||"
  \break
  df, df df df
  bf df df bf
  c c g2  g4 f a bf bf c c d c bf f' ef 
  d d  a a a a  bf8 c df4 \break
  df df df df  df df  df df 
  c c c c c c c c
  bf bf bf bf bf bf bf bf 
  d d d d d d d df'~
 
 \break
  df4 af df4 af df4 af df4 af
  ef' bf ef bf ef bf ef bf
 
  f' c f c ef bf ef bf c g c g c g c g \break
  df' af df af bf f' bf, f' af, ef' af, ef' af, ef' af, ef'
  
  bf f' bf, f' ef bf ef bf c g c df c bf c g \break
  af ef' af, ef' f c f c g' d g d g d g d
  
  df af' df, c bf f' bf, b 
  c g' c, g' c, g' c, g' \break

  
  af ef af g f c f fs
  g d g d g d g d 
  f c ef df c g df' ef 
  
  f2 f2 f2 f2 
  \bar "||"
  \time 7/8
  \break
  f2 f4.~ f8 f2 f4~ f4 f2 f8~ f4. f2 \break
  f2 f4.~ f8 f2 f4~ f4 f2 f8~ f4. f2 \break
  
  f4. f4 f f4. f4 f gf4. gf4 gf f4. f4 f \break
  f4. f4 f f4. f4 f gf4. gf4 gf f4. f4 f \break
  f4. f4 f f4. f4 f gf4. gf4 gf f4. f4 f \break
  f4. f4 f f4. f4 f gf4. gf4 gf f4. f4 f \break
  f4. f4 f f4. f4 f gf4. gf4 gf f4. f4 f \break
  
  \time 5/8
  
  f,4. f4
  \time 9/8
  bf4. df4. f4. 
  \time 4/4
  gf4. gf4. gf4 f4 r2.
  \bar "|."
}



\book{
  \bookOutputName "bucovina_all"
  \score {
    <<
      \new ChordNames { 
        \transpose c c{
           \chordNames 
        }
      }
      
      \new Staff  \with {
        instrumentName = #"Clarinet (C)"
      }{ 
        \global        
        \transpose c c,{ 
          \key f \minor  
          \clarinet
        }
      }
  
      \new Staff \with {
        instrumentName = #"Bass (C)"
      }{ 
        \global
        \clef "F"
        \transpose c c {
          \key f \minor  
          \bass 
        }
      }
    >>
    \layout {}
  }
}

\book{
  \bookOutputName "bucovina_bass"

  \score {
    <<
      \new ChordNames {         
           \chordNames 
      }  
      \new Staff \with {
        instrumentName = #"Bass (C)"
      }{ 
        \global
        \clef "F"
        \key f \minor  
        \bass 
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "bucovina_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose bf c {  \chordNames }
      }
 
      \new Staff  \with {
        instrumentName = #"Clarinet (Bb)"
      }{ 
        \global
        \transpose bf c { 
          \key f \minor  
          \clarinet
        }      
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "bucovina_C"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
      \new Staff  \with {
        instrumentName = #"Guitar (C)"
      }{ 
        \global      
        \transpose c c, { 
          \key f \minor  
          \clarinet
        }
      }    
    >>
  %\layout { #(layout-set-staff-size 16)}
  \layout { }
  }
}	

\book{
  \bookOutputName "bucovina_bass_Bb"

  \score {
    <<
      \new ChordNames {         
        \transpose c d{
          \chordNames 
        }
      }  
      \new Staff \with {
        instrumentName = #"Bass (Bb)"
      }{ 
        \global
        \transpose c d'{
          \key f \minor  
          \bass 
        }
      }
    >>
    \layout { }
  }
}
