
\version "2.18.2"
\language "english"

\header {
  title = "Mazeltov"
  composer = "Trad / David Orlowsky Trio"
}



chordNames = {
  \chordmode {
    d1:m s s s s s s
    g2:m a:7 d1:m s
    
    d1:m s s s s s
    g2:m a:7 d1:m
    
    d1:m s s s s s
    g2:m a:7 d1:m
    
    g1:m d:m  a:7 a2:7 d:m
    g1:m d:m  a:7 a2:7 d:m

    d1:m s s s s s
    g2:m a:7 d1:m

    g1:m d:m  a:7 a2:7 d:m
    g1:m d:m  a:7 a2:7 d:m
    
    f2 a:7/e d1:m
    bf2 d:7/a g1:m
    a:7 d:m a:7 a2:7 d:m

    f2 a:7/e d1:m
    bf2 d:7/a g1:m
    a:7 d:m a:7 a2:7 d:m

    d1:m d:m d2:m d:7
    g1:m a:7 d:m a:7 a2:7 d:m

    d1:m d:m d2:m d:7
    g1:m a:7 d:m a:7 a2:7 d:m

    d1:m s s s s s
    g2:m a:7 d1:m

    g1:m d:m  a:7 a2:7 d:m
    g1:m d:m  a:7 a2:7 d:m

    d1:m s s s s s
    g2:m a:7
    g2:m a:7 
    g2:m a:7 
    d1:m
  }
}



sacha={
  \key d \minor

  \relative c''{
    s4 s4 s8 a8^"Xim" d f <d f a>4 <d f a>4 <cs e gs>4 <d f a>4
    <c f>8 e
    <d, a' d f>4
    r8 a'8 d f <d f a>4 <d f a>4 <cs e gs>4 <d f a>4
    <d g b>8 c
    <d f a>4 r8 a' b cs <f, a d>4 <d f a>4 <cs e gs>4 <d f a>4
    <c f>8 e
    <d f> a f d' e f g4. f8 \tuplet 3/2 { a g f} \tuplet 3/2 {f e d} d4 r4 r2 
  }

  \relative c'{
    r4 r r8 
    f^"Sacha (le chat)" a d f4 f e f f8 e d4 r8 f, a d f4 f e f g f4
    r8 cs d e f4 f e f f8 e d4 r r g,2~ g8 a b cs d4
    
    r4 r8
    f, a d
    \break
    f4 f e f f8 e d4 r8 f, a d f4 f e f g4 f4
    r8 cs d e f4 f e f f8 e d4 r r g,2~ g8 a b cs d4
    r r r
  }
  \break
  \bar "||"
  \relative c'{
    r8 d g a bf4 g f8 a, d e f8 e d4  cs4 cs cs cs cs a d4 r
    r8 d g a bf bf a g f a, d e f e e d cs4 cs cs cs cs a d8
  }
  \relative c'{
    f a d 
    \break
    f4 f e f f8 e d4 r8 f, a d f4 f e f g f4
    r8 cs d e f4 f e f f8 e d4 r r g,2~ g8 a b cs d4 r r r
  }
  \break
  \relative c'{
    r8 d g a bf4 r 
    r8 a d e f4 r
    cs,4 cs cs cs cs8 bf a g f4 r4
    g'4 r8 bf~ bf4 r
    f4 r8 a~ a4 r
    cs,4 cs cs cs cs8 bf a g f4 r4
  }
  \break
  \bar "||"
  \relative c'{
    f2 d4. cs8 cs4 d~ d r
    a'2 f4. e8 e4 d~ d r
    g4 cs, g'4. cs,8 cs4 d4 r8 d e f g4 g g g g8 f e cs d4 r
  }
  \break
  \relative c'{
    f2 d4. cs8 cs4 d~ d r
    a'2 f4. e8 e4 d~ d r
    g4 cs, g'4. cs,8 cs4 d4 r8 d e f g4 g g g g8 f e cs d4 r
  }
  \break
  \bar "||"
  \relative c''{
    f2 d4. c8 c4 d4 r r
    f2 d4. c8 c4 bf4 r8 bf c d
    cs4 g cs4. e8 e4 d4  r2 e4 e e cs a g f4 r
  }
  \break
  \relative c''{
    f2 d4. c8 c4 d4 r r
    f2 d4. c8 c4 bf4 r8 bf c d
    cs4 g cs4. e8 e4 d4  r2 e4 e e cs a g f8 f a d 
  }
  \break
  \bar "||"
  \relative c''{

    f4 f e f f8 e d4 r8 f, a d f4 f e f g f4
    r8 cs d e f4 f e f f8 e d4 r r g,2~ g8 a b cs d4 r r r
    \break
    r1 r2 r8 g, a bf cs4 cs cs cs cs8 bf a g f4 r 
    r1 r2 r8 g a bf cs4 cs cs cs cs8 bf a g f8 f a d
  }
  \break
  \relative c''{
    f4 f e f f8 e d4 r8 f, a d f4 f e f g f4
    r8 cs d e f4 f e f f8 e d4 r r
    \break
    r8 bf r bf cs4 cs 
    r8 bf r bf cs4 cs
    r8 bf r bf cs4 cs
    a a8 a~ a a a4
    bf bf8 bf~ bf bf bf4
    c c8 c~ c c c4
    r a d, r
    
  }
  \bar "|."
}

clarinet={
  \key d \minor
  \tempo 4 = 180
  \relative c''{
    s4 s4 s8 a8^"Ximxim" d f <d f a>4 <d f a>4 <cs e gs>4 <d f a>4
    <c f>8 e
    <d, a' d f>4
    r8 a'8 d f <d f a>4 <d f a>4 <cs e gs>4 <d f a>4
    <d g b>8 c
    <d f a>4 r8 a' b cs <f, a d>4 <d f a>4 <cs e gs>4 <d f a>4
    <c f>8 e
    <d f> a f d' e f g4. f8 \tuplet 3/2 { a g f} \tuplet 3/2 {f e d} d4 r4 r2 
  }
  
  \relative c''{
    r4 r4 r8 a^"Fan" d f 
    a4 a gs a f8 e d4 d8 a d f a4 a gs a b8 c a4 r8 a b cs d4 a gs a
    f8 e d4 r8 d8 e f g4 bf a8\mordent g f e 
    d4 r4 r8 a8 d f
    \break
    a4 a gs a f8 e d4 d8 a d f a4 a gs a b8 c a4 r8 a b cs d4 a gs a
    f8 e d4 r8 d8 e f g4 bf a8\mordent g f e 

    d4 r4 r8 d8\mordent cs d
  }
  \bar "||"
  \break
  \relative c''{
    bf4 r4 r8 d8\mordent cs d
    a4 r4 r8 e f g a4 a a a a8 g f e d8 d'\mordent cs d 
    bf8 d bf d bf d\mordent cs d 
    a d a d a e f g a4 a a a a8 g f e d a' d f
  }
  \break
  \bar "||"
  \relative c'''{
      a4 a gs a f8 e d4 r8 a d f a4 a gs a b8 c a4 r8 a b cs 
      d4 a gs a
      f8 e f e d8 d8 e f g4 bf a8\mordent g f e 
  }
  \relative c''{
    d4 r r8  d8\mordent cs d
    \break
    
    bf4 r4 r8 d8\mordent cs d
    a4 r4 r8 e f g a4 a a a a8 g f e d8 d'\mordent cs d 
    bf4 r8 d8~d4 r a4. d8~d4 r4
    a4 a a a a8 g f e d4 r  
  }
  \break
  
  \bar "||"
  \relative c'{
    c2 a4. g8 g4 f4~f4 r
    f'2 d4. c8 c4 bf4~bf4 r
    cs4 g cs4. bf8 bf4 a~a4 r
    cs4 cs cs cs cs8 bf a g f4  r
  }
  \break
  \relative c'{
    c2 a4. g8 g4 f4~f4 r
    f'2 d4. c8 c4 bf4~bf4 r
    cs4 g cs4. bf8 bf4 a~a4 r
    cs4 cs cs cs cs8 bf a g f8 a' d f
  }
  \bar "||"
  \break
  \relative c'''{
    a2 f4. e8 e4 d4 r8 d8 f a
    d2 bf4. a8 a4 g4 r8 g a bf a4 e a4. g8 g4 f4 r8 
    f e d a'4 a a a a8 g f e d8 a d f
    \break
    a2 f4. e8 e4 d8 d f d f a 
    d2 bf4. a8 a4 g r8 g a bf a4 e a4. g8 g f e f d e f g 
    a gs a gs a4 a a8 g f e d a d f
    \break
  }
  \bar "||"
  \relative c'''{
    a4 a gs a f8 e d4 r8 a d f a8\mordent gs a\mordent gs a4 bf b8 c a4 r8 a b cs d4 a gs a
    f8 e d4 r8 d8 e f g4 bf a8\mordent g f e d4 r4 r8 d8\mordent cs d
  }
  \break
  \relative c''{
    bf8 d bf d bf d8\mordent cs d
    a d a d a e f g 
    a4 a a a a8 g f e d8 d'8\mordent cs d
    bf8 d bf d bf d8\mordent cs d
    a d a d a e f g 
    
    a4 a a a a8 g f e d8 a' d f 
  } 
  \break
  \relative c'''{
    a4 a gs a f8 e d4 r8 a d f a8 \mordent gs a\mordent gs a4 a b8 c a4 r8 a b cs d4 a gs a
    f8 e d4 r8 d8 e f 
    
    g2 g8 d e f g2 r8 d e f g4. bf8 a bf c cs d1~ d1~ d1 
    r4 a4 d r
  }
  \bar "|."
}


bass={
  \key d \minor
  \clef F
  \relative c{
    d1~ d~ d~ d~ d~ d~ d g2 a
  }
  \relative c{
    d4^"a tempo" a' d, a' d, a' d, a'
    \break
    d, a' d, a' d, a' d, a'
    d, a' d, a' d, a' d, a'
    d, a' d, a' d, a' d, a'
    g  g a8 g f e  
    d4 a' d, a'
    \break
    
    d, a' d, a' d, a' d, a' 
    d, a' d, a' d, a' d, a'
    d, a' d, a' d, a' d, a' 
    g  g a8 g f e  
    d4 a' d, a'
    \break
  }
  \bar "||"
  \relative c'{
    g d g d d a' d, a' a e a e a e d a' 
    g d g d d a' d, a' a e a e a e d a
    \break
  }
  \bar "||"
  \relative c{
    d a d a d a d a d a d a d a d a d a d a d a d a
    g' g a a 
    d a d a
    \break
    g4. g g4 d4. d d4 a' e g gs a a,
    d d
    g4. g g4 d4. d d4 a' e g gs a a,
    d8 a d f
  }
  \break
  \bar "||"
  \relative c'{
    a2 f4. e8 e4 d r8 d f a
    d2 bf4. a8 a4 g4 r8 g8 a bf 
    a4 e a4. g8 g4 f r8 f e d a'4 a a a 
    a8 g f e d8 a d f
    \break

    a2 f4. e8 e4 d r8 d f a
    d2 bf4. a8 a4 g4 r8 g8 a bf 
    a4 e a4. g8 g4 f r8 f e d a'4 a a a 
    a8 g f e d4 a'
  }
  \break
  \bar "||"
  \relative c{
    d4 a' d,4 a' d,4 a' d,4 a' d,4 a' d,4 a'
    g d g d a' e a e d a' d, a' a e a e a e d a'
    \break
    d,4 a' d,4 a' d,4 a' d,4 a' d,4 a' d,4 a'
    g d g d a' e a e d a' d, a' a e a e a e d a' 
  }
  \break
  \bar "||"
  \relative c{
    d4 a' d, a' d, a' d, a' d, a' d, a' g a f a
    d, a' d, a' d, a' d, a' g  g  a  a  d, a' d, a'
    \break
    r8 g r bf a g f d r d r e f d c bf a4 e' g gs a a, d d
    r8 g r bf a g f d r d r e f d c bf a4 e' g gs a a, d d
  }
  \break
  \relative c{
    d a' d, a' d, a' d, a' d, a' d, a' d, a' d, a' d, a' d, a' d, a' d, a'
    \break
    r8 g r g a4 a
    r8 g r g a4 a 
    r8 g r g a4 a
    d, a'8 d,~ d a' d,4
    d bf'8 d,~ d bf' d,4
    d c'8 d,~ d c' d,4
    d' a d, r
  }
  \bar "|."
}

\paper {
  system-system-spacing =
    #'((basic-distance . 10.1)
       (minimum-distance . 6)
       (padding . 0)
       (stretchability . 60))
  
} 

staffF_C={
  \new Staff  \with { instrumentName = #"Fan" }{ 
    \set Staff.midiInstrument = "clarinet"
    \transpose c c{ 
      \clarinet
    }
  }
}

staffF_Bb={
  \new Staff  \with { instrumentName = #"Fan" }{ 
    \set Staff.midiInstrument = "clarinet"
    \transpose c d{ 
      \clarinet
    }
  }
}
staff_chords_C={
  \new ChordNames { 
    \transpose c c { \chordNames }
  }
}

staff_chords_Bb={
  \new ChordNames { 
    \transpose c d { \chordNames }
  }
}

staffS_C={
  \new Staff \with { instrumentName = #"Sacha le chat" }{
    \set Staff.midiInstrument = "clarinet"
    \transpose c c{
      \sacha
    }
  }
}

staffS_Bb={
  \new Staff \with { instrumentName = #"Sacha le chat" }{
    \set Staff.midiInstrument = "clarinet"
    \transpose c d{
      \sacha
    }
  }
}

staffE_C={
  \new Staff  \with { instrumentName = #"Éléoswin" }{
    \set Staff.midiInstrument = "acoustic bass"
    \transpose c c { 
      \bass
    }
  }
}

staffE_Bb={
  \new Staff  \with { instrumentName = #"Éléoswin" }{
    \set Staff.midiInstrument = "acoustic bass"
    \transpose c d { 
      \bass
    }
  }
}

\book{
  \bookOutputName "mazeltov_all"
  \score {
    << 
      \staff_chords_C
      \staffF_C
      \staffS_C
      \staffE_C
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
    \midi { }
  }
}

\book{
  \bookOutputName "mazeltov_cla_1_C"
  \score {
    << 
      \staff_chords_C
      \staffF_C
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
  }
}

\book{
  \bookOutputName "mazeltov_cla_1_Bb"
  \score {
    << 
      \staff_chords_Bb
      \staffF_Bb
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
  }
}

\book{
  \bookOutputName "mazeltov_cla_2_C"
  \score {
    << 
      \staff_chords_C
      \staffS_C
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
  }
}

\book{
  \bookOutputName "mazeltov_cla_2_Bb"
  \score {
    << 
      \staff_chords_Bb
      \staffS_Bb
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
  }
}


\book{
  \bookOutputName "mazeltov_bass_C"
  \score {
    << 
      \staff_chords_C
      \staffE_C
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
  }
}

\book{
  \bookOutputName "mazeltov_bass_Bb"
  \score {
    << 
      \staff_chords_Bb
      \staffE_Bb
    >>
    \layout { 
      #(layout-set-staff-size 15)
    }
  }
}