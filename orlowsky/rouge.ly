
\version "2.18.2"
\language "english"


\header {
  title = "Rouge"
  composer = "David Orlowsky Trio"
}


chorda = \chordmode{ 
  c2 :m b:dim7 c1:m f:m c:m
  d2:m7.5-  f:m c1:m    af g:m
  f:m c:m d2:m7.5- g:7 c1:m
}
chordb = \chordmode {
  f1:m c:m d ef
  f:m g:m 
}

chordc = \chordmode {
  c1:m g:m d:m c:m 
  f:m g:m f:m d:m
  f:m c:m g2:m ef
}


chordNames = {
  \chordmode { s1 }
  \chorda
  \chorda
  \chordb
  \chordmode { c2:m g:m c1:m}
  %\chordb
  \chordmode { g2:m f:m c4:m g:m c:m s}
  \chordc
  \chordmode { c1:m  c1:m }
  \chorda
  \chordb
  \chordmode { c2:m g:m c1:m}
  %\chordb
  \chordmode { g2:m f:m c4:m g:m c:m s}
}




melody = \relative g' {
  \time 4/4
  \key c \minor  

  r2 r8 c8 b c | 
%  \repeat volta 2 { ef4 c f ef | g8 f ef f g4 r | c af f c' | g f ef r   \break
%  d ef f g | ef f g r | af2 af4 c  | g8 f ef f g2 | \break
%  c4 af f c' | ef d c r |    af f d g |  }
%  \alternative {{ c, r r8 c8 b c |  }
%                { c4  r4 r8 g' fs g |  }} \break

  ef4 c f ef | g8 f ef f g4 r | c af f c' | g f ef r   \break
  d ef f g | ef f g r | af2 af4 c  | g8 f ef f g2 | \break
  c4 af f c' | ef d c r |    af f d g | c, r r8 c8 b c |   \break
  
  ef4 c f ef | g8 f ef f g4 r | c af f c' | g f ef r   \break
  d ef f g | ef f g r | af2 af4 c  | g8 f ef f g2 | \break
  c4 af f c' | ef d c r |    af f d g |  c4  
  
  
  r4 r8 g fs g | \break
                

  \repeat volta 2 {
  af4 f8 af c4 bf8 af | g4 ef c r |  b8 c b c d2 | b8 c b c d2 \break
  af'4 f8 af c4 bf8 af | c4 g g r | }
  \alternative {
    {g f ef d | c2 r8 g' fs g }
    {g4 af bf b | c g c r }
  }\break 
  \repeat volta 2 {
  ef,1  | d1 | f | ef 
  af    |  g   |  bf | f \break
  af       | ef  | d2 g,  |}
  \alternative {
    { c1  }
    { c2 r8 c8 b c }
  } \break
  \bar "||"
  ef4 c f ef | g8 f ef f g4 r | c af f c' | g f ef r   \break
  d ef f g | ef8 d ef8 f g4 r | af2 af4 c  | g8 f ef f g2 | \break
  c4 af f c' | ef d c r |    af f d g | c,2 r8  g' fs g | \break

  \repeat volta 2 {
  af4 f8 af c4 bf8 af | g4 ef c r |  b8 c b c d2 | b8 c b c d2 \break
  af'4 f8 af c4 bf8 af | c4 g g r | }
  \alternative {
    {g f ef d | c2 r8 g' fs g }
    {g4 af bf b | c g c r }
  }\break 
  \bar "|."
}


bassT = \relative c {
  \time 4/4
  \key c \minor  

  <<
  \new CueVoice {
    \stemUp r2 r8 c'8^"clarinet" b c
  }  
  >>
  \bar "||"
  c,2 b | c4 ef g4. g8 | f4 ef d f | ef d c4. ef8 |\break
  d4 ef f f | ef d c4. g'8 | af4 g8 f ef4 f | g g  ef2 | \break
  f4 f c f | ef d c2 | d4 f af b | c g c r | \break
  \bar "||"
  c4 c b b | c c g g | f f d d | ef d c c | \break
  d d f f  | ef d c c | af' af ef ef | g d g d | \break 
  f c f f | ef d c c | d f af b | c g c g | \break

% \bar "||"
  
%  f f c c | ef ef c c | d a' d, a' | ef bf' ef, bf' | \break
%  f f c c | g' ef g f | ef ef g g | c, ef g c | \break
%  \bar "||"
%  f, f d d | ef ef c c | d a' d, a' | ef bf' ef, bf' | \break
%  f f c c | g' d g g | g g f f | c g' c4. g8 \break

  \repeat volta 2{
    f f d d | ef ef c c | d a' d, a' | ef bf' ef, bf' | \break
    f f c c | g' d g g | 
  }
  \alternative{
    {ef ef g g | c, ef g c | }
    {g g f f | c g' c4. g8 }
  }\break

  
  \repeat volta 2 {
  c,4. ef8 g4 ef | g4. bf8 bf4 d | d,4.  d8 f4 a | c,4. ef8 ef4 g | \break
  f4. af8 af4 c | g4. g8 bf4 d | f,4. af8 af4 c | d,4.  d8 f4 a |  \break
  f4. af8 af4 c | c,4. ef8 ef4 g | g f ef d |
  } 
  \alternative{
     { c4. ef8 ef4 g | }
     { c,4. ef8 ef4 g | }
  }\break
  
  c,4 c b b | c c g' g | f f d d | ef d c c | \break
  d d f f  | ef d c c | af' af ef ef | g g d d | \break 
  f c f ef | ef d c c | d f af b | c g c g | \break

%  \bar "||" 
%  f f c c | ef ef c c | d a' d, a' | ef bf' ef, bf' | \break
%  f f c c | g' d  g g | ef ef g g | c, ef g c | \break 
  
%  f,   f c c | ef ef c c | d a' d, a' | ef bf' ef, bf' | \break
%  f f c c | g' d  g d | g g f f | c g' c r4 \bar "|."
  \repeat volta 2{
      f f c c | ef ef c c | d a' d, a' | ef bf' ef, bf' | \break
      f f c c | g' d  g g |
  }
  \alternative{
     {ef ef g g | c, ef g c | }
     {g g f f | c g' c r4 |}
  }
  \bar "|."
}

bass={
  \clef F
  \bassT
}


\book {
  \bookOutputName "rouge_all"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
  
      \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
        \melody
      }
      
      \new Staff  \with { instrumentName = #"Bass (C)" }{ 
        \bass
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\paper {
  %page-count = 1
  top-margin = 1
  bottom-margin = 1
  %annotate-spacing = ##t 
  system-system-spacing = #'((basic-distance . 10.1) (padding . 0))
  %system-system-spacing = #10
}


\book {
  \bookOutputName "rouge_C"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }  
      \new Staff  \with { instrumentName = #"Clarinet (C)" }{ 
        \melody
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "rouge_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
  
      \new Staff  \with { instrumentName = #"Clarinet (Bb)" }{ 
        \transpose c d {
          \melody
        }
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "rouge_bass"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
  
      \new Staff  \with { instrumentName = #"Bass (C)" }{ 
        \bass
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "rouge_bass_Bb_bass_clef"
  \score {
    <<
      \new ChordNames { 
        \transpose c d {  \chordNames }
      }
  
      \new Staff  \with { instrumentName = #"Bass (Bb)" }{ 
        \transpose c d { \bass }
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}

\book {
  \bookOutputName "rouge_bass_Bb_treble_clef"
  \score {
    <<
      \new ChordNames { 
        \transpose c d {  \chordNames }
      }
  
      \new Staff  \with { instrumentName = #"Bass (Bb)" }{ 
        \transpose c d' { \bassT }
      }
    >>
    \layout { #(layout-set-staff-size 16)}
  }
}



\book {
  \bookOutputName "rouge_alto"
  \score {
    <<
      \new ChordNames { 
        \chordNames
      }
  
      \new Staff  \with { instrumentName = #"Alto (C)" }{ 
        \clef "C"
        \transpose c c,{
          \melody
        }
      }
    >>
    \layout { #(layout-set-staff-size 16)}
   }
}
    